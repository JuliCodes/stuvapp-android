package de.stuv_mosbach.stuvapp.parser;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Tests for the {@link FeedElement} functions.
 *
 * @author Peer Beckmann
 */
public class FeedElementTest {

    @Test
    public void equals_sym_hashCode() {
        FeedElement feedElement1 = new FeedElement();
        feedElement1.setTitle("Hallo");
        feedElement1.setContent("Welt");
        feedElement1.setDescription("Hallo Welt!");
        feedElement1.setImageName("Image Name");
        feedElement1.setLink("Link");
        feedElement1.setPubDate(100L);

        FeedElement feedElement2 = new FeedElement();
        feedElement2.setTitle("Hallo");
        feedElement2.setContent("Welt");
        feedElement2.setDescription("Hallo Welt!");
        feedElement2.setImageName("Image Name");
        feedElement2.setLink("Link");
        feedElement2.setPubDate(100L);

        FeedElement feedElement3 = new FeedElement();
        feedElement3.setTitle("Ciao");
        feedElement3.setContent("Welt");
        feedElement3.setDescription("Hallo Welt!");
        feedElement3.setImageName("Image Name");
        feedElement3.setLink("Link");
        feedElement3.setPubDate(100L);

        // Assert both messages are equals in hashCode and real object
        assertTrue(feedElement1.equals(feedElement2));
        assertEquals(feedElement1.hashCode(), feedElement2.hashCode());

        // Assert both messages are unequal in hashCode and real object
        assertFalse(feedElement1.equals(feedElement3));
        assertNotEquals(feedElement1.hashCode(), feedElement3.hashCode());

        assertFalse(feedElement2.equals(feedElement3));
        assertNotEquals(feedElement2.hashCode(), feedElement3.hashCode());

        // Assert a message is not a string
        //noinspection EqualsBetweenInconvertibleTypes
        assertFalse(feedElement1.equals("Hallo"));
    }
}