package de.stuv_mosbach.stuvapp.parser;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Test for the Message functions.
 *
 * @author Peer Beckmann
 */
public class MessageTest {
    @Test
    public void isValid() {
        Message message = new Message();
        assertFalse(message.isValid());

        message.setTitle("Hallo");
        assertFalse(message.isValid());

        message.setContent("Welt");
        assertFalse(message.isValid());

        message.setUid(1);
        assertFalse(message.isValid());

        message.setPriority(0);
        assertTrue(message.isValid());

        message.setPriority(1);
        assertTrue(message.isValid());

        message.setPriority(2);
        assertTrue(message.isValid());

        message.setPriority(42);
        assertTrue(message.isValid());

        message.setPriority(-1);
        assertFalse(message.isValid());
        message.setPriority(1);

        message.setUid(-1);
        assertFalse(message.isValid());
        message.setUid(1);
    }

    @Test
    public void equals_sym_hashCode() {
        Message message1 = new Message();
        message1.setTitle("Hallo");
        message1.setContent("Welt");
        message1.setPriority(0);
        message1.setUid(0);
        message1.setBegin(100L);
        message1.setEnd(100L);

        Message message2 = new Message();
        message2.setTitle("Hallo");
        message2.setContent("Welt");
        message2.setPriority(0);
        message2.setUid(0);
        message2.setBegin(100L);
        message2.setEnd(100L);

        Message message3 = new Message();
        message3.setTitle("Ciao");
        message3.setContent("Welt");
        message3.setPriority(0);
        message3.setUid(0);
        message3.setBegin(100L);
        message3.setEnd(100L);

        // Assert both messages are equals in hashCode and real object
        assertTrue(message1.equals(message2));
        assertEquals(message1.hashCode(), message2.hashCode());

        // Assert both messages are unequal in hashCode and real object
        assertFalse(message1.equals(message3));
        assertNotEquals(message1.hashCode(), message3.hashCode());

        assertFalse(message2.equals(message3));
        assertNotEquals(message2.hashCode(), message3.hashCode());

        // Assert a message is not a string
        //noinspection EqualsBetweenInconvertibleTypes
        assertFalse(message1.equals("Hallo"));
    }
}