package de.stuv_mosbach.stuvapp.sync.syncer;

import android.content.ContentProviderClient;

import com.google.gson.JsonArray;
import com.google.gson.JsonParser;

import org.json.JSONException;
import org.junit.Test;
import org.mockito.Mockito;

import java.net.URL;
import java.util.LinkedList;

import de.stuv_mosbach.stuvapp.sync.localSources.LocalSource;

/**
 * Created by chaos on 10/26/16.
 */
public class MessagesSyncerTest {

    private ContentProviderClient contentProviderClient = Mockito.mock(ContentProviderClient.class);
    private LocalSource localSource = Mockito.mock(LocalSource.class);

    @Test
    public void onCompleted() throws Exception {
        // Test empty JSON Array
        MessagesSyncer syncer = new MessagesSyncer();
        syncer.onCompleted(null, new JsonArray());

        // Test empty JSON Array with error
        syncer = new MessagesSyncer();
        syncer.onCompleted(new JSONException("Error"), new JsonArray());

        class MessageTest {
            private String jsonString;

            private MessageTest(String jsonString) {
                this.jsonString = jsonString;
            }
        }

        LinkedList<MessageTest> tests = new LinkedList<>();

        tests.add(new MessageTest("[{\"title\": \"\"}]"));
        tests.add(new MessageTest("[{\"title\": 3}]"));
        tests.add(new MessageTest("[{\"title\": \"Welt\"}]"));
        tests.add(new MessageTest("[{\"title\": \"Welt\", \"content\": \"\"}]"));
        tests.add(new MessageTest("[{\"title\": \"Welt\", \"content\": 2}]"));
        tests.add(new MessageTest("[{\"title\": \"Welt\", \"content\": \"Alarm\"}]"));
        tests.add(new MessageTest("[{\"content\": \"Alarm\"}]"));
        tests.add(new MessageTest("[{\"title\": \"Welt\", \"content\": \"Alarm\", \"uid\": \"String\", \"priority\": \"String\", \"begin\": \"String\", \"end\": \"String\"}]"));
        tests.add(new MessageTest("[{\"title\": \"Welt\", \"content\": \"Alarm\", \"uid\": 0, \"priority\": 1, \"begin\": 100, \"end\": 100}]"));
        tests.add(new MessageTest("[{\"title\": \"Welt\", \"content\": \"Alarm\", \"uid\": 0, \"priority\": \"1\", \"begin\": 100, \"end\": 100}]")); // API returns  priority as string

        for (int i = 0; i < tests.size(); i++) {
            MessageTest test = tests.get(i);
            JsonParser parser = new JsonParser();
            JsonArray array = parser.parse(test.jsonString).getAsJsonArray();
            syncer = new MessagesSyncer();
            syncer.initialize(null, contentProviderClient, new URL("https://stuv-mosbach.de/survival/api/getMessages.php"), localSource);
            syncer.onCompleted(null, array);
        }
    }

}