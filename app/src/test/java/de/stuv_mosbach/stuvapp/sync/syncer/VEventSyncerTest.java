package de.stuv_mosbach.stuvapp.sync.syncer;

import android.content.ContentProviderClient;
import android.content.Context;
import android.content.SyncResult;
import android.content.SyncStats;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.net.URL;

import biweekly.component.VEvent;
import de.stuv_mosbach.stuvapp.sync.localSources.LocalSource;
import de.stuv_mosbach.stuvapp.sync.localSources.LocalSourceResult;

import static org.junit.Assert.assertEquals;

/**
 * Created by chaos on 11/14/16.
 */
public class VEventSyncerTest {

    private Context context = Mockito.mock(Context.class);
    private ContentProviderClient client = Mockito.mock(ContentProviderClient.class);
    private LocalSource<VEvent> local = Mockito.mock(LocalSource.class);

    @Before
    public void setUp() throws Exception {
        Mockito.when(local.runDifferences(client)).thenReturn(new LocalSourceResult());
    }

    // Try to parse all that courses one time.
    @Test
    public void testAllClasses() throws Exception {
        VEventSyncer syncer = new VEventSyncer();
        URL courses = new URL("http://ics.mosbach.dhbw.de/ics/calendars.list");

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(courses.openStream()));
        String line;

        SyncResult result = Mockito.mock(SyncResult.class);
        Field statsField = result.getClass().getField("stats");
        SyncStats syncStats = Mockito.mock(SyncStats.class);
        statsField.set(result, syncStats);

        while ((line = bufferedReader.readLine()) != null) {
            String[] lineArr = line.split(";");
            if ((lineArr[0].length() != 0) && (!lineArr[0].equals("CALENDARS"))) {
                syncer.initialize(context, client, new URL(lineArr[1]), local);
                syncer.prepare();
                syncer.run(result);
                assertEquals(0, result.stats.numIoExceptions);
            }
        }
    }

    // Assert the stream won't be parsed, if the stream did not contain a icalendar.
    @Test
    public void testMissingClass() throws Exception {
        SyncResult result = Mockito.mock(SyncResult.class);
        Field statsField = result.getClass().getField("stats");
        SyncStats syncStats = Mockito.mock(SyncStats.class);
        statsField.set(result, syncStats);

        VEventSyncer syncer = new VEventSyncer();
        syncer.initialize(context, client, new URL("http://stuv-mosbach.de"), local);
        syncer.prepare();
        syncer.run(result);
        assertEquals(1, result.stats.numIoExceptions);
    }

    // Assert the stream won't be parsed, if the stream did not contain a icalendar.
    @Test
    public void testIOException() throws Exception {
        SyncResult result = Mockito.mock(SyncResult.class);
        Field statsField = result.getClass().getField("stats");
        SyncStats syncStats = Mockito.mock(SyncStats.class);
        statsField.set(result, syncStats);

        VEventSyncer syncer = new VEventSyncer();
        syncer.initialize(context, client, new URL("http://localhost/"), local);
        syncer.prepare();
        syncer.run(result);
        assertEquals(1, result.stats.numIoExceptions);
    }
}