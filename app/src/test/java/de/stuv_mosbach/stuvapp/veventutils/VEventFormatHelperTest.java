package de.stuv_mosbach.stuvapp.veventutils;

import android.content.SharedPreferences;
import android.content.res.Resources;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.Locale;

import de.stuv_mosbach.stuvapp.R;
import de.stuv_mosbach.stuvapp.settings.PreferenceKeys;

import static org.junit.Assert.assertEquals;

public class VEventFormatHelperTest {
    final private SharedPreferences sharedPreferences = Mockito.mock(SharedPreferences.class);
    final private Resources resources = Mockito.mock(Resources.class);
    final private String prefixNew = "New: ";
    final private String prefixDeleted = "Deleted: ";
    final private String minusDelimiter = " - ";
    final private String prefixFinishes = "Finishes: ";
    final private String allDay = "All-Day";

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Before
    public void setUp() throws Exception {
        Mockito.when(resources.getString(R.string.prefix_new)).thenReturn(prefixNew);
        Mockito.when(resources.getString(R.string.prefix_deleted)).thenReturn(prefixDeleted);
        Mockito.when(resources.getString(R.string.minus_delimiter)).thenReturn(minusDelimiter);
        Mockito.when(resources.getString(R.string.prefix_finish)).thenReturn(prefixFinishes);
        Mockito.when(resources.getString(R.string.fullday)).thenReturn(allDay);
    }

    @Test
    public void testForNoResourcesGiven() {
        VEventFormatHelper.Builder builder = new VEventFormatHelper.Builder()
                .setSharedPreferences(sharedPreferences);

        exception.expect(RuntimeException.class);
        exception.expectMessage("No resources object given.");
        builder.build();
    }

    @Test
    public void testForNoSharedPreferencesGiven() {
        VEventFormatHelper.Builder builder = new VEventFormatHelper.Builder()
                .setResources(resources);

        exception.expect(RuntimeException.class);
        exception.expectMessage("No SharedPreferences given.");
        builder.build();
    }

    @Test
    public void testGetHeader() {
        Locale.setDefault(Locale.US);

        GregorianCalendar cal = new GregorianCalendar(Locale.getDefault());
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.MONTH, Calendar.JULY);
        cal.set(Calendar.YEAR, 2016);
        final long start = cal.getTime().getTime(); // Fri 01.07.2016

        VEventFormatHelper testNormalHeader = new VEventFormatHelper.Builder()
                .setSharedPreferences(sharedPreferences)
                .setResources(resources)
                .setStart(start)
                .build();

        assertEquals("Fri 01.07.2016", testNormalHeader.getHeader());
    }

    @Test
    public void testGetSummary() {
        final String customSummary = "Mathematik";
        final String summary = "Datenbanken 2 Vorlesung";
        final String shortedSummary = "Datenbanken 2";

        class TestCaseSummary {
            private String summary;
            private boolean shortSummary;
            private boolean flagNew;
            private boolean flagDeleted;

            private String expectedResult;

            private TestCaseSummary(String summary, boolean shortSummary, boolean flagNew, boolean flagDeleted, String expectedResult) {
                this.summary = summary;
                this.shortSummary = shortSummary;
                this.flagDeleted = flagDeleted;
                this.flagNew = flagNew;
                this.expectedResult = expectedResult;
            }
        }

        LinkedList<TestCaseSummary> testCases = new LinkedList<>();

        // Test with normal summary
        testCases.add(new TestCaseSummary(summary, false, false, false, summary));
        // Test with null summary
        testCases.add(new TestCaseSummary(null, true, false, false, ""));
        // Test with shorted summary
        testCases.add(new TestCaseSummary(summary, true, false, false, shortedSummary));
        // Test with NEW flag and normal summary
        testCases.add(new TestCaseSummary(summary, false, true, false, prefixNew + summary));
        // Test with NEW flag and shorted summary
        testCases.add(new TestCaseSummary(summary, true, true, false, prefixNew + shortedSummary));
        // Test with DELETED flag and normal summary
        testCases.add(new TestCaseSummary(summary, false, false, true, prefixDeleted + summary));
        // Test with DELETED flag and shorted summary
        testCases.add(new TestCaseSummary(summary, true, false, true, prefixDeleted + shortedSummary));
        // Test with NEW and DELETED flag and normal summary
        testCases.add(new TestCaseSummary(summary, false, true, true, prefixDeleted + summary));
        // Test with NEW and DELETED flag and shorted summary
        testCases.add(new TestCaseSummary(summary, true, true, true, prefixDeleted + shortedSummary));
        // Test with Summary without "Vorlesung" and shorting
        testCases.add(new TestCaseSummary(customSummary, true, false, false, customSummary));

        for (int i = 0; i < testCases.size(); i++) {
            TestCaseSummary testCaseSummary = testCases.get(i);
            Mockito.when(sharedPreferences.getBoolean(PreferenceKeys.SHOW_LECTURE_IN_TITLE, false)).thenReturn(testCaseSummary.shortSummary);
            VEventFormatHelper formatHelper = new VEventFormatHelper.Builder()
                    .setSharedPreferences(sharedPreferences)
                    .setResources(resources)
                    .setSummary(testCaseSummary.summary)
                    .setFLAG_NEW(testCaseSummary.flagNew)
                    .setFLAG_DELETED(testCaseSummary.flagDeleted)
                    .build();

            assertEquals("TestCase " + i + " failed.", testCaseSummary.expectedResult, formatHelper.getSummary());
        }
    }

    @Test
    public void testGetOrigin() {
        class TestCaseOrigin {
            private String setter;
            private String expected;

            private TestCaseOrigin(String setter, String expected) {
                this.setter = setter;
                this.expected = expected;
            }
        }

        LinkedList<TestCaseOrigin> testCases = new LinkedList<>();

        testCases.add(new TestCaseOrigin(null, ""));
        testCases.add(new TestCaseOrigin("", ""));
        testCases.add(new TestCaseOrigin("Students", "Students"));

        for (int i = 0; i < testCases.size(); i++) {
            TestCaseOrigin testCaseOrigin = testCases.get(i);
            VEventFormatHelper formatHelper = new VEventFormatHelper.Builder()
                    .setSharedPreferences(sharedPreferences)
                    .setResources(resources)
                    .setOrigin(testCaseOrigin.setter)
                    .build();

            assertEquals("Test " + i, testCaseOrigin.expected, formatHelper.getOrigin());
        }
    }

    @Test
    public void testGetFormattedDates() {
        Locale.setDefault(Locale.US);

        GregorianCalendar cal = new GregorianCalendar(Locale.getDefault());
        cal.set(Calendar.DAY_OF_MONTH, 9);
        cal.set(Calendar.MONTH, Calendar.OCTOBER);
        cal.set(Calendar.HOUR_OF_DAY, 12);
        cal.set(Calendar.MINUTE, 40);

        final long start = cal.getTime().getTime(); // Sun, 09 Oct 2016 12:40:09 GMT
        final String startHourMinute = "12:40";

        cal.set(Calendar.HOUR_OF_DAY, 14);
        final long end = cal.getTime().getTime(); // Sun, 09 Oct 2016 14:40:09 GMT
        final String endHourMinute = "14:40";
        final String endDayMonth = "09.10";
        final String endDayMonthHourMinute = "09.Oct " + endHourMinute + " Uhr";

        class TestCaseDates {
            private long start;
            private long end;
            private boolean allDay;
            private boolean multiDay;
            private String expectedResult;

            private TestCaseDates(long start, long end, boolean allDay, boolean multiDay, String expectedResult) {
                this.start = start;
                this.end = end;
                this.allDay = allDay;
                this.multiDay = multiDay;
                this.expectedResult = expectedResult;
            }
        }

        LinkedList<TestCaseDates> testCases = new LinkedList<>();

        // Test for normal times
        testCases.add(new TestCaseDates(start, end, false, false, startHourMinute + minusDelimiter + endHourMinute));
        // Test for all-day event
        testCases.add(new TestCaseDates(start, end, true, false, allDay));
        // Test for multi day event
        testCases.add(new TestCaseDates(start, end, false, true, startHourMinute + minusDelimiter + endDayMonthHourMinute));
        // Test for all-day and multi-day event
        testCases.add(new TestCaseDates(start, end, true, true, prefixFinishes + endDayMonth));

        for (int i = 0; i < testCases.size(); i++) {
            TestCaseDates testCaseDates = testCases.get(i);
            VEventFormatHelper formatHelper = new VEventFormatHelper.Builder()
                    .setSharedPreferences(sharedPreferences)
                    .setResources(resources)
                    .setStart(testCaseDates.start)
                    .setEnd(testCaseDates.end)
                    .setAllDay(testCaseDates.allDay)
                    .setMultiDay(testCaseDates.multiDay)
                    .build();

            assertEquals("Test case " + i + " failed", testCaseDates.expectedResult, formatHelper.getFormattedDates());
        }
    }

    @Test
    public void testDescription() {
        class TestCaseDescription {
            private String description;
            private boolean isSet;

            private String expectedResult;

            private TestCaseDescription(String description, boolean isSet, String expectedResult) {
                this.description = description;
                this.isSet = isSet;
                this.expectedResult = expectedResult;
            }
        }

        LinkedList<TestCaseDescription> testCases = new LinkedList<>();

        testCases.add(new TestCaseDescription("", false, ""));
        testCases.add(new TestCaseDescription(null, false, ""));
        testCases.add(new TestCaseDescription("Dr.H.Neemann", true, "Dr.H.Neemann"));

        for (int i = 0; i < testCases.size(); i++) {
            TestCaseDescription testCaseDescription = testCases.get(i);
            VEventFormatHelper formatHelper = new VEventFormatHelper.Builder()
                    .setSharedPreferences(sharedPreferences)
                    .setResources(resources)
                    .setDescription(testCaseDescription.description)
                    .build();

            assertEquals("TestCase " + i, testCaseDescription.isSet, formatHelper.isDescription());
            assertEquals("TestCase " + i, testCaseDescription.expectedResult, formatHelper.getDescription());
        }
    }

    @Test
    public void testLocation() {
        class TestCaseLocation {
            private String location;
            private boolean isSet;

            private String expectedResult;

            private TestCaseLocation(String location, boolean isSet, String expectedResult) {
                this.location = location;
                this.isSet = isSet;
                this.expectedResult = expectedResult;
            }
        }

        LinkedList<TestCaseLocation> testCases = new LinkedList<>();

        testCases.add(new TestCaseLocation("", false, ""));
        testCases.add(new TestCaseLocation(null, false, ""));
        testCases.add(new TestCaseLocation("D-1.130", true, "D-1.130"));

        for (int i = 0; i < testCases.size(); i++) {
            TestCaseLocation testCaseLocation = testCases.get(i);
            VEventFormatHelper formatHelper = new VEventFormatHelper.Builder()
                    .setSharedPreferences(sharedPreferences)
                    .setResources(resources)
                    .setLocation(testCaseLocation.location)
                    .build();

            assertEquals("TestCase " + i, testCaseLocation.isSet, formatHelper.isLocation());
            assertEquals("TestCase " + i, testCaseLocation.expectedResult, formatHelper.getLocation());
        }
    }

    @Test
    public void testIsRunning() {
        long now = new Date().getTime();
        long secondsAgo = now - 10000;
        long doubleSecondsAgo = now - 20000;
        long secondsLater = now + 10000;
        long doubleSecondsLater = now + 20000;

        class TestCaseRunning {
            private long start;
            private long end;
            private boolean expectedResult;

            private TestCaseRunning(long start, long end, boolean expectedResult) {
                this.start = start;
                this.end = end;
                this.expectedResult = expectedResult;
            }
        }

        LinkedList<TestCaseRunning> testCases = new LinkedList<>();

        testCases.add(new TestCaseRunning(doubleSecondsAgo, secondsAgo, false));
        testCases.add(new TestCaseRunning(secondsAgo, now, true));
        testCases.add(new TestCaseRunning(secondsAgo, secondsLater, true));
        testCases.add(new TestCaseRunning(now, secondsLater, true));
        testCases.add(new TestCaseRunning(secondsLater, doubleSecondsLater, false));

        for (int i = 0; i < testCases.size(); i++) {
            TestCaseRunning testCaseRunning = testCases.get(i);
            VEventFormatHelper formatHelper = new VEventFormatHelper.Builder()
                    .setSharedPreferences(sharedPreferences)
                    .setResources(resources)
                    .setStart(testCaseRunning.start)
                    .setEnd(testCaseRunning.end)
                    .build();

            assertEquals("Test " + i, testCaseRunning.expectedResult, formatHelper.isRunning());
        }
    }
}