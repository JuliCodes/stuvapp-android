package de.stuv_mosbach.stuvapp.veventutils;

import android.content.res.Resources;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.LinkedList;

import de.stuv_mosbach.stuvapp.R;

import static org.junit.Assert.assertEquals;

public class VEventNotificationHelperTest {

    final private Resources resources = Mockito.mock(Resources.class);
    final private String prefixNew = "New: ";
    final private String prefixDeleted = "Deleted: ";
    final private String prefixUpdated = "Updated: ";
    final private String changes = "There are %s changes";
    final private String newEvents = "There are %s new events";
    final private String newEvent = "There is one new event";
    final private String changedEvents = "There are %s changed events";
    final private String changedEvent = "There is one changed event";
    final private String deletedEvents = "There are %s deleted events";
    final private String deletedEvent = "There is one deleted event";

    @Before
    public void setUp() throws Exception {
        Mockito.when(resources.getString(R.string.prefix_new)).thenReturn(prefixNew);
        Mockito.when(resources.getString(R.string.prefix_deleted)).thenReturn(prefixDeleted);
        Mockito.when(resources.getString(R.string.prefix_updated)).thenReturn(prefixUpdated);
        Mockito.when(resources.getString(R.string.notification_changes)).thenReturn(changes);
        Mockito.when(resources.getString(R.string.notification_new_events)).thenReturn(newEvents);
        Mockito.when(resources.getString(R.string.notification_new_event)).thenReturn(newEvent);
        Mockito.when(resources.getString(R.string.notification_changed_events)).thenReturn(changedEvents);
        Mockito.when(resources.getString(R.string.notification_changed_event)).thenReturn(changedEvent);
        Mockito.when(resources.getString(R.string.notification_deleted_events)).thenReturn(deletedEvents);
        Mockito.when(resources.getString(R.string.notification_deleted_event)).thenReturn(deletedEvent);
    }

    @Test
    public void testIsBigStyle() {
        class TestCaseBigStyle {
            private int inserts;
            private int updates;
            private int deletes;

            private boolean expected;

            private TestCaseBigStyle(int inserts, int updates, int deletes, boolean expected) {
                this.inserts = inserts;
                this.updates = updates;
                this.deletes = deletes;
                this.expected = expected;
            }
        }

        LinkedList<TestCaseBigStyle> testCases = new LinkedList<>();

        testCases.add(new TestCaseBigStyle(0, 0, 0, false));

        testCases.add(new TestCaseBigStyle(1, 0, 0, false));
        testCases.add(new TestCaseBigStyle(0, 1, 0, false));
        testCases.add(new TestCaseBigStyle(0, 0, 1, false));

        testCases.add(new TestCaseBigStyle(1, 1, 0, true));
        testCases.add(new TestCaseBigStyle(1, 0, 1, true));
        testCases.add(new TestCaseBigStyle(0, 1, 1, true));

        testCases.add(new TestCaseBigStyle(1, 1, 1, true));

        for (int i = 0; i < testCases.size(); i++) {
            TestCaseBigStyle testCaseBigStyle = testCases.get(i);

            VEventNotificationHelper notificationHelper = new VEventNotificationHelper(resources);
            notificationHelper.setNewEvents(testCaseBigStyle.inserts);
            notificationHelper.setDeletedEvents(testCaseBigStyle.deletes);
            notificationHelper.setChangedEvents(testCaseBigStyle.updates);

            assertEquals("Test " + i, testCaseBigStyle.expected, notificationHelper.isBigStyle());
        }
    }

    @Test
    public void testGetLittleContent() {
        class TestCaseLittleContent {
            private int inserts;
            private int updates;
            private int deletes;

            private String expected;

            private TestCaseLittleContent(int inserts, int updates, int deletes, String expected) {
                this.inserts = inserts;
                this.updates = updates;
                this.deletes = deletes;
                this.expected = expected;
            }
        }

        LinkedList<TestCaseLittleContent> testCases = new LinkedList<>();

        testCases.add(new TestCaseLittleContent(0, 0, 0, ""));

        testCases.add(new TestCaseLittleContent(1, 0, 0, newEvent));
        testCases.add(new TestCaseLittleContent(0, 1, 0, changedEvent));
        testCases.add(new TestCaseLittleContent(0, 0, 1, deletedEvent));
        
        testCases.add(new TestCaseLittleContent(2, 0, 0, newEvents.replace("%s", String.valueOf(2))));
        testCases.add(new TestCaseLittleContent(0, 2, 0, changedEvents.replace("%s", String.valueOf(2))));
        testCases.add(new TestCaseLittleContent(0, 0, 2, deletedEvents.replace("%s", String.valueOf(2))));

        testCases.add(new TestCaseLittleContent(15, 0, 0, newEvents.replace("%s", String.valueOf(15))));
        testCases.add(new TestCaseLittleContent(0, 15, 0, changedEvents.replace("%s", String.valueOf(15))));
        testCases.add(new TestCaseLittleContent(0, 0, 15, deletedEvents.replace("%s", String.valueOf(15))));

        testCases.add(new TestCaseLittleContent(1, 1, 0, changes.replace("%s", String.valueOf(2))));
        testCases.add(new TestCaseLittleContent(1, 0, 1, changes.replace("%s", String.valueOf(2))));
        testCases.add(new TestCaseLittleContent(0, 1, 1, changes.replace("%s", String.valueOf(2))));

        testCases.add(new TestCaseLittleContent(5, 5, 0, changes.replace("%s", String.valueOf(10))));
        testCases.add(new TestCaseLittleContent(5, 0, 5, changes.replace("%s", String.valueOf(10))));
        testCases.add(new TestCaseLittleContent(0, 5, 5, changes.replace("%s", String.valueOf(10))));

        testCases.add(new TestCaseLittleContent(1, 1, 1, changes.replace("%s", String.valueOf(3))));

        for (int i = 0; i < testCases.size(); i++) {
            TestCaseLittleContent testCaseLittleContent = testCases.get(i);

            VEventNotificationHelper notificationHelper = new VEventNotificationHelper(resources);
            notificationHelper.setNewEvents(testCaseLittleContent.inserts);
            notificationHelper.setChangedEvents(testCaseLittleContent.updates);
            notificationHelper.setDeletedEvents(testCaseLittleContent.deletes);
            assertEquals("Test " + i, testCaseLittleContent.expected, notificationHelper.getLittleContent());
        }
    }

    @Test
    public void testGetBigContent() {
        class TestCaseBigContent {
            private int inserts;
            private int updates;
            private int deletes;

            private String expected;

            private TestCaseBigContent(int inserts, int updates, int deletes, String expected) {
                this.inserts = inserts;
                this.updates = updates;
                this.deletes = deletes;
                this.expected = expected;
            }
        }

        LinkedList<TestCaseBigContent> testCases = new LinkedList<>();

        testCases.add(new TestCaseBigContent(0, 0, 0, ""));

        testCases.add(new TestCaseBigContent(1, 0, 0, prefixNew + 1));
        testCases.add(new TestCaseBigContent(0, 1, 0, prefixUpdated + 1));
        testCases.add(new TestCaseBigContent(0, 0, 1, prefixDeleted + 1));

        testCases.add(new TestCaseBigContent(1, 1, 0, prefixNew + 1 + "\n" + prefixUpdated + 1));
        testCases.add(new TestCaseBigContent(1, 0, 1, prefixNew + 1 + "\n" + prefixDeleted + 1));
        testCases.add(new TestCaseBigContent(0, 1, 1, prefixUpdated + 1 + "\n" + prefixDeleted + 1));

        testCases.add(new TestCaseBigContent(1, 1, 1, prefixNew + 1 + "\n" + prefixUpdated + 1 + "\n" + prefixDeleted + 1));

        for (int i = 0; i < testCases.size(); i++) {
            TestCaseBigContent testCaseBigContent = testCases.get(i);
            VEventNotificationHelper notificationHelper = new VEventNotificationHelper(resources);
            notificationHelper.setNewEvents(testCaseBigContent.inserts);
            notificationHelper.setChangedEvents(testCaseBigContent.updates);
            notificationHelper.setDeletedEvents(testCaseBigContent.deletes);
            assertEquals("Test " + i, testCaseBigContent.expected, notificationHelper.getBigContent());
        }
    }
}