package de.stuv_mosbach.stuvapp.veventutils;

import org.junit.Test;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedList;

import biweekly.property.DateEnd;
import biweekly.property.DateStart;
import biweekly.property.TextProperty;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class VEventHelperTest {

    @Test
    public void testGetDurationOr0() {
        final long duration = 1000;

        final Date leftValue = new Date();
        final Date rightValue = new Date(leftValue.getTime() + duration);
        final Date nullDate = null;

        final DateStart left = new DateStart(leftValue);
        final DateEnd right = new DateEnd(rightValue);
        final DateStart nullStart = new DateStart(nullDate);
        final DateEnd nullEnd = new DateEnd(nullDate);

        class TestCaseDuration {
            private DateStart start;
            private DateEnd end;

            private long expected;

            private TestCaseDuration(DateStart start, DateEnd end, long expected) {
                this.start = start;
                this.end = end;
                this.expected = expected;
            }
        }

        LinkedList<TestCaseDuration> testCases = new LinkedList<>();

        testCases.add(new TestCaseDuration(left, right, duration));
        testCases.add(new TestCaseDuration(nullStart, right, 0));
        testCases.add(new TestCaseDuration(left, nullEnd, 0));
        testCases.add(new TestCaseDuration(nullStart, nullEnd, 0));
        testCases.add(new TestCaseDuration(null, null, 0));
        testCases.add(new TestCaseDuration(null, right, 0));
        testCases.add(new TestCaseDuration(left, nullEnd, 0));

        for (int i = 0; i < testCases.size(); i++) {
            TestCaseDuration testCaseDuration = testCases.get(i);
            long actualDuration = VEventHelper.getDurationOr0(testCaseDuration.start, testCaseDuration.end);
            assertEquals("Test " + i, testCaseDuration.expected, actualDuration);
        }
    }

    @Test
    public void testCompareTextProperties() {
        final TextProperty left = new TextProperty("B-1.120");
        final TextProperty right = new TextProperty("D-1.100");
        final String nullString = null;
        final TextProperty nullProperty = new TextProperty(nullString);

        int correctOrder = VEventHelper.compareTextProperties(left, right);
        assertTrue(correctOrder < 0);

        int reverseOrder = VEventHelper.compareTextProperties(right, left);
        assertTrue(reverseOrder > 0);

        int equals = VEventHelper.compareTextProperties(left, left);
        assertTrue(equals == 0);

        int leftNull = VEventHelper.compareTextProperties(null, right);
        assertTrue(leftNull < 0);

        int rightNull = VEventHelper.compareTextProperties(left, null);
        assertTrue(rightNull > 0);

        int allNull = VEventHelper.compareTextProperties(null, null);
        assertTrue(allNull == 0);

        int leftNullProperty = VEventHelper.compareTextProperties(nullProperty, right);
        assertTrue(leftNullProperty < 0);

        int rightNullProperty = VEventHelper.compareTextProperties(left, nullProperty);
        assertTrue(rightNullProperty > 0);

        int allNullProperty = VEventHelper.compareTextProperties(nullProperty, nullProperty);
        assertTrue(allNullProperty == 0);
    }

    @Test
    public void testCompareByStart() {
        final Date leftValue = new Date();
        final Date rightValue = new Date(leftValue.getTime() + 1000);

        final DateStart left = new DateStart(leftValue);
        final DateStart right = new DateStart(rightValue);

        int correctOrder = VEventHelper.compareByStart(left, right);
        assertTrue(correctOrder < 0);

        int reverseOrder = VEventHelper.compareByStart(right, left);
        assertTrue(reverseOrder > 0);

        int equals = VEventHelper.compareByStart(left, left);
        assertEquals(0, equals);

        int leftNull = VEventHelper.compareByStart(null, right);
        assertTrue(leftNull < 0);

        int rightNull = VEventHelper.compareByStart(left, null);
        assertTrue(rightNull > 0);

        int allNull = VEventHelper.compareByStart(null, null);
        assertTrue(allNull == 0);
    }

    @Test
    public void testCompareByEnd() {
        final Date leftValue = new Date();
        final Date rightValue = new Date(leftValue.getTime() + 1000);

        final DateEnd left = new DateEnd(leftValue);
        final DateEnd right = new DateEnd(rightValue);

        int correctOrder = VEventHelper.compareByEnd(left, right);
        assertTrue(correctOrder < 0);

        int reverseOrder = VEventHelper.compareByEnd(right, left);
        assertTrue(reverseOrder > 0);

        int equals = VEventHelper.compareByEnd(left, left);
        assertEquals(0, equals);

        int leftNull = VEventHelper.compareByEnd(null, right);
        assertTrue(leftNull < 0);

        int rightNull = VEventHelper.compareByEnd(left, null);
        assertTrue(rightNull > 0);

        int allNull = VEventHelper.compareByEnd(null, null);
        assertTrue(allNull == 0);
    }

    @Test
    public void testCompareByDuration() {
        final int durationLeft = 1000;
        final int durationRight = 2000;

        final Date leftStartValue = new Date();
        final Date leftEndValue = new Date(leftStartValue.getTime() + durationLeft);

        final DateStart leftStart = new DateStart(leftStartValue);
        final DateEnd leftEnd = new DateEnd(leftEndValue);

        final Date rightStartValue = new Date();
        final Date rightEndValue = new Date(rightStartValue.getTime() + durationRight);

        final DateStart rightStart = new DateStart(rightStartValue);
        final DateEnd rightEnd = new DateEnd(rightEndValue);

        final Date nullDate = null;
        final DateStart startNull = new DateStart(nullDate);
        final DateEnd endNull = new DateEnd(nullDate);

        class TestDuration {
            private DateStart leftStart;
            private DateEnd leftEnd;
            private DateStart rightStart;
            private DateEnd rightEnd;

            private int expected;

            private TestDuration(DateStart leftStart, DateEnd leftEnd, DateStart rightStart, DateEnd rightEnd, int expected) {
                this.leftStart = leftStart;
                this.leftEnd = leftEnd;
                this.rightStart = rightStart;
                this.rightEnd = rightEnd;
                this.expected = expected;
            }
        }

        LinkedList<TestDuration> testCases = new LinkedList<>();

        testCases.add(new TestDuration(null, null, null, null, 0)); // 0
        testCases.add(new TestDuration(leftStart, null, null, null, 0));
        testCases.add(new TestDuration(null, leftEnd, null, null, 0));
        testCases.add(new TestDuration(null, null, rightStart, null, 0));
        testCases.add(new TestDuration(null, null, null, rightEnd, 0));

        testCases.add(new TestDuration(leftStart, leftEnd, null, null, 1)); // 5
        testCases.add(new TestDuration(null, leftEnd, rightStart, null, -1));
        testCases.add(new TestDuration(null, null, rightStart, rightEnd, -1));
        testCases.add(new TestDuration(leftStart, null, null, rightEnd, -1));

        testCases.add(new TestDuration(leftStart, leftEnd, rightStart, null, 1)); // 9
        testCases.add(new TestDuration(leftStart, leftEnd, null, rightEnd, 1));
        testCases.add(new TestDuration(leftStart, null, rightStart, rightEnd, -1));
        testCases.add(new TestDuration(null, leftEnd, rightStart, rightEnd, -1));

        testCases.add(new TestDuration(startNull, endNull, startNull, endNull, 0)); // 13
        testCases.add(new TestDuration(leftStart, endNull, startNull, endNull, 0));
        testCases.add(new TestDuration(startNull, leftEnd, startNull, endNull, 0));
        testCases.add(new TestDuration(startNull, endNull, rightStart, endNull, 0));
        testCases.add(new TestDuration(startNull, endNull, startNull, rightEnd, 0));

        testCases.add(new TestDuration(leftStart, leftEnd, startNull, endNull, durationLeft)); // 18
        testCases.add(new TestDuration(startNull, leftEnd, rightStart, endNull, 0));
        testCases.add(new TestDuration(startNull, endNull, rightStart, rightEnd, -durationRight));
        testCases.add(new TestDuration(leftStart, endNull, startNull, rightEnd, 0));

        testCases.add(new TestDuration(leftStart, leftEnd, rightStart, rightEnd, durationLeft - durationRight)); // 22
        testCases.add(new TestDuration(rightStart, rightEnd, leftStart, leftEnd, durationRight - durationLeft));

        for (int i = 0; i < testCases.size(); i++) {
            TestDuration testDuration = testCases.get(i);
            int result = VEventHelper.compareByDuration(testDuration.leftStart, testDuration.leftEnd, testDuration.rightStart, testDuration.rightEnd);
            assertEquals("Test " + i, testDuration.expected, result);
        }
    }

    @Test
    public void testConcatTextProperties() {
        final String leftValue = "B-1.120";
        final String rightValue = "D-1.100";
        final String delimiter = ", ";
        final String nullString = null;

        final TextProperty left = new TextProperty(leftValue);
        final TextProperty right = new TextProperty(rightValue);
        final TextProperty nullProperty = new TextProperty(nullString);

        class TestCaseConcat {
            private TextProperty left;
            private TextProperty right;
            private String delimiter;

            private String expected;

            private TestCaseConcat(TextProperty left, TextProperty right, String delimiter, String expected) {
                this.left = left;
                this.right = right;
                this.delimiter = delimiter;
                this.expected = expected;
            }
        }

        LinkedList<TestCaseConcat> testCases = new LinkedList<>();

        testCases.add(new TestCaseConcat(null, null, delimiter, ""));
        testCases.add(new TestCaseConcat(nullProperty, nullProperty, delimiter, ""));
        testCases.add(new TestCaseConcat(left, null, delimiter, leftValue));
        testCases.add(new TestCaseConcat(null, right, delimiter, rightValue));
        testCases.add(new TestCaseConcat(left, right, delimiter, leftValue + delimiter + rightValue));
        testCases.add(new TestCaseConcat(left, right, null, leftValue + rightValue));
        testCases.add(new TestCaseConcat(left, nullProperty, delimiter, leftValue));
        testCases.add(new TestCaseConcat(nullProperty, right, delimiter, rightValue));

        for (int i = 0; i < testCases.size(); i++) {
            TestCaseConcat testCaseConcat = testCases.get(i);
            String result = VEventHelper.concatTextProperties(testCaseConcat.left, testCaseConcat.right, testCaseConcat.delimiter);
            assertEquals("Test " + i, testCaseConcat.expected, result);
        }
    }

    @SuppressWarnings("ConstantConditions")
    @Test
    public void testIsFullDay() {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.set(Calendar.YEAR, 2016);
        gregorianCalendar.set(Calendar.MONTH, Calendar.OCTOBER);
        gregorianCalendar.set(Calendar.HOUR_OF_DAY, 0);
        gregorianCalendar.set(Calendar.MINUTE, 0);
        gregorianCalendar.set(Calendar.SECOND, 0);
        gregorianCalendar.set(Calendar.MILLISECOND, 0);

        gregorianCalendar.set(Calendar.DAY_OF_MONTH, 15);
        Date midnight = gregorianCalendar.getTime();

        gregorianCalendar.set(Calendar.DAY_OF_MONTH, 16);
        Date midnightPlus24 = gregorianCalendar.getTime();

        Date nullDate = null;

        class TestCaseFullDay {
            private DateStart start;
            private DateEnd end;

            private boolean expected;

            private TestCaseFullDay(DateStart start, DateEnd end, boolean expected) {
                this.start = start;
                this.end = end;
                this.expected = expected;
            }
        }

        LinkedList<TestCaseFullDay> testCases = new LinkedList<>();

        testCases.add(new TestCaseFullDay(null, null, false));
        testCases.add(new TestCaseFullDay(new DateStart(midnight), null, false));
        testCases.add(new TestCaseFullDay(null, new DateEnd(midnightPlus24), false));
        testCases.add(new TestCaseFullDay(new DateStart(nullDate), new DateEnd(nullDate), false));
        testCases.add(new TestCaseFullDay(new DateStart(midnight), new DateEnd(nullDate), false));
        testCases.add(new TestCaseFullDay(new DateStart(nullDate), new DateEnd(midnightPlus24), false));
        testCases.add(new TestCaseFullDay(new DateStart(midnight), new DateEnd(midnightPlus24), true));


        gregorianCalendar.set(Calendar.DAY_OF_MONTH, 15);
        gregorianCalendar.set(Calendar.HOUR_OF_DAY, 12);
        Date lunch = gregorianCalendar.getTime();

        gregorianCalendar.set(Calendar.DAY_OF_MONTH, 16);
        Date lunchPlus24 = gregorianCalendar.getTime();

        testCases.add(new TestCaseFullDay(new DateStart(lunch), new DateEnd(lunchPlus24), false));

        for (int i = 0; i < testCases.size(); i++) {
            TestCaseFullDay testCaseFullDay = testCases.get(i);
            assertEquals("Test " + i, testCaseFullDay.expected, VEventHelper.isFullDay(testCaseFullDay.start, testCaseFullDay.end));
        }
    }

    @Test
    public void testIsMultiDay() {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.set(Calendar.YEAR, 2016);
        gregorianCalendar.set(Calendar.MONTH, Calendar.OCTOBER);
        gregorianCalendar.set(Calendar.HOUR_OF_DAY, 0);
        gregorianCalendar.set(Calendar.MINUTE, 0);
        gregorianCalendar.set(Calendar.SECOND, 0);
        gregorianCalendar.set(Calendar.MILLISECOND, 0);

        gregorianCalendar.set(Calendar.DAY_OF_MONTH, 15);
        Date midnight = gregorianCalendar.getTime();
        DateStart midnightStart = new DateStart(midnight);

        gregorianCalendar.set(Calendar.DAY_OF_MONTH, 16);
        Date midnightPlus24 = gregorianCalendar.getTime();
        DateEnd midnightPlus24End = new DateEnd(midnightPlus24);

        Date nullDate = null;
        DateStart nullStart = new DateStart(nullDate);
        DateEnd nullEnd = new DateEnd(nullDate);

        class TestCaseFullDay {
            private DateStart start;
            private DateEnd end;

            private boolean expected;

            private TestCaseFullDay(DateStart start, DateEnd end, boolean expected) {
                this.start = start;
                this.end = end;
                this.expected = expected;
            }
        }

        LinkedList<TestCaseFullDay> testCases = new LinkedList<>();

        testCases.add(new TestCaseFullDay(null, null, false));
        testCases.add(new TestCaseFullDay(midnightStart, null, false));
        testCases.add(new TestCaseFullDay(null, midnightPlus24End, false));
        testCases.add(new TestCaseFullDay(nullStart, nullEnd, false));
        testCases.add(new TestCaseFullDay(midnightStart, nullEnd, false));
        testCases.add(new TestCaseFullDay(nullStart, midnightPlus24End, false));
        testCases.add(new TestCaseFullDay(midnightStart, midnightPlus24End, false));


        gregorianCalendar.set(Calendar.DAY_OF_MONTH, 15);
        gregorianCalendar.set(Calendar.HOUR_OF_DAY, 12);
        Date lunch = gregorianCalendar.getTime();

        gregorianCalendar.set(Calendar.DAY_OF_MONTH, 16);
        Date lunchPlus24 = gregorianCalendar.getTime();

        testCases.add(new TestCaseFullDay(new DateStart(lunch), new DateEnd(lunchPlus24), false));

        gregorianCalendar.set(Calendar.HOUR_OF_DAY, 2);

        testCases.add(new TestCaseFullDay(new DateStart(lunch), new DateEnd(gregorianCalendar.getTime()), true));

        for (int i = 0; i < testCases.size(); i++) {
            TestCaseFullDay testCaseFullDay = testCases.get(i);
            assertEquals("Test " + i, testCaseFullDay.expected, VEventHelper.isMultiDay(testCaseFullDay.start, testCaseFullDay.end));
        }
    }

}