package de.stuv_mosbach.stuvapp.settings;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import de.stuv_mosbach.stuvapp.R;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Test if the {@link SettingsActivity} can be started.
 *
 * @author Peer Beckmann
 */
@RunWith(AndroidJUnit4.class)
public class SettingsActivityTest {
    @Rule
    public ActivityTestRule<SettingsActivity> activityRule = new ActivityTestRule<>(SettingsActivity.class);

    @Test
    public void SkipTest() {
        onView(withId(R.id.tool_bar)).perform(click());
    }
}