package de.stuv_mosbach.stuvapp.widget;

import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import de.stuv_mosbach.stuvapp.R;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by pbeckmann on 06.05.16.
 */
@RunWith(AndroidJUnit4.class)
public class ConfigureActivityTest {

    @Rule
    public final ActivityTestRule<ConfigureActivity> activityRule = new ActivityTestRule<>(ConfigureActivity.class);

    @Before
    public void setUp() throws Exception {
        Intent intent = new Intent(InstrumentationRegistry.getTargetContext(), ConfigureActivity.class);
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, 1);
        activityRule.launchActivity(intent);
    }

    @Test
    public void cancelTest() {
        onView(withId(R.id.button_cancel)).perform(click());
    }

    @Test
    public void acceptTest() {
        onView(withId(R.id.button_ready)).perform(click());
    }

}