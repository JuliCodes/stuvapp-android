package de.stuv_mosbach.stuvapp.util;

import android.accounts.Account;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import de.stuv_mosbach.stuvapp.MainActivity;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class AccountHelperTest {

    @BeforeClass
    public static void oneTimeSetUp() throws Exception {
        Intent intent = new Intent(InstrumentationRegistry.getTargetContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        InstrumentationRegistry.getContext().startActivity(intent);
    }

    @Test
    public void testGetAccount() throws Exception {
        Account account = AccountHelper.getAccount(InstrumentationRegistry.getTargetContext());
        assertEquals(account.type, ConfigGlobal.ACCOUNT_TYPE);
        assertEquals(account.name, ConfigGlobal.ACCOUNT);
    }
}