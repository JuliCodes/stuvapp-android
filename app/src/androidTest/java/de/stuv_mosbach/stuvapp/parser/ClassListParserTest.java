package de.stuv_mosbach.stuvapp.parser;

import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import de.stuv_mosbach.stuvapp.MainActivity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class ClassListParserTest {
    private ClassListParser parser;

    @BeforeClass
    public static void oneTimeSetUp() {
        Intent intent = new Intent(InstrumentationRegistry.getTargetContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        InstrumentationRegistry.getContext().startActivity(intent);
    }

    @Test
    public void testDownload() throws Exception {
        parser = ClassListParser.getInstance(InstrumentationRegistry.getTargetContext());
        assertFalse(parser.getDownloadActive());
        parser.startDownload();
        assertTrue(parser.getDownloadActive());
        parser.cancelDownload();
        while (parser.getDownloadActive()) {
            Thread.sleep(50);
        }
        assertFalse(parser.getDownloadActive());
        assertTrue(parser.existClassList());
    }

    @Test
    public void testExistClassList() throws Exception {
        parser = ClassListParser.getInstance(InstrumentationRegistry.getTargetContext());
        assertFalse(parser.getDownloadActive());
        parser.startDownload();
        while (parser.getDownloadActive()) {
            Thread.sleep(50);
        }
        assertTrue(parser.existClassList());
    }

    @Test
    public void testParse() throws Exception {
        parser = ClassListParser.getInstance(InstrumentationRegistry.getTargetContext().getApplicationContext());
        parser.startDownload();
        while (parser.getDownloadActive()) {
            Thread.sleep(50);
        }
        assertTrue(parser.parse());
        assertNotNull(parser.getClassIds());
        assertNotNull(parser.getKeys());
        assertNotNull(parser.getValues());
        assertEquals(parser.getKeys().length, parser.getValues().length);
        assertEquals(parser.getClassIds().size(), parser.getKeys().length);
    }
}