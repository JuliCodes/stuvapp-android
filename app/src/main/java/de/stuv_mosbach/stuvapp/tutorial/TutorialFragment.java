package de.stuv_mosbach.stuvapp.tutorial;

import android.app.Fragment;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;

import de.stuv_mosbach.stuvapp.R;
import de.stuv_mosbach.stuvapp.navigation.MySwipeRefreshLayout;

/**
 * Created by kevin on 13.06.2016.
 */
public class TutorialFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.tutorial, container, false);
        view.findViewById(R.id.title).setVisibility(View.GONE);

        //FadeInAnimation beim Fragmentwechsel
        AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(200);
        view.startAnimation(anim);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        MySwipeRefreshLayout swipeRefreshLayout = (MySwipeRefreshLayout) getActivity().findViewById(R.id.swiperefresh);
        swipeRefreshLayout.setEnabled(false);
    }
}
