package de.stuv_mosbach.stuvapp.webview;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Fragment;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import de.stuv_mosbach.stuvapp.R;

/**
 * Diese Klasse ist ein generisches WebView Fragment. Es wird in der App an verschiedenen Stellen
 * eingesetzt, um WebInhalte anzuzeigen. Je nach Zweck, kann man verschiedene Fähigkeiten der WebView
 * aktivieren oder deaktiviert lassen.
 *
 * @author Peer Beckmann
 */
public class WebViewFragment extends Fragment {

    private String url;
    private Boolean js;
    private Boolean builtInZoom;

    /**
     * Diese Funktion erstellt die eigentliche WebView. Hierbei nutzen wir die Parameter, die mit den
     * Settern unten an die Klasse übergeben werden können.
     *
     * @param inflater           Ein LayoutInflater des Systems, der uns aus einer XML Datei ein Objekt erstellt.
     * @param container          Der Container, in den die WebView eingefügt wird.
     * @param savedInstanceState Wenn gegeben ein Bundle mit verschiedenen Elementen aus einer vorherigen Instanz. Wird hier ignoriert.
     * @return Die letztendliche fertige View
     */
    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.simple_webview, container, false);

        WebView webView = (WebView) view.findViewById(R.id.webview);

        webView.loadUrl(url);

        // Updaten, animieren und ausblenden des Fortschrittbalkens
        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                RelativeLayout parent = (RelativeLayout) view.getParent();
                final ProgressBar progressBar = (ProgressBar) parent.findViewById(R.id.page_progress);
                ObjectAnimator animation = ObjectAnimator.ofInt(progressBar, "progress", progress);
                animation.setDuration(300); // .3 second
                animation.setInterpolator(new AccelerateDecelerateInterpolator());
                animation.start();
                if (progress == 100) {
                    progressBar.animate()
                            .alpha(0)
                            .setDuration(400)
                            .setListener(new AnimatorListenerAdapter() {
                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    progressBar.setVisibility(View.GONE);
                                }
                            });
                }

            }
        });

        if (js != null && js)
            webView.getSettings().setJavaScriptEnabled(true);

        if (builtInZoom != null && builtInZoom)
            webView.getSettings().setBuiltInZoomControls(true);

        return view;
    }

    /**
     * Die URL, die in der WebView letztendlich geöffnet werden soll.
     *
     * @param url String der eine vollständige URL enthalten muss.
     */
    public void setUrl(@NonNull String url) {
        this.url = url;
    }

    /**
     * Hier muss true übergeben werden, wenn die WebView eingeschaltetes Javascript haben soll. Da dies
     * ein Risiko mit sich bringt, ist es standardmäßig deaktiviert.
     *
     * @param js True, wenn Javascript aktiviert werden soll.
     */
    public void setJs(Boolean js) {
        this.js = js;
    }

    /**
     * Hier muss true übergeben werden, wenn der Nutzer durch Gesten oder andere Systemfeatures auf der
     * Website scrollen können soll.
     *
     * @param builtInZoom True, wenn die Systemeigenen Zoom funktionen aktiviert werden sollen.
     */
    public void setBuiltInZoom(Boolean builtInZoom) {
        this.builtInZoom = builtInZoom;
    }
}
