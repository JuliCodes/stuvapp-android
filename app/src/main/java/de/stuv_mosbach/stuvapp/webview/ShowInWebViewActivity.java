package de.stuv_mosbach.stuvapp.webview;

import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import de.stuv_mosbach.stuvapp.R;
import de.stuv_mosbach.stuvapp.util.ThemeFunctions;

/**
 * Diese Klasse stellt eine generische Activity zur Verfügung, die das {@link WebViewFragment}
 * einbindet und die Einstellungen entsprechend weiterreicht.
 */
public class ShowInWebViewActivity extends AppCompatActivity {
    public static final String KEY_URL = "url";
    public static final String KEY_TITLE = "title";
    public static final String KEY_ENABLE_JS = "JS";
    public static final String KEY_ENABLE_BUILTINZOOM = "BUILTINZOOM";

    private Bundle savedState;

    /**
     * Die Funktion kümmert sich um die Erstellung des Fragmentes und die Initialisierung der restlichen
     * Oberflächenelementen.
     *
     * @param savedInstanceState Wenn vorhanden ein Bundle mit Elementen aus einer vorherigen Instanz. Wird hier ignoeriert.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        savedState = savedInstanceState;

        ThemeFunctions.changeTheme(this, PreferenceManager.getDefaultSharedPreferences(this));

        setContentView(R.layout.simple_fragment_container);

        WebViewFragment webviewFragment = new WebViewFragment();

        if (savedState == null) {
            savedState = getIntent().getExtras();
        }

        String url = savedState.getString(KEY_URL);
        if (url == null)
            url = "";

        String title = savedState.getString(KEY_TITLE);
        if (title == null)
            title = "";

        webviewFragment.setUrl(url);
        webviewFragment.setJs(savedState.getBoolean(KEY_ENABLE_JS));
        webviewFragment.setBuiltInZoom(savedState.getBoolean(KEY_ENABLE_BUILTINZOOM));

        setTitle(title);

        getFragmentManager().beginTransaction()
                .add(R.id.contentPane, webviewFragment, "webview")
                .commit();

        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putAll(savedState);
        super.onSaveInstanceState(outState);
    }
}
