package de.stuv_mosbach.stuvapp.startup;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatSeekBar;
import androidx.appcompat.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import com.heinrichreimersoftware.materialintro.app.SlideFragment;

import java.util.Calendar;

import de.stuv_mosbach.stuvapp.MainActivity;
import de.stuv_mosbach.stuvapp.R;
import de.stuv_mosbach.stuvapp.settings.PreferenceKeys;

/**
 * Ask some minor settings from the user.
 *
 * @author Peer Beckmann
 */
public class PreferenceStartFragment extends SlideFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View root = inflater.inflate(R.layout.intro_preference_fragment, container, false);
        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());

        final int currentHour1 = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        final boolean itsNight1 = (currentHour1 > 18 || currentHour1 < 7);

        RadioGroup themepicker = (RadioGroup) root.findViewById(R.id.themePicker);
        final String oldTheme = sharedPreferences.getString(PreferenceKeys.THEME, "Light");
        switch (oldTheme) {
            case "Black":
                ((RadioButton) root.findViewById(R.id.radiobuttonBlack)).setChecked(true);
                root.findViewById(R.id.nightOnlyCard).setVisibility(View.VISIBLE);
                break;
            case "Dark":
                ((RadioButton) root.findViewById(R.id.radiobuttonDark)).setChecked(true);
                root.findViewById(R.id.nightOnlyCard).setVisibility(View.VISIBLE);
                break;
            default:
                ((RadioButton) root.findViewById(R.id.radiobuttonLight)).setChecked(true);
                break;
        }
        themepicker.setOnCheckedChangeListener((group, checkedId) -> {
            String newTheme = (String) root.findViewById(group.getCheckedRadioButtonId()).getTag();
            sharedPreferences.edit().putString(PreferenceKeys.THEME, newTheme).apply();
            root.findViewById(R.id.nightOnlyCard).setVisibility(View.VISIBLE);
            if (!newTheme.equals(oldTheme) && (!sharedPreferences.getBoolean(PreferenceKeys.DARK_THEME_NIGHT_ONLY, false) || itsNight1)) {
                Intent intent2 = new Intent(getActivity(), MainActivity.class);
                intent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent2);
            }
        });
        SwitchCompat nightOnlySwitch = ((SwitchCompat) root.findViewById(R.id.switch_nightOnly));
        final Boolean oldNightOnly = sharedPreferences.getBoolean(PreferenceKeys.DARK_THEME_NIGHT_ONLY, false);
        nightOnlySwitch.setChecked(oldNightOnly);
        nightOnlySwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            sharedPreferences.edit().putBoolean(PreferenceKeys.DARK_THEME_NIGHT_ONLY, isChecked).apply();

            if ((oldNightOnly != isChecked) && !itsNight1 && (oldTheme.equals("Dark")
                    || oldTheme.equals("Black"))) {
                getActivity().recreate();
            }
        });
        AppCompatSeekBar seekbar = ((AppCompatSeekBar) root.findViewById(R.id.seekbar));
        final TextView seekbardNumber = ((TextView) root.findViewById(R.id.seekbarNumber));
        String oldAmount = sharedPreferences.getString(PreferenceKeys.AMOUNT_OF_DAYS, "2");
        Log.d("tag", "oldAmount: " + oldAmount);
        seekbar.setProgress(Integer.valueOf(oldAmount) - 1);
        seekbardNumber.setText(String.valueOf(Integer.valueOf(oldAmount)));
        seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progress += 1;
                //Progress jeweils plus eins weil die Seekbar bei 0 anfängt, wir können aber ja schlecht 0 Tage auf der Aktuell Seite anzeigen
                seekbardNumber.setText(String.valueOf(progress));
                sharedPreferences.edit().putString(PreferenceKeys.AMOUNT_OF_DAYS, String.valueOf(progress)).apply();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        return root;
    }
}
