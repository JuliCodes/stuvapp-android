package de.stuv_mosbach.stuvapp.startup;

import android.content.ContentResolver;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.heinrichreimersoftware.materialintro.app.SlideFragment;

import de.stuv_mosbach.stuvapp.R;
import de.stuv_mosbach.stuvapp.parser.ClassListParser;
import de.stuv_mosbach.stuvapp.settings.PreferenceKeys;
import de.stuv_mosbach.stuvapp.util.AccountHelper;
import de.stuv_mosbach.stuvapp.util.ConfigGlobal;

/**
 * Created by pbeckmann on 09.06.16.
 */
public class ClassChooseFragment extends SlideFragment {

    private ClassListParser listParser;
    private Handler handler;
    private ListView listView;
    private TextView errorMessage;
    private CardView listContainer;

    private ProgressBar progressBar;

    private final Runnable downloading = new Runnable() {
        @Override
        public void run() {
            if (listParser.getDownloadActive()) {
                handler.postDelayed(this, 250);
            } else if (listParser.parse()) {
                progressBar.setVisibility(View.GONE);
                listView.setVisibility(View.VISIBLE);
                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, listParser.getKeys());
                listView.setAdapter(arrayAdapter);
                listView.setOnItemClickListener((parent, view, position, id) -> {
                    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
                    sharedPreferences.edit().putString(PreferenceKeys.CLASS, listParser.getClassIds().get(listParser.getKeys()[position])).apply();
                    sharedPreferences.edit().putString(PreferenceKeys.CLASS_VALUE, listParser.getKeys()[position]).apply();

                    Bundle settingsBundle = new Bundle();
                    settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
                    settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
                    ContentResolver.requestSync(AccountHelper.getAccount(getContext()), ConfigGlobal.AUTHORITY, settingsBundle);

                    nextSlide();
                });
                errorMessage.setVisibility(View.GONE);
            } else {
                listContainer.setVisibility(View.GONE);
                errorMessage.setVisibility(View.VISIBLE);
            }
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        listParser = ClassListParser.getInstance(getActivity().getApplicationContext());

        View root = inflater.inflate(R.layout.chooseclass_fragment, container, false);
        listView = (ListView) root.findViewById(R.id.list_view);
        errorMessage = (TextView) root.findViewById(R.id.error_no_network);
        listContainer = (CardView) root.findViewById(R.id.listViewContainer);
        progressBar = (ProgressBar) root.findViewById(R.id.progressBar);

        handler = new Handler();
        handler.post(downloading);

        return root;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        handler.removeCallbacks(downloading);
    }
}
