package de.stuv_mosbach.stuvapp.startup;

import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.heinrichreimersoftware.materialintro.app.SlideFragment;

import de.stuv_mosbach.stuvapp.R;
import de.stuv_mosbach.stuvapp.settings.PreferenceKeys;

/**
 * Created by pbeckmann on 08.06.16.
 */
public class DisclaimerFragment extends SlideFragment {

    private boolean accepted = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.disclaimer_fragment, container, false);

        ((CheckBox) root.findViewById(R.id.accept_disclaimer)).setOnCheckedChangeListener((buttonView, isChecked) -> {
            accepted = isChecked;
            updateNavigation();
            PreferenceManager.getDefaultSharedPreferences(getContext()).edit().putBoolean(PreferenceKeys.DISCLAIMER, true).apply();
        });

        return root;
    }

    @Override
    public boolean canGoForward() {
        return accepted;
    }
}
