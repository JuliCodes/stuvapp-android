package de.stuv_mosbach.stuvapp.startup;

import android.accounts.Account;
import android.content.ContentResolver;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.viewpager.widget.ViewPager;
import android.util.Log;

import com.heinrichreimersoftware.materialintro.app.IntroActivity;
import com.heinrichreimersoftware.materialintro.app.SlideFragment;
import com.heinrichreimersoftware.materialintro.slide.FragmentSlide;

import org.chaosdaten.commons.DateHelper;

import java.util.Calendar;

import de.stuv_mosbach.stuvapp.R;
import de.stuv_mosbach.stuvapp.parser.ClassListParser;
import de.stuv_mosbach.stuvapp.settings.PreferenceKeys;
import de.stuv_mosbach.stuvapp.util.AccountHelper;
import de.stuv_mosbach.stuvapp.util.ConfigGlobal;
import de.stuv_mosbach.stuvapp.util.ThemeFunctions;

/**
 * StartUp Screen. Initialer Download der Kursliste und Abfrage, welchen Kurs der Student besucht.
 */
public class Startup extends IntroActivity {
    private int position = 0;
    private int backgroundColor = R.color.backgroundColor;
    private int accentColor = R.color.dividerColor;
    private int lastPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        ThemeFunctions.changeTheme(this, sharedPreferences);
        final boolean nightOnly = sharedPreferences.getBoolean(PreferenceKeys.DARK_THEME_NIGHT_ONLY, false);
        final int currentHour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        final boolean itsNight = (currentHour > 18 || currentHour < 7);

        String theme = sharedPreferences.getString(PreferenceKeys.THEME, "Light");

        if (theme.equals("Dark") && (itsNight || !nightOnly)) {
            accentColor = R.color.colorPrimaryDark;
            backgroundColor = R.color.backgroundColorDark;
        } else if (theme.equals("Black") && (itsNight || !nightOnly)) {
            accentColor = R.color.amoledThemeStatusbarColor;
            backgroundColor = R.color.black;
        }

        lastPosition = sharedPreferences.getInt(PreferenceKeys.INTRO_POSITION, 0);

        addStartupSlide(new WelcomeFragment());
        addStartupSlide(new DisclaimerFragment());
        addStartupSlide(new ClassChooseFragment());
        addStartupSlide(new PreferenceStartFragment());
        addStartupSlide(new TutorialFragment());
        addStartupSlide(new IntroInfosFragment());

        setButtonBackVisible(false);

        setButtonNextVisible(true);
        setButtonNextFunction(BUTTON_NEXT_FUNCTION_NEXT_FINISH);

        setPagerIndicatorVisible(false);

        ClassListParser.getInstance(this).startDownload();

        Account account = AccountHelper.getAccount(this);

        ContentResolver.setIsSyncable(
                account,
                ConfigGlobal.AUTHORITY,
                1
        );

        ContentResolver.setSyncAutomatically(
                account,
                ConfigGlobal.AUTHORITY,
                true);

        ContentResolver.addPeriodicSync(
                account,
                ConfigGlobal.AUTHORITY,
                Bundle.EMPTY,
                Long.valueOf(sharedPreferences.getString(PreferenceKeys.SYNC_INTERVAL, String.valueOf(DateHelper.HOUR_IN_SEC * 6))));

        addOnPageChangeListener(new ViewPager.OnPageChangeListener(){
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }
            @Override
            public void onPageSelected(int position) {
                sharedPreferences.edit().putInt(PreferenceKeys.INTRO_POSITION, position + lastPosition).apply();
                Log.d("POSITION", String.valueOf(position));
            }
            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    private void addStartupSlide(SlideFragment slide) {
        if (position++ >= lastPosition) {
            addSlide(new FragmentSlide.Builder()
                    .background(backgroundColor)
                    .backgroundDark(accentColor)
                    .fragment(slide)
                    .build());
        }
    }
}