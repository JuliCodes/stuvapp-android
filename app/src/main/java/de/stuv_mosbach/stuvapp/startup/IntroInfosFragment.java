package de.stuv_mosbach.stuvapp.startup;

import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.heinrichreimersoftware.materialintro.app.SlideFragment;

import de.stuv_mosbach.stuvapp.R;

/**
 * Created by chaos on 9/26/16.
 */

public class IntroInfosFragment extends SlideFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.intro_infos, container, false);
    }
}
