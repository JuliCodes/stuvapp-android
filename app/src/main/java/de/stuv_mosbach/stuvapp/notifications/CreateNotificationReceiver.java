package de.stuv_mosbach.stuvapp.notifications;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import androidx.core.app.NotificationCompat;

import de.stuv_mosbach.stuvapp.R;
import de.stuv_mosbach.stuvapp.contentProvider.MessageContract;
import de.stuv_mosbach.stuvapp.util.ConfigGlobal;

/**
 * @author Peer Beckmann <peer@pbeckmann.de>
 */
public class CreateNotificationReceiver extends BroadcastReceiver {

    public static final String MESSAGE_ID = "messageID";

    private final String[] projection = new String[]{
            MessageContract.MessageEntry.COLUMN_TITLE,
            MessageContract.MessageEntry.COLUMN_CONTENT
    };

    @Override
    public void onReceive(Context context, Intent intent) {
        int uid = intent.getIntExtra(MESSAGE_ID, -1);

        ContentResolver cr = context.getContentResolver();
        Cursor cursor = cr.query(
                MessageContract.MessageEntry.CONTENT_URI,
                projection,
                MessageContract.MessageEntry.COLUMN_UID + " == ?",
                new String[]{String.valueOf(uid)},
                null);

        if (cursor == null || cursor.getCount() == 0)
            return;

        cursor.moveToFirst();

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, ConfigGlobal.NOTIFICATION_CHANNEL_ID);
        builder.setContentTitle(cursor.getString(0));
        builder.setContentText(cursor.getString(1));
        builder.setSmallIcon(R.drawable.ic_school_white_24dp);
        builder.setAutoCancel(true);

        builder.setStyle(new NotificationCompat.BigTextStyle()
                .bigText(cursor.getString(1)));


        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(uid, builder.build());

        cursor.close();
    }
}
