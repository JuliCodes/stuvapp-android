package de.stuv_mosbach.stuvapp.notifications;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;

import java.util.Date;

import de.stuv_mosbach.stuvapp.contentProvider.MessageContract;

/**
 * Dieser Receiver überprüft nach einem Reboot des Handys ob es in der Zukunft Termine gibt, welche
 * Benachrichtigungen erfordern.
 *
 * @author Peer Beckmann
 */
public class BootReceiver extends BroadcastReceiver {

    private final String[] projection = new String[]{
            MessageContract.MessageEntry.COLUMN_TITLE,
            MessageContract.MessageEntry.COLUMN_CONTENT,
            MessageContract.MessageEntry.COLUMN_BEGIN,
            MessageContract.MessageEntry.COLUMN_UID
    };

    /**
     * Diese Funktion wird aufgerufen, wenn die App die Benachrichtigung des Systems erhält, dass ein
     * Boot Vorgang komplettiert wurde. Zuerst werden alle Nachrichten aus der Datenbank ausgelesen,
     * anschließend wird überprüft ob die Priorität der Nachricht 1 entspricht. Wenn das der Fall ist,
     * wird im AlarmManager des System ein Intent für den {@link CreateNotificationReceiver} hinterlegt.
     *
     * @param context Der Context in dem die App läuft.
     * @param intent  Der Intent, der den Receiver aufruft.
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        if (!intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED))
            return;
        ContentResolver cr = context.getContentResolver();
        Cursor cursor = cr.query(MessageContract.MessageEntry.CONTENT_URI,
                projection,
                MessageContract.MessageEntry.COLUMN_BEGIN + " >  ? AND " + MessageContract.MessageEntry.COLUMN_PRIORITY + " == ?",
                new String[]{
                        String.valueOf(new Date().getTime()),
                        "1"
                },
                null);

        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();

            do {
                Intent createIntent = new Intent(context, CreateNotificationReceiver.class);
                createIntent.putExtra("MESSAGE_ID", cursor.getInt(3));
                PendingIntent pendingIntent = PendingIntent.getBroadcast(context, cursor.getPosition(), createIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

                alarmManager.set(AlarmManager.RTC, cursor.getLong(2), pendingIntent);
            } while (cursor.moveToNext());
        }

        if (cursor != null) {
            cursor.close();
        }
    }
}
