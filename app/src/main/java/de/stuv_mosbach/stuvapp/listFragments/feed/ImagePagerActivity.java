package de.stuv_mosbach.stuvapp.listFragments.feed;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;

import com.koushikdutta.async.future.Future;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONException;

import de.stuv_mosbach.stuvapp.R;
import de.stuv_mosbach.stuvapp.util.HackyViewPager;
import de.stuv_mosbach.stuvapp.util.ThemeFunctions;
import uk.co.senab.photoview.PhotoView;

/**
 * Created by pbeckmann on 8/15/16.
 */
public class ImagePagerActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ThemeFunctions.changeTheme(this, PreferenceManager.getDefaultSharedPreferences(this));
        setContentView(R.layout.fragment_viewpager);
        ViewPager viewPager = (HackyViewPager) findViewById(R.id.view_pager);
        SimplePagerAdapter pagerAdapter = new SimplePagerAdapter();
        pagerAdapter.setImageName(getIntent().getStringExtra("IMAGES"));
        viewPager.setAdapter(pagerAdapter);
    }

    static class SimplePagerAdapter extends PagerAdapter {
        private JSONArray imageName;

        private void setImageName(String imageName) {
            try {
                this.imageName = new JSONArray(imageName);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getCount() {
            return imageName.length();
        }

        @Override
        public View instantiateItem(ViewGroup container, int position) {
            PhotoView photoView = new PhotoView(container.getContext());
            try {
                Future<Bitmap> image = Ion
                        .with(container.getContext())
                        .load((String) imageName.get(position))
                        .asBitmap();

                photoView.setImageDrawable(new BitmapDrawable(container.getContext().getResources(), image.get()));
                // Now just add PhotoView to ViewPager and return it
                container.addView(photoView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return photoView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

    }
}
