package de.stuv_mosbach.stuvapp.listFragments.lectures;

import android.content.Context;
import android.database.Cursor;
import android.os.Build;
import android.preference.PreferenceManager;
import androidx.core.content.ContextCompat;
import androidx.cardview.widget.CardView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import de.stuv_mosbach.stuvapp.R;
import de.stuv_mosbach.stuvapp.contentProvider.HomeContract;
import de.stuv_mosbach.stuvapp.listFragments.ParentCursorAdapter;
import de.stuv_mosbach.stuvapp.util.ConfigGlobal;
import de.stuv_mosbach.stuvapp.veventutils.VEventFormatHelper;

/**
 * @author Peer Beckmann
 */
class LectureCursorAdapter extends ParentCursorAdapter{

    private static final int COLUMN_TITLE = 1;
    private static final int COLUMN_LOCATION = 2;
    private static final int COLUMN_BEGIN = 3;
    private static final int COLUMN_END = 4;
    private static final int COLUMN_FULL_DAY = 5;
    private static final int COLUMN_DESCRIPTION = 6;
    private static final int COLUMN_MULTI_DAY = 7;
    private static final int COLUMN_DELETED = 8;
    private static final int COLUMN_NEW = 9;
    private static final int COLUMN_CHANGED_LOCATION = 10;
    private static final int COLUMN_CHANGED_DATE = 11;

    /**
     * Just push the data to the SuperClass
     *
     * @param context The context of the adapter.
     * @param c       The cursor with the necessary data.
     */
    LectureCursorAdapter(Context context, Cursor c) {
        super(context, c);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return inflater.inflate(R.layout.item_lectures, parent, false);
    }

    @SuppressWarnings("deprecation")
    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        VEventFormatHelper formatHelper = new VEventFormatHelper.Builder()
                .setSummary(                    cursor.getString(COLUMN_TITLE))
                .setDescription(                cursor.getString(COLUMN_DESCRIPTION))
                .setLocation(                   cursor.getString(COLUMN_LOCATION))
                .setOrigin(                     HomeContract.KEY_LECTURES)
                .setStart(                      cursor.getLong(COLUMN_BEGIN))
                .setEnd(                        cursor.getLong(COLUMN_END))
                .setFirstAtDay(                 isFirstAtDay(cursor))
                .setAllDay(                     cursor.getInt(COLUMN_FULL_DAY) == 1)
                .setMultiDay(                   cursor.getInt(COLUMN_MULTI_DAY) == 1)
                .setFLAG_NEW(                   cursor.getInt(COLUMN_NEW) == 1)
                .setFLAG_DELETED(               cursor.getInt(COLUMN_DELETED) == 1)
                .setFLAG_CHANGED_LOCATION(      cursor.getInt(COLUMN_CHANGED_LOCATION) == 1)
                .setFLAG_CHANGED_DATE(          cursor.getInt(COLUMN_CHANGED_DATE) == 1)
                .setSharedPreferences(          PreferenceManager.getDefaultSharedPreferences(context))
                .setResources(                  context.getResources())
                .build();

        view.setOnLongClickListener(View::showContextMenu);
        view.setLongClickable(true);

        // Layout
        CardView cardView = (CardView) view.findViewById(R.id.card_view);
        cardView.setTag(formatHelper.getOrigin());
        cardView.setMaxCardElevation(0);
        if (formatHelper.isFirstAtDay()) {
            view.setPadding(0, ConfigGlobal.dpToPx(7), 0, 0);
        } else {
            view.setPadding(0, 0, 0, 0);
        }

        // Header
        TextView header = (TextView) view.findViewById(R.id.header);
        // TODO: Optimize the FastScroll, so this function must not be called every element.
        header.setText(formatHelper.getHeader());
        if (formatHelper.isFirstAtDay()) {
            header.setVisibility(View.VISIBLE);
        } else {
            header.setVisibility(View.GONE);
        }

        // Summary
        TextView eventSummary = (TextView) view.findViewById(R.id.event_header);
        eventSummary.setText(formatHelper.getSummary());

        if (formatHelper.isFLAG_DELETED()) {
            eventSummary.setTextColor(ContextCompat.getColor(context, android.R.color.holo_red_light));
        } else if (formatHelper.isFLAG_NEW()) {
            eventSummary.setTextColor(ContextCompat.getColor(context, android.R.color.holo_green_light));
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                eventSummary.setTextAppearance(android.R.style.TextAppearance_Medium);
            } else {
                //noinspection deprecation
                eventSummary.setTextAppearance(context, android.R.style.TextAppearance_Medium);
            }
        }

        // Description
        TextView eventDescription = (TextView) view.findViewById(R.id.event_description);
        if (formatHelper.isDescription()) {
            eventDescription.setText(formatHelper.getDescription());
            eventDescription.setVisibility(View.VISIBLE);
        } else {
            eventDescription.setVisibility(View.GONE);
        }

        // Location
        TextView eventLocation = (TextView) view.findViewById(R.id.event_location);
        if (formatHelper.isLocation()) {
            eventLocation.setText(formatHelper.getLocation());
            eventLocation.setVisibility(View.VISIBLE);
            if (formatHelper.isFLAG_CHANGED_LOCATION()) {
                eventLocation.setTextColor(ContextCompat.getColor(context, android.R.color.holo_orange_dark));
            } else {
                eventLocation.setTextAppearance(context, android.R.style.TextAppearance_Small);
            }
        } else {
            eventLocation.setVisibility(View.GONE);
        }

        // Date information
        TextView eventDates = (TextView) view.findViewById(R.id.event_data_range);
        eventDates.setText(formatHelper.getFormattedDates());
        if (formatHelper.isFLAG_CHANGED_DATE()) {
            eventDates.setTextColor(ContextCompat.getColor(context, android.R.color.holo_orange_dark));
        } else {
            eventDates.setTextAppearance(context, android.R.style.TextAppearance_Small);
        }
    }
}
