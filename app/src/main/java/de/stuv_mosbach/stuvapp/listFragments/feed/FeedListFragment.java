package de.stuv_mosbach.stuvapp.listFragments.feed;

import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import org.chaosdaten.commons.CursorLoaderBuilder;

import de.stuv_mosbach.stuvapp.R;
import de.stuv_mosbach.stuvapp.contentProvider.FeedContract;
import de.stuv_mosbach.stuvapp.listFragments.ParentListFragment;

/**
 * Create the feed listview, so the {@link FeedCursorAdapter}
 */
public class FeedListFragment extends ParentListFragment implements AdapterView.OnItemClickListener {

    private static final String[] FEED_PROJECTION = new String[]{
            FeedContract.FeedEntry._ID,
            FeedContract.FeedEntry.COLUMN_TITLE,
            FeedContract.FeedEntry.COLUMN_DESCRIPTION,
            FeedContract.FeedEntry.COLUMN_IMAGE_NAME,
            FeedContract.FeedEntry.COLUMN_CONTENT,
            FeedContract.FeedEntry.COLUMN_PUBDATE
    };

    public FeedListFragment() {
        super();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getLoaderManager().initLoader(0, new Bundle(), this);

        adapter = new FeedCursorAdapter(getActivity(), null);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.listview, container, false);
        if (view == null) return null;
        ListView listView = (ListView) view.findViewById(android.R.id.list);
        listView.setDividerHeight(0);
        listView.setSelector(new ColorDrawable(Color.TRANSPARENT));
        listView.setAdapter(adapter);

        View emptyView = view.findViewById(R.id.empty_view);
        ((TextView) emptyView.findViewById(R.id.header_title)).setText(getString(R.string.emptyview_title_nonews));
        ((TextView) emptyView.findViewById(R.id.body_text)).setText(getString(R.string.emptyview_messages_nonews));

        listView.setEmptyView(emptyView);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getListView().setOnItemClickListener(this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        CursorLoaderBuilder builder = new CursorLoaderBuilder(getActivity());
        builder.setUri(FeedContract.FeedEntry.CONTENT_URI)
                .setProjection(FEED_PROJECTION)
                .addSortOrder(FeedContract.FeedEntry.COLUMN_PUBDATE + " desc");

        return builder.build();
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        adapter.changeCursor(cursor);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(getActivity(), FeedActivity.class);
        intent.putExtra("ID", id);
        startActivity(intent);
    }
}