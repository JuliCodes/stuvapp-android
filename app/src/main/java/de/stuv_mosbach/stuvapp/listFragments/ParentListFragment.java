package de.stuv_mosbach.stuvapp.listFragments;

import android.animation.Animator;
import android.app.ListFragment;
import android.app.LoaderManager;
import android.content.Loader;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Menu;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.AbsListView;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import de.stuv_mosbach.stuvapp.R;
import de.stuv_mosbach.stuvapp.contentProvider.DBHelper;
import de.stuv_mosbach.stuvapp.contentProvider.LectureContract;
import de.stuv_mosbach.stuvapp.contentProvider.MeetingContract;
import de.stuv_mosbach.stuvapp.util.ConfigGlobal;

/**
 * @author Peer Beckmann
 */
abstract public class ParentListFragment extends ListFragment implements LoaderManager.LoaderCallbacks<Cursor>, AbsListView.OnScrollListener {

    protected ParentCursorAdapter adapter;

    private int previousFirstVisibleItem = 0;
    private long previousEventTime = 0;
    private boolean isFadingIn;

    public ParentListFragment() {
        setHasOptionsMenu(true);
    }

    public static String getEventStartTime() {
        Calendar cal = new GregorianCalendar(Locale.getDefault());
        Long actualTime = cal.getTimeInMillis() - ConfigGlobal.timeAfterEventIsHidden;
        return String.valueOf(actualTime);
    }

    /**
     * Wenn das Scrollen aufhört wird der StickyHeader ausgeblendet
     *
     * @param scrollState
     *          0: Kein Scrollen/Scrollen beendet
     *          1: Es wird gescrollt
     *          2: Der Finger berührt nicht mehr den Bildschirm aber das Scrollen hat noch "Schwung"
     */
    @Override
    public void onScrollStateChanged(AbsListView absListView, int scrollState) {
        if (scrollState==0){

            //Umrechnen von DP in Pixel für das FadeIn des StickyHeaders
            Resources ressources = getResources();
            float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 60, ressources.getDisplayMetrics());
            getActivity().findViewById(R.id.scrollDateCard).animate().translationY(-px).setDuration(500).setStartDelay(500).setInterpolator(new AccelerateInterpolator()).start();
        }
    }


    /**
     * Wird beim Scrollen der Liste aufgerufen. Blendet den StickyHeader ein wenn schnell gescrollt wird und aktualisiert das Datum im StickyHeader.
     */
    @Override
    public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        //Berechnen der Scrollgeschwindigkeit um darauf basierend den StickyHeader anzuzeigen
        if (previousFirstVisibleItem != firstVisibleItem) {
            long currTime = System.currentTimeMillis();
            long timeToScrollOneElement = currTime - previousEventTime;
            double speed = ((double) 1 / timeToScrollOneElement) * 1000;

            previousFirstVisibleItem = firstVisibleItem;
            previousEventTime = currTime;

            if (speed > 15 && !isFadingIn) {
                isFadingIn = true;
                getActivity().findViewById(R.id.scrollDateCard).animate().setStartDelay(0).translationY(0.0f).setDuration(150).setInterpolator(new DecelerateInterpolator()).setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {}

                    @Override
                    public void onAnimationEnd(Animator animator) {
                        isFadingIn = false;
                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {
                        isFadingIn = false;
                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {}
                }).start();
            }
        }

        //Aus dem momentan obersten Child wird das Datum gelesen, der Wochentag abschnitten und in den Header eingetragen
        if (absListView.getChildAt(1) != null) {
            String date = ((TextView) ((absListView.getChildAt(1)).findViewById(R.id.header))).getText().toString();
            try {
                DateFormat format = new SimpleDateFormat("EEE dd.MM.yyyy", Locale.getDefault());
                Date date1 = format.parse(date);
                DateFormat format1 = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
                String dateShortened = format1.format(date1);
                if (getActivity().findViewById(R.id.scrollDate) != null) {
                    ((TextView) getActivity().findViewById(R.id.scrollDate)).setText(dateShortened);
                }
            } catch (ParseException e) {
                if (getActivity().findViewById(R.id.scrollDate) != null) {
                    ((TextView) getActivity().findViewById(R.id.scrollDate)).setText(date);
                }
            }
        }
    }

    /**
     * Hier definieren wir in den Child-Klassen, mitr welchen Parametern und wie die einzelnen
     * Querys gebaut werden sollen.
     *
     * @param id   Die Id des Querys.
     * @param args Argumente, die wir an den Query übergeben wollen.
     * @return Cursor Loader, auf dessen Aktionen wir dann mit den anderen Callbacks überwachen.
     */
    @Override
    abstract public Loader<Cursor> onCreateLoader(int id, Bundle args);

    /**
     * Lasst uns bei neuen Daten, die Daten im Cursor ersetzen.
     *
     * @param loader Eine Referenz zum loader.
     */
    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        adapter.changeCursor(null);
    }


    /**
     * Hier schauen wir, ob wir den Clean Up Button anzeigen müssen.
     */
    public class GetCleanState extends AsyncTask<Integer, Object, Cursor> {
        public final int TYPE_LECTURES = 0;
        public final int TYPE_MEETINGS = 1;
        public final int TYPE_BOTH = 2;

        DBHelper dbHelper;
        SQLiteDatabase database;
        Menu menu;

        public void setMenu(Menu menu) {
            this.menu = menu;
        }

        /**
         * smoothScrollToPosition(0);
         * Öffnen der Datenbank.
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dbHelper = new DBHelper(getActivity());
            database = dbHelper.getReadableDatabase();
        }

        /**
         * Durchführen der Datenbankabfrage.
         *
         * @param params Ignorieren wir hier.
         * @return Cursor, der dann an {@link #onPostExecute(Cursor)} übergeben wird.
         */
        @Override
        protected Cursor doInBackground(Integer... params) {
            switch (params[0]) {
                case TYPE_LECTURES:
                    return database.rawQuery("SELECT " + LectureContract.LectureEntry.COLUMN_TITLE + " FROM " + LectureContract.LectureEntry.TABLE_NAME + " WHERE " + LectureContract.LectureEntry.COLUMN_DELETED + " = 1 OR " + LectureContract.LectureEntry.COLUMN_NEW + " = 1 OR " + LectureContract.LectureEntry.COLUMN_CHANGED_DATE + " = 1 OR " + LectureContract.LectureEntry.COLUMN_CHANGED_LOCATION + " = ?", null);
                case TYPE_MEETINGS:
                    return database.rawQuery("SELECT " + MeetingContract.MeetingEntry.COLUMN_TITLE + " FROM " + MeetingContract.MeetingEntry.TABLE_NAME + " WHERE " + MeetingContract.MeetingEntry.COLUMN_NEW + " = 1 OR " + MeetingContract.MeetingEntry.COLUMN_DELETED + " = 1 OR " + MeetingContract.MeetingEntry.COLUMN_CHANGED_DATE + " = 1 OR " + MeetingContract.MeetingEntry.COLUMN_CHANGED_LOCATION + " = ?", null);
                case TYPE_BOTH:
                default:
                    return database.rawQuery("SELECT " + LectureContract.LectureEntry.COLUMN_TITLE + " FROM " + LectureContract.LectureEntry.TABLE_NAME + " WHERE " + LectureContract.LectureEntry.COLUMN_DELETED + " = 1 OR " + LectureContract.LectureEntry.COLUMN_NEW + " = 1 OR " + LectureContract.LectureEntry.COLUMN_CHANGED_DATE + " = 1 OR " + LectureContract.LectureEntry.COLUMN_CHANGED_LOCATION + " = ? UNION SELECT " +
                            MeetingContract.MeetingEntry.COLUMN_TITLE + " FROM " + MeetingContract.MeetingEntry.TABLE_NAME + " WHERE " + MeetingContract.MeetingEntry.COLUMN_NEW + " = 1 OR " + MeetingContract.MeetingEntry.COLUMN_DELETED + " = 1  OR " + LectureContract.LectureEntry.COLUMN_CHANGED_DATE + " = 1 OR " + LectureContract.LectureEntry.COLUMN_CHANGED_LOCATION + " = ?", null);
            }
        }

        /**
         * Wenn es Einträge gibt, die neu/gelöscht sind, zeige das Menuicon an.
         *
         * @param result Der Cursor aus {@link #doInBackground(Object...)}.
         */
        @Override
        protected void onPostExecute(Cursor result) {
            if (result.getCount() > 0) {
                menu.findItem(R.id.action_cleanup).setVisible(true);
            } else {
                menu.findItem(R.id.action_cleanup).setVisible(false);
            }
            database.close();
            dbHelper.close();
        }
    }
}
