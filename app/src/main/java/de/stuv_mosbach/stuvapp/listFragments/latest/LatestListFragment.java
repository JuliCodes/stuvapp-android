package de.stuv_mosbach.stuvapp.listFragments.latest;

import android.app.ListFragment;
import android.app.LoaderManager;
import android.content.Context;
import android.content.Loader;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import org.chaosdaten.commons.CursorLoaderBuilder;
import org.chaosdaten.commons.DateHelper;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import de.stuv_mosbach.stuvapp.R;
import de.stuv_mosbach.stuvapp.contentProvider.HomeContract;
import de.stuv_mosbach.stuvapp.contentProvider.LectureContract;
import de.stuv_mosbach.stuvapp.contentProvider.MessageContract;
import de.stuv_mosbach.stuvapp.listFragments.ParentListFragment;
import de.stuv_mosbach.stuvapp.settings.PreferenceKeys;
import de.stuv_mosbach.stuvapp.util.ThemeFunctions;

public class LatestListFragment extends ListFragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private static final String[] PROJECTION = new String[]{
            LectureContract.LectureEntry._ID,
            LectureContract.LectureEntry.COLUMN_TITLE,
            LectureContract.LectureEntry.COLUMN_LOCATION,
            LectureContract.LectureEntry.COLUMN_BEGIN,
            LectureContract.LectureEntry.COLUMN_END,
            LectureContract.LectureEntry.COLUMN_FULL_DAY,
            LectureContract.LectureEntry.COLUMN_DESCRIPTION,
            LectureContract.LectureEntry.COLUMN_MORE_DAYS,
            "origin"
    };
    private LatestCursorAdapter adapter;
    private onClickEvent callback;
    private Handler handler;
    private final Runnable refreshStatusBar = new Runnable() {
        @Override
        public void run() {
            adapter.notifyDataSetChanged();
            handler.postDelayed(this, 1000 * 60);
        }
    };
    private int amountDays;

    private Cursor messageCursor;
    private SharedPreferences sharedPref;

    public void setCallback(onClickEvent callback) {
        this.callback = callback;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getLoaderManager().initLoader(0, new Bundle(), LatestListFragment.this);
        adapter = new LatestCursorAdapter(getActivity(), null);
        setListAdapter(adapter);

        messageCursor = new CursorLoaderBuilder(getActivity())
                .setUri(MessageContract.MessageEntry.CONTENT_URI)
                .orQuery(MessageContract.MessageEntry.COLUMN_BEGIN + " <  ? AND " + MessageContract.MessageEntry.COLUMN_END + " > ?")
                .addQueryArgument(String.valueOf(System.currentTimeMillis()))
                .addQueryArgument(String.valueOf(System.currentTimeMillis()))
                .build(getActivity().getContentResolver());

        sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        amountDays = Integer.valueOf(sharedPref.getString(PreferenceKeys.AMOUNT_OF_DAYS, "2"));

        handler = new Handler();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.listview, container, false);

        ListView listView = (ListView) view.findViewById(android.R.id.list);
        if (listView == null) return null;

        View emptyView = view.findViewById(R.id.empty_view);
        if (emptyView == null) return view;

        TextView emptyBodyText = (TextView) emptyView.findViewById(R.id.body_text);
        ImageView emptyBodyImage = (ImageView) emptyView.findViewById(R.id.body_image);

        if (sharedPref.getString(PreferenceKeys.CLASS, null) == null) {
            emptyBodyText.setText(getString(R.string.emptyview_messages_noclassid));
        } else {
            if (amountDays > 1)
                emptyBodyText.setText(getString(R.string.emptyview_messages_noevents).replace("%s", String.valueOf(amountDays)));

            if (amountDays == 1)
                emptyBodyText.setText(getString(R.string.emptyview_messages_noeventstoday));

            emptyBodyImage.setImageResource(R.drawable.ic_wb_sunny_black_48dp);
            emptyBodyImage.setOnClickListener(v -> {
                RotateAnimation rotateAnimation = new RotateAnimation(v.getRotation(),v.getRotation()+360.0f,RotateAnimation.RELATIVE_TO_SELF,0.5f, RotateAnimation.RELATIVE_TO_SELF,0.47916f);
                rotateAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
                rotateAnimation.setDuration(1000);
                v.startAnimation(rotateAnimation);
            });
            AnimationSet animSet = new AnimationSet(true);
            animSet.addAnimation(new AlphaAnimation(0.5f, 1f));
            animSet.addAnimation(new RotateAnimation(-120, 0, RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.47916f));
            animSet.addAnimation(new ScaleAnimation(0.5f, 1f, 0.5f, 1f, ScaleAnimation.RELATIVE_TO_SELF, 0.5f, ScaleAnimation.RELATIVE_TO_SELF, 0.5f));
            animSet.setDuration(1000);
            animSet.setInterpolator(new DecelerateInterpolator());
            emptyBodyImage.startAnimation(animSet);
        }

        listView.setEmptyView(emptyView);

        View header = inflater.inflate(R.layout.lunchtime_message, listView, false);
        listView.addHeaderView(header);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        if (messageCursor.getCount() > 0) {
            View newView = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.message_view, null);
            messageCursor.moveToFirst();

            ((TextView) newView.findViewById(R.id.header_title)).setText(messageCursor.getString(1));
            ((TextView) newView.findViewById(R.id.body_text)).setText(messageCursor.getString(2));

            messageCursor.close();

            ThemeFunctions.colorMessageView(getActivity(), newView);

            getListView().addHeaderView(newView);
        }

        handler.postDelayed(refreshStatusBar, 1000 * 60);

        getListView().setOnItemClickListener((parent, view1, position, id) -> {
            if (view1.findViewById(R.id.card_view) != null) {
                if (view1.findViewById(R.id.card_view).getTag().equals(HomeContract.KEY_MEETINGS)) {
                    callback.clickHome(R.id.navigation_students);
                } else if (view1.findViewById(R.id.card_view).getTag().equals(HomeContract.KEY_LECTURES)) {
                    callback.clickHome(R.id.navigation_lectures);
                }
            }
        });
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        boolean hideStudyDays = PreferenceManager.getDefaultSharedPreferences(getActivity()).getBoolean(PreferenceKeys.HIDE_STUDY_DAYS, false);
        CursorLoaderBuilder builder = new CursorLoaderBuilder(getActivity());
        String eventStartTime = ParentListFragment.getEventStartTime();
        String eventEndTime = String.valueOf(DateHelper.getTimestampZeroClock(new Date(), Locale.getDefault()) + (long) amountDays * (long) DateHelper.DAY_IN_MILLI);
        builder.setUri(HomeContract.CONTENT_URI);
        builder.setProjection(PROJECTION);

        if (hideStudyDays) {
            builder.andQuery(LectureContract.LectureEntry.COLUMN_TITLE + " != ?");
            builder.addQueryArgument("Studientag");
        }

        builder.addBracket();

        builder.andQuery(LectureContract.LectureEntry.COLUMN_END + " BETWEEN ? AND ?");
        builder.addQueryArgument(eventStartTime);
        builder.addQueryArgument(eventEndTime);

        builder.orQuery(LectureContract.LectureEntry.COLUMN_BEGIN + " BETWEEN ? AND ?");
        builder.addQueryArgument(eventStartTime);
        builder.addQueryArgument(eventEndTime);

        builder.orQuery(LectureContract.LectureEntry.COLUMN_BEGIN + " < ? AND " + LectureContract.LectureEntry.COLUMN_END + " > ?");
        builder.addQueryArgument(eventStartTime);
        builder.addQueryArgument(eventEndTime);

        builder.addBracket();

        return builder.build();
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        adapter.changeCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        adapter.changeCursor(null);
    }

    @Override
    public void onResume() {
        super.onResume();

        //Zwischen 11 Uhr und 13 Uhr wird ein Nachricht angezeigt, die direkt zum Mensaplan führt. Außerhalb der Zeiten wird sie wieder ausgeblendet.
        final Calendar c = Calendar.getInstance();
        final int currentHour = c.get(Calendar.HOUR_OF_DAY);
        final int day = c.get(Calendar.DAY_OF_WEEK);
        final boolean isWeekday = ((day >= Calendar.MONDAY) && (day <= Calendar.FRIDAY));
        final boolean isTimeForLunch = currentHour >= 11 && currentHour < 13 && isWeekday;

        ListView listView = getListView();
        LinearLayout lunchtime = (LinearLayout) listView.findViewById(R.id.lunchtime_message);
        if (lunchtime != null) {
            View card = lunchtime.findViewById(R.id.card_view);
            if (isTimeForLunch) {
                card.setVisibility(View.VISIBLE);
            } else {
                card.setVisibility(View.GONE);
            }
        }

        getLoaderManager().restartLoader(0, new Bundle(), this);
    }

    @Override
    public void onPause() {
        super.onPause();
        handler.removeCallbacks(refreshStatusBar);
    }

    public interface onClickEvent {
        void clickHome(int type);
    }
}
