package de.stuv_mosbach.stuvapp.listFragments.latest;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.database.Cursor;
import android.os.Build;
import android.preference.PreferenceManager;
import androidx.core.content.ContextCompat;
import androidx.cardview.widget.CardView;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.chaosdaten.commons.DateHelper;

import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import de.stuv_mosbach.stuvapp.R;
import de.stuv_mosbach.stuvapp.listFragments.ParentCursorAdapter;
import de.stuv_mosbach.stuvapp.util.ConfigGlobal;
import de.stuv_mosbach.stuvapp.veventutils.VEventFormatHelper;

/**
 * @author Peer Beckmann
 */
public class LatestCursorAdapter extends ParentCursorAdapter {

    private static final int TYPE_NORMAL = 1;
    private static final int TYPE_TODAY = 2;
    private static final int TYPE_TOMORROW = 3;
    private static final int TYPE_FIRST = 0;
    private static final int TYPE_YESTERDAY = 4;

    private static final int COLUMN_TITLE = 1;
    private static final int COLUMN_LOCATION = 2;
    private static final int COLUMN_BEGIN = 3;
    private static final int COLUMN_END = 4;
    private static final int COLUMN_FULL_DAY = 5;
    private static final int COLUMN_DESCRIPTION = 6;
    private static final int COLUMN_MULTI_DAY = 7;
    private static final int COLUMN_ORIGIN = 8;

    public LatestCursorAdapter(Context context, Cursor c) {
        super(context, c);
    }

    /**
     * Der ViewType hat hier eine Option mehr, deswegen müssen wir die Funktion überschreiben.
     *
     * @param position Die Position des Elementes.
     * @return {@link #TYPE_TODAY} wenn es das erste Event am heutigen Tag ist, {@link #TYPE_TOMORROW}
     * , wenn es das erste Event am morgigen Tag ist und {@link #TYPE_NORMAL} für alles andere.
     */
    @Override
    public int getItemViewType(int position) {
        Cursor cursor = (Cursor) getItem(position);
        Boolean firstAtDay = isFirstAtDay(cursor);
        if (!firstAtDay)
            return TYPE_NORMAL;

        Date eventEnd = new Date();
        eventEnd.setTime(cursor.getLong(COLUMN_BEGIN));

        Date todayZeroClock = DateHelper.getDateZeroClock(new Date(), Locale.getDefault());

        Date yesterday = new Date(todayZeroClock.getTime());
        int offsetYesterday = TimeZone.getTimeZone("Europe/Berlin").getOffset(yesterday.getTime());
        yesterday = new Date(yesterday.getTime() - offsetYesterday);

        Date today = new Date(todayZeroClock.getTime() + DateHelper.DAY_IN_MILLI);
        int offsetToday = TimeZone.getTimeZone("Europe/Berlin").getOffset(today.getTime());
        today = new Date(today.getTime() - offsetToday);

        Date tomorrow = new Date(todayZeroClock.getTime() + DateHelper.DAY_IN_MILLI * 2);
        int offsetTomorrow = TimeZone.getTimeZone("Europe/Berlin").getOffset(tomorrow.getTime());
        tomorrow = new Date(tomorrow.getTime() - offsetTomorrow);

        if (eventEnd.before(yesterday))
            return TYPE_YESTERDAY;

        if (eventEnd.before(today))
            return TYPE_TODAY;

        if (eventEnd.before(tomorrow))
            return TYPE_TOMORROW;

        return TYPE_FIRST;
    }

    /**
     * Wir müssen das hier überschreiben, da wir hier 5 Typen und nicht nur 2 haben.
     */
    @Override
    public int getViewTypeCount() {
        return 5;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return inflater.inflate(R.layout.item_lectures, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        final VEventFormatHelper formatHelper = new VEventFormatHelper.Builder()
                .setSummary(                    cursor.getString(COLUMN_TITLE))
                .setDescription(                cursor.getString(COLUMN_DESCRIPTION))
                .setLocation(                   cursor.getString(COLUMN_LOCATION))
                .setOrigin(                     cursor.getString(COLUMN_ORIGIN))
                .setStart(                      cursor.getLong(COLUMN_BEGIN))
                .setEnd(                        cursor.getLong(COLUMN_END))
                .setFirstAtDay(                 isFirstAtDay(cursor))
                .setAllDay(                     cursor.getInt(COLUMN_FULL_DAY) == 1)
                .setMultiDay(                   cursor.getInt(COLUMN_MULTI_DAY) == 1)
                .setSharedPreferences(          PreferenceManager.getDefaultSharedPreferences(context))
                .setResources(                  context.getResources())
                .build();

        // Layout
        CardView cardView = (CardView) view.findViewById(R.id.card_view);
        cardView.setTag(formatHelper.getOrigin());
        cardView.setMaxCardElevation(0);
        if (formatHelper.isFirstAtDay()) {
            view.setPadding(0, ConfigGlobal.dpToPx(7), 0, 0);
        } else {
            view.setPadding(0, 0, 0, 0);
        }

        // Header
        TextView header = (TextView) view.findViewById(R.id.header);
        // TODO: Optimize the FastScroll, so this function must not be called every element.
        header.setText(formatHelper.getHeader());
        if (formatHelper.isFirstAtDay())
            header.setVisibility(View.VISIBLE);

        switch (getItemViewType(cursor.getPosition())) {
            case TYPE_YESTERDAY:
                header.setText(context.getString(R.string.latest_messages_yesterday));
                break;
            case TYPE_TOMORROW:
                header.setText(context.getString(R.string.latest_messages_tomorrow));
                break;
            case TYPE_TODAY:
                header.setText(context.getString(R.string.latest_messages_today));
                break;
        }

        // Summary
        TextView eventSummary = (TextView) view.findViewById(R.id.event_header);
        eventSummary.setText(formatHelper.getSummary());

        if (formatHelper.isFLAG_DELETED()) {
            eventSummary.setTextColor(ContextCompat.getColor(context, android.R.color.holo_green_light));
        } else if (formatHelper.isFLAG_NEW()) {
            eventSummary.setTextColor(ContextCompat.getColor(context, android.R.color.holo_red_light));
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                eventSummary.setTextAppearance(android.R.style.TextAppearance_Medium);
            } else {
                //noinspection deprecation
                eventSummary.setTextAppearance(context, android.R.style.TextAppearance_Medium);
            }
        }

        // Description
        TextView eventDescription = (TextView) view.findViewById(R.id.event_description);
        if (formatHelper.isDescription()) {
            eventDescription.setText(formatHelper.getDescription());
        } else {
            eventDescription.setVisibility(View.GONE);
        }

        // Location
        TextView eventLocation = (TextView) view.findViewById(R.id.event_location);
        if (formatHelper.isLocation()) {
            eventLocation.setText(formatHelper.getLocation());
        } else {
            eventLocation.setVisibility(View.GONE);
        }

        // Date information
        TextView eventDates = (TextView) view.findViewById(R.id.event_data_range);
        eventDates.setText(formatHelper.getFormattedDates());

        // ProgressBar
        ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressbar);

        if (formatHelper.isRunning() && !formatHelper.isAllDay() && !formatHelper.isMultiDay()) {
            progressBar.setVisibility(View.VISIBLE);
            progressBar.setMax((int) (formatHelper.getEnd().getTime() - formatHelper.getStart().getTime()) / 1000);
            int progress = (int) (System.currentTimeMillis() - formatHelper.getStart().getTime()) / 1000;

            // will update the "progress" propriety of seekbar until it reaches progress
            ObjectAnimator animation = ObjectAnimator.ofInt(progressBar, "progress", progress);
            animation.setDuration(1000); // 1 second
            animation.setInterpolator(new DecelerateInterpolator());
            animation.start();
        }
    }
}
