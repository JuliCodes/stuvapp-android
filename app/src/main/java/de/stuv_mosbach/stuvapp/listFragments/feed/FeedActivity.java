package de.stuv_mosbach.stuvapp.listFragments.feed;

/**
 * Created by pbeckmann on 8/21/16.
 */

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.koushikdutta.ion.Ion;

import org.chaosdaten.commons.CursorLoaderBuilder;
import org.json.JSONArray;

import de.stuv_mosbach.stuvapp.R;
import de.stuv_mosbach.stuvapp.contentProvider.FeedContract;
import de.stuv_mosbach.stuvapp.util.ThemeFunctions;

public class FeedActivity extends AppCompatActivity {

    /**
     * Wir initialisieren das Standard Layout, dann binden wir das Fragment in das Layout ein und
     * zuletzt erstellen wir die ActionBar.
     *
     * @param savedInstanceState Nachzulesen bei
     *                           {@link de.stuv_mosbach.stuvapp.MainActivity#onCreate(Bundle)}
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ThemeFunctions.changeTheme(this, PreferenceManager.getDefaultSharedPreferences(this));

        setContentView(R.layout.feed_element);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        if (bundle == null)
            throw new UnknownError();

        final Cursor cursor = new CursorLoaderBuilder(this)
                .setUri(FeedContract.FeedEntry.CONTENT_URI)
                .setProjection(new String[]{
                        FeedContract.FeedEntry.COLUMN_TITLE,
                        FeedContract.FeedEntry.COLUMN_CONTENT,
                        FeedContract.FeedEntry.COLUMN_IMAGE_NAME})
                .andQuery(FeedContract.FeedEntry._ID + "= ?")
                .addQueryArgument(String.valueOf(bundle.get("ID")))
                .build(getContentResolver());

        cursor.moveToFirst();

        setTitle(cursor.getString(0));
        TextView content = (TextView) findViewById(R.id.feed_content);

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            content.setText(Html.fromHtml(cursor.getString(1), Html.FROM_HTML_MODE_LEGACY,s -> new ColorDrawable(Color.TRANSPARENT), null));
        } else {
            //noinspection deprecation
            content.setText(Html.fromHtml(cursor.getString(1), s -> new ColorDrawable(Color.TRANSPARENT), null));
        }

        JSONArray array;
        try {
            array = new JSONArray(cursor.getString(2));
            LinearLayout linearLayout = (LinearLayout) findViewById(R.id.image_container);
            linearLayout.setOnClickListener(v -> {
                Intent intent1 = new Intent(getApplicationContext(), ImagePagerActivity.class);
                intent1.putExtra("IMAGES", cursor.getString(2));
                startActivity(intent1);
            });
            if (array.length() > 0) {
                for (int i = 0; i < array.length(); i++) {
                    ImageView view = new ImageView(this);
                    final LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
                    view.setAdjustViewBounds(true);
                    view.setLayoutParams(params);
                    linearLayout.addView(view);
                    Ion.with(view).load((String) array.get(i));
                }
            } else {
                findViewById(R.id.scroll_card).setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
