package de.stuv_mosbach.stuvapp.listFragments.lectures;

import android.content.Loader;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.appcompat.widget.SearchView;
import android.text.TextUtils;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import org.chaosdaten.commons.CursorLoaderBuilder;

import de.stuv_mosbach.stuvapp.R;
import de.stuv_mosbach.stuvapp.contentProvider.LectureContract;
import de.stuv_mosbach.stuvapp.listFragments.ParentListFragment;
import de.stuv_mosbach.stuvapp.settings.PreferenceKeys;
import de.stuv_mosbach.stuvapp.util.ConfigGlobal;

/**
 * The fragment displaying the lectures.
 *
 * @author Peer Beckmann
 */
public class LectureListFragment extends ParentListFragment
        implements SearchView.OnQueryTextListener, SearchView.OnCloseListener {
    private static final String[] PROJECTION = new String[]{
            LectureContract.LectureEntry._ID,
            LectureContract.LectureEntry.COLUMN_TITLE,
            LectureContract.LectureEntry.COLUMN_LOCATION,
            LectureContract.LectureEntry.COLUMN_BEGIN,
            LectureContract.LectureEntry.COLUMN_END,
            LectureContract.LectureEntry.COLUMN_FULL_DAY,
            LectureContract.LectureEntry.COLUMN_DESCRIPTION,
            LectureContract.LectureEntry.COLUMN_MORE_DAYS,
            LectureContract.LectureEntry.COLUMN_DELETED,
            LectureContract.LectureEntry.COLUMN_NEW,
            LectureContract.LectureEntry.COLUMN_CHANGED_LOCATION,
            LectureContract.LectureEntry.COLUMN_CHANGED_DATE};
    private static int amountOfNextEntries;
    private static boolean showHistory;
    private static String searchQuery;
    private SearchView searchView;
    private SharedPreferences sharedPref;

    private int position;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getLoaderManager().initLoader(0, new Bundle(), LectureListFragment.this);

        //Initialize the adapter the first time.
        adapter = new LectureCursorAdapter(getActivity(), null);
        setListAdapter(adapter);

        sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;

        position = info.position;

        Cursor cursor = (Cursor) adapter.getItem(position);
        if (-1 == position) {
            return;
        }

        LinearLayout layout = new LinearLayout(getActivity());
        layout.setOrientation(LinearLayout.VERTICAL);
        int padding = ConfigGlobal.dpToPx(15);
        layout.setPadding(padding, padding, padding, 0);

        TextView title = new TextView(getActivity());
        title.setText(cursor.getString(cursor.getColumnIndex(LectureContract.LectureEntry.COLUMN_TITLE)));
        title.setTextAppearance(getActivity(), android.R.style.TextAppearance_Medium);
        layout.addView(title);

        TextView subTitle = new TextView(getActivity());
        subTitle.setText(getActivity().getResources().getString(R.string.tb_action_search_by));
        subTitle.setTextAppearance(getActivity(), android.R.style.TextAppearance_Small);
        layout.addView(subTitle);

        menu.setHeaderView(layout);

        getActivity().getMenuInflater().inflate(R.menu.lecture_context, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        Cursor cursor = (Cursor) adapter.getItem(position);
        searchView.setIconified(false);

        if (R.id.action_search_by_summary == item.getItemId()) {
            searchView.setQuery(cursor.getString(cursor.getColumnIndex(LectureContract.LectureEntry.COLUMN_TITLE)), true);
        } else if (R.id.action_search_by_description == item.getItemId()) {
            searchView.setQuery(cursor.getString(cursor.getColumnIndex(LectureContract.LectureEntry.COLUMN_DESCRIPTION)), true);
        } else if (R.id.action_search_by_location == item.getItemId()) {
            searchView.setQuery(cursor.getString(cursor.getColumnIndex(LectureContract.LectureEntry.COLUMN_LOCATION)), true);
        }

        return true;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.listview, container, false);
        ListView listView = (ListView) view.findViewById(android.R.id.list);

        registerForContextMenu(listView);

        if (listView == null) return null;

        View emptyView = view.findViewById(R.id.empty_view);
        if (emptyView == null) return view;

        TextView emptyBodyText = (TextView) emptyView.findViewById(R.id.body_text);
        ImageView emptyBodyImage = (ImageView) emptyView.findViewById(R.id.body_image);

        if (sharedPref.getString(PreferenceKeys.CLASS, null) == null) {
            emptyBodyText.setText(getString(R.string.emptyview_messages_noclassid));
        } else {
            emptyBodyText.setText(getString(R.string.emptyview_messages_nolectures));
            emptyBodyImage.setImageResource(R.drawable.ic_wb_sunny_black_48dp);
            emptyBodyImage.setOnClickListener(v -> {
                RotateAnimation rotateAnimation = new RotateAnimation(v.getRotation(),v.getRotation()+360.0f,RotateAnimation.RELATIVE_TO_SELF,0.5f, RotateAnimation.RELATIVE_TO_SELF,0.47916f);
                rotateAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
                rotateAnimation.setDuration(1000);
                v.startAnimation(rotateAnimation);
            });
            AnimationSet animSet = new AnimationSet(true);
            animSet.addAnimation(new AlphaAnimation(0.5f, 1f));
            animSet.addAnimation(new RotateAnimation(-120, 0, RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.47916f));
            animSet.addAnimation(new ScaleAnimation(0.5f, 1f, 0.5f, 1f, ScaleAnimation.RELATIVE_TO_SELF, 0.5f, ScaleAnimation.RELATIVE_TO_SELF, 0.5f));
            animSet.setDuration(1000);
            animSet.setInterpolator(new DecelerateInterpolator());
            emptyBodyImage.startAnimation(animSet);
        }

        listView.setEmptyView(emptyView);

        listView.setFastScrollEnabled(true);
        listView.setOnScrollListener(this);

        return view;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        Boolean hideStudyDays = preferences.getBoolean(PreferenceKeys.HIDE_STUDY_DAYS, false);

        CursorLoaderBuilder builder = new CursorLoaderBuilder(getActivity());
        builder.setUri(LectureContract.LectureEntry.CONTENT_URI);
        builder.setProjection(PROJECTION);

        if (!showHistory) {
            builder.andQuery(LectureContract.LectureEntry.COLUMN_END + " > ?");
            builder.addQueryArgument(ParentListFragment.getEventStartTime());
        }

        if (hideStudyDays) {
            builder.andQuery(LectureContract.LectureEntry.COLUMN_TITLE + " != ?");
            builder.addQueryArgument("Studientag");
        }

        //noinspection ConstantConditions
        if (!TextUtils.isEmpty(searchQuery)) {
            builder.addBracket();

            builder.orQuery(LectureContract.LectureEntry.COLUMN_TITLE + " LIKE ?");
            builder.addQueryArgument("%" + searchQuery + "%");

            builder.orQuery(LectureContract.LectureEntry.COLUMN_LOCATION + " LIKE ?");
            builder.addQueryArgument("%" + searchQuery + "%");

            builder.orQuery(LectureContract.LectureEntry.COLUMN_DESCRIPTION + " LIKE ?");
            builder.addQueryArgument("%" + searchQuery + "%");

            builder.addBracket();
        }

        return builder.build();
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        adapter.changeCursor(data);
        if (showHistory) {
            int amountOfAllEntries = data.getCount();
            getListView().setSelection(amountOfAllEntries - amountOfNextEntries);
            getListView().smoothScrollToPosition(amountOfAllEntries - amountOfNextEntries - 1);
        } else {
            amountOfNextEntries = data.getCount();
            getListView().setSelection(0);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, final MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.menu_extended, menu);

        GetCleanState getCleanState = new GetCleanState();
        getCleanState.setMenu(menu);
        getCleanState.execute(getCleanState.TYPE_LECTURES);

        MenuItem showHistoryItem = menu.findItem(R.id.action_show_history);

        PorterDuffColorFilter pdColorFilter;
        if (!showHistory) {
            showHistoryItem.setChecked(false);
            pdColorFilter = new PorterDuffColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
        } else {
            showHistoryItem.setChecked(true);
            pdColorFilter = new PorterDuffColorFilter(Color.BLACK, PorterDuff.Mode.SRC_ATOP);
        }
        showHistoryItem.getIcon().setColorFilter(pdColorFilter);

        searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setOnQueryTextListener(this);
        if (!TextUtils.isEmpty(searchQuery) && !searchView.getQuery().equals(searchQuery)) {
            searchView.setQuery(searchQuery, true);
            searchView.setIconified(false);
        }
        searchView.setOnCloseListener(this);
    }

    @Override
    public boolean onClose() {
        getActivity().invalidateOptionsMenu();
        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );
        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        searchView.clearFocus();
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        searchQuery = newText;
        getLoaderManager().restartLoader(0, new Bundle(), this);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_show_history) {
            PorterDuffColorFilter pdColorFilter;
            if (showHistory) {
                showHistory = false;
                item.setChecked(false);
                pdColorFilter = new PorterDuffColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
            } else {
                showHistory = true;
                item.setChecked(true);
                pdColorFilter = new PorterDuffColorFilter(Color.BLACK, PorterDuff.Mode.SRC_ATOP);
            }
            item.getIcon().setColorFilter(pdColorFilter);
            getLoaderManager().restartLoader(0, new Bundle(), this);
            return true;
        }
        return false;
    }
}