package de.stuv_mosbach.stuvapp.listFragments;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;

import org.chaosdaten.commons.DateHelper;

import java.util.Date;
import java.util.Locale;

/**
 * Dies ist die übergeordnete Klasse für die einzelnen ListViews. Sie enthält die wichtigsten
 * allgemeinen Funktionen, sowie Funktionen um Daten in die einzelnen Views einzufügen.
 *
 * @author Peer Beckmann
 */
abstract public class ParentCursorAdapter extends CursorAdapter {

    protected static final int TYPE_FIRST = 0;
    private static final int COLUMN_BEGIN = 3;
    private static final int TYPE_NORMAL = 1;

    protected final LayoutInflater inflater;

    /**
     * Konstruktor. Wir initializieren hier einen LayoutInflater.
     *
     * @param context Der Kontext, indem der Adapter laufen soll.
     * @param c       Der Cursor, aus dem die Daten stammen sollen.
     */
    protected ParentCursorAdapter(Context context, Cursor c) {
        super(context, c, true);
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    /**
     * Diese Funktion überprüft ob das Event an der Position das erste an dem Tag ist, oder nicht.
     *
     * @param cursor   Der Cursor, aus dem wir die Daten bekommen. Hierbei ist es nicht relevant an
     *                 welcher Position er steht.
     * @return True, wenn die Position die erste am Tag sein soll. Position 0 ist automatisch auch
     * immer der erste Event am Tag.
     */
    protected static boolean isFirstAtDay(Cursor cursor) {
        int position = cursor.getPosition();
        if (position == 0) {
            return true;
        }
        cursor.moveToPosition((position - 1));
        Date start1 = new Date();
        start1.setTime(cursor.getLong(COLUMN_BEGIN));
        cursor.moveToPosition(position);
        Date start2 = new Date();
        start2.setTime(cursor.getLong(COLUMN_BEGIN));
        return (!DateHelper.getDateZeroClock(start1, Locale.getDefault()).equals(DateHelper.getDateZeroClock(start2, Locale.getDefault())));
    }

    /**
     * Liefer zurück, was für einen Typ der View wir haben.
     *
     * @param position Die Position des Elementes.
     * @return Den Typ der View.
     */
    @Override
    public int getItemViewType(int position) {
        Cursor cursor = (Cursor) getItem(position);
        return (isFirstAtDay(cursor)) ? TYPE_FIRST : TYPE_NORMAL;
    }

    /**
     * Wie viele verschiedene Ansichten gibt es innerhalb der Liste.
     *
     * @return Bei den Child-Klassen gibt es nur 2 verschiedene Ansichten, außer in der Heute-Ansicht
     * deswegen wird dort die Funktion überschrieben.
     */
    @Override
    public int getViewTypeCount() {
        return 2;
    }

    /**
     * Hier müssen wir in den Child-Klassen die Ansicht aus dem XML bauen.
     *
     * @param context Der Kontext in dem die Ansicht später sehbar sein soll.
     * @param cursor  Der Cursor mit den Daten.
     * @param parent  Der parent in den wir die Ansicht einfügen wollen.
     * @return Die aufgebaute Ansicht.
     */
    @Override
    abstract public View newView(Context context, Cursor cursor, ViewGroup parent);

    /**
     * Hier füllen wir wir die Ansicht mit Daten.
     *
     * @param view    Die Ansicht, in das wir die Daten einfügen wollen.
     * @param context Der Context des ganzen Scheiß.
     * @param cursor  Der Cursor mit den Daten.
     */
    @Override
    abstract public void bindView(View view, Context context, Cursor cursor);
}
