package de.stuv_mosbach.stuvapp.listFragments.feed;

import android.content.Context;
import android.database.Cursor;
import android.os.Build;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.Date;

import de.stuv_mosbach.stuvapp.R;
import de.stuv_mosbach.stuvapp.listFragments.ParentCursorAdapter;
import de.stuv_mosbach.stuvapp.util.ConfigGlobal;

/**
 * Fill the views of the feed listview with content.
 */
public class FeedCursorAdapter extends ParentCursorAdapter {

    private static final int COLUMN_TITLE = 1;
    private static final int COLUMN_DESCRIPTION = 2;
    private static final int COLUMN_IMAGE_LINK = 3;
    private static final int COLUMN_PUBDATE = 5;

    public FeedCursorAdapter(Context context, Cursor c) {
        super(context, c);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return inflater.inflate(R.layout.item_feed_article, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ((TextView) view.findViewById(R.id.title)).setText(cursor.getString(COLUMN_TITLE));

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            ((TextView) view.findViewById(R.id.description)).setText(Html.fromHtml(cursor.getString(COLUMN_DESCRIPTION), Html.FROM_HTML_MODE_LEGACY));
        } else {
            //noinspection deprecation
            ((TextView) view.findViewById(R.id.description)).setText(Html.fromHtml(cursor.getString(COLUMN_DESCRIPTION)));
        }

        String images = cursor.getString(COLUMN_IMAGE_LINK);
        JSONArray array;
        try {
            array = new JSONArray(images);
            if (array.length() > 0) {
                ImageView imageView = (ImageView) view.findViewById(R.id.image);
                Ion.with(imageView)
                        .load((String) array.get(0));
                view.findViewById(R.id.image).setVisibility(View.VISIBLE);
            } else {
                view.findViewById(R.id.image).setVisibility(View.GONE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Date pubDate = new Date(cursor.getLong(COLUMN_PUBDATE));
        ((TextView) view.findViewById(R.id.date)).setText(ConfigGlobal.dayMonthYear.format(pubDate));
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public int getItemViewType(int position) {
        return TYPE_FIRST;
    }
}
