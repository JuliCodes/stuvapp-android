package de.stuv_mosbach.stuvapp.listFragments.meetings;

import android.content.Loader;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.appcompat.widget.SearchView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Transformation;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import org.chaosdaten.commons.CursorLoaderBuilder;

import de.stuv_mosbach.stuvapp.R;
import de.stuv_mosbach.stuvapp.contentProvider.MeetingContract;
import de.stuv_mosbach.stuvapp.listFragments.ParentListFragment;

/**
 * @author Peer Beckmann
 */
public class MeetingsListFragment extends ParentListFragment
        implements SearchView.OnQueryTextListener, SearchView.OnCloseListener, AdapterView.OnItemClickListener {

    private static final String[] PROJECTION = new String[]{
            MeetingContract.MeetingEntry._ID,
            MeetingContract.MeetingEntry.COLUMN_TITLE,
            MeetingContract.MeetingEntry.COLUMN_LOCATION,
            MeetingContract.MeetingEntry.COLUMN_BEGIN,
            MeetingContract.MeetingEntry.COLUMN_END,
            MeetingContract.MeetingEntry.COLUMN_FULL_DAY,
            MeetingContract.MeetingEntry.COLUMN_DESCRIPTION,
            MeetingContract.MeetingEntry.COLUMN_MORE_DAYS,
            MeetingContract.MeetingEntry.COLUMN_DELETED,
            MeetingContract.MeetingEntry.COLUMN_NEW,
            MeetingContract.MeetingEntry.COLUMN_CHANGED_LOCATION,
            MeetingContract.MeetingEntry.COLUMN_CHANGED_DATE
    };
    private static int amountOfNextEntries;
    private static boolean showHistory;
    private static String searchQuery;
    private SearchView searchView;

    private static void expand(final View v) {
        int matchParentMeasureSpec = View.MeasureSpec.makeMeasureSpec(((View) v.getParent()).getWidth(), View.MeasureSpec.EXACTLY);
        int wrapContentMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        v.measure(matchParentMeasureSpec, wrapContentMeasureSpec);
        final int targetHeight = v.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? LinearLayoutCompat.LayoutParams.WRAP_CONTENT
                        : (int) (targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        a.setDuration(300);
        a.setInterpolator(new DecelerateInterpolator());
        v.startAnimation(a);
    }

    private static void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        a.setDuration(300);
        a.setInterpolator(new DecelerateInterpolator());
        v.startAnimation(a);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        adapter = new MeetingsCursorAdapter(getActivity(), null);
        setListAdapter(adapter);

        getLoaderManager().initLoader(0, new Bundle(), this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.listview, container, false);
        if (view == null) return null;
        ListView listView = (ListView) view.findViewById(android.R.id.list);

        listView.setDividerHeight(0);
        listView.setSelector(new ColorDrawable(Color.TRANSPARENT));

        listView.setFastScrollEnabled(true);
        listView.setOnScrollListener(this);
        listView.setClipToPadding(true);

        final int paddingInDp = 4;
        final float scale = getResources().getDisplayMetrics().density;
        int paddingInPx = (int) (paddingInDp * scale + 0.5f);
        listView.setPadding(0, paddingInPx, 0 , paddingInPx);

        View emptyView = view.findViewById(R.id.empty_view);
        ((TextView) emptyView.findViewById(R.id.header_title)).setText(getString(R.string.emptyview_title_noevents));
        ((TextView) emptyView.findViewById(R.id.body_text)).setText(getString(R.string.emptyview_messages_nodata));

        listView.setEmptyView(emptyView);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getListView().setOnItemClickListener(this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        CursorLoaderBuilder builder = new CursorLoaderBuilder(getActivity());
        builder.setUri(MeetingContract.MeetingEntry.CONTENT_URI);
        builder.setProjection(PROJECTION);

        if (!showHistory) {
            builder.andQuery(MeetingContract.MeetingEntry.COLUMN_END + " > ?");
            builder.addQueryArgument(ParentListFragment.getEventStartTime());
        }

        if (!TextUtils.isEmpty(searchQuery)) {
            builder.andQuery(MeetingContract.MeetingEntry.COLUMN_TITLE + " LIKE ?");
            builder.addQueryArgument("%" + searchQuery + "%");

            builder.andQuery(MeetingContract.MeetingEntry.COLUMN_DESCRIPTION + " LIKE ?");
            builder.addQueryArgument("%" + searchQuery + "%");
        }

        return builder.build();
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        adapter.changeCursor(data);
        if (showHistory) {
            int amountOfAllEntries = data.getCount();
            getListView().setSelection(amountOfAllEntries - amountOfNextEntries);
            getListView().smoothScrollToPosition(amountOfAllEntries - amountOfNextEntries - 1);
        } else {
            amountOfNextEntries = data.getCount();
            getListView().setSelection(0);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, final MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.menu_extended, menu);

        GetCleanState getCleanState = new GetCleanState();
        getCleanState.setMenu(menu);
        getCleanState.execute(getCleanState.TYPE_MEETINGS);

        MenuItem showHistoryItem = menu.findItem(R.id.action_show_history);
        PorterDuffColorFilter pdColorFilter;
        if (!showHistory) {
            showHistoryItem.setChecked(false);
            pdColorFilter = new PorterDuffColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
        } else {
            showHistoryItem.setChecked(true);
            pdColorFilter = new PorterDuffColorFilter(Color.BLACK, PorterDuff.Mode.SRC_ATOP);
        }
        showHistoryItem.getIcon().setColorFilter(pdColorFilter);

        searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setOnQueryTextListener(this);
        if (!TextUtils.isEmpty(searchQuery) && !searchView.getQuery().equals(searchQuery)) {
            searchView.setQuery(searchQuery, true);
            searchView.setIconified(false);
        }

        searchView.setOnCloseListener(this);
    }

    @Override
    public boolean onClose() {
        getActivity().invalidateOptionsMenu();
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        searchView.clearFocus();
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        searchQuery = newText;
        getLoaderManager().restartLoader(0, new Bundle(), this);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_show_history) {
            PorterDuffColorFilter pdColorFilter;
            if (showHistory) {
                showHistory = false;
                item.setChecked(false);
                pdColorFilter = new PorterDuffColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
            } else {
                showHistory = true;
                item.setChecked(true);
                pdColorFilter = new PorterDuffColorFilter(Color.BLACK, PorterDuff.Mode.SRC_ATOP);
            }
            item.getIcon().setColorFilter(pdColorFilter);
            getLoaderManager().restartLoader(0, new Bundle(), this);
            return true;
        }
        return false;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
        Log.d("Click on", String.valueOf(id));
        if ((MeetingsCursorAdapter.EXPANDED.get(id) == null) || (!MeetingsCursorAdapter.EXPANDED.get(id))) {
            MeetingsCursorAdapter.EXPANDED.put(id, true);
            expand(v.findViewById(R.id.expandable));
            v.findViewById(R.id.expandable).animate().alpha(1).setDuration(300);
            v.findViewById(R.id.expand).animate().rotation(180.0f).setDuration(300);
        } else {
            MeetingsCursorAdapter.EXPANDED.remove(id);
            collapse(v.findViewById(R.id.expandable));
            v.findViewById(R.id.expandable).animate().alpha(0).setDuration(300);
            v.findViewById(R.id.expand).animate().rotation(0.0f).setDuration(300);
        }
    }
}
