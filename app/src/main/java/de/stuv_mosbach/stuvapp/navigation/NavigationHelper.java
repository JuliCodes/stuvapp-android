package de.stuv_mosbach.stuvapp.navigation;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;
import com.google.android.material.navigation.NavigationView;
import androidx.appcompat.app.ActionBar;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import de.stuv_mosbach.stuvapp.MainActivity;
import de.stuv_mosbach.stuvapp.R;
import de.stuv_mosbach.stuvapp.about.AboutActivity;
import de.stuv_mosbach.stuvapp.listFragments.feed.FeedListFragment;
import de.stuv_mosbach.stuvapp.listFragments.latest.LatestListFragment;
import de.stuv_mosbach.stuvapp.listFragments.lectures.LectureListFragment;
import de.stuv_mosbach.stuvapp.listFragments.meetings.MeetingsListFragment;
import de.stuv_mosbach.stuvapp.lunch.StudentFragment;
import de.stuv_mosbach.stuvapp.settings.PreferenceKeys;
import de.stuv_mosbach.stuvapp.settings.SettingsActivity;
import de.stuv_mosbach.stuvapp.tutorial.TutorialFragment;
import de.stuv_mosbach.stuvapp.util.ConfigGlobal;

/**
 * Diese Klasse lagert alle Funktionen aus der MainActivity aus, die sich in welcher Weise auch
 * immer mit dem Wechsel von Inhalten beschäftigen.
 * <p/>
 * Die Klasse enthält dabei drei "Mapper-Funktionen", welche auf Basis des Inputs eine spezialiserte
 * Funktion aufrufen, die dann die eigentliche Arbeit übernimmt.
 *
 * @author Peer Beckmann <peer@pbeckmann.de>
 */
public class NavigationHelper {

    private static final String LOG_NAME = NavigationHelper.class.getName();

    private static int lastOpenedFragment = -1;
    private final FragmentManager fragmentManager;
    private final SharedPreferences sharedPreferences;
    private final Context context;
    private final Activity activity;

    /**
     * Standard Konstruktor.
     *
     * @param fragmentManager Der Fragment Manager der Main Activity, der die Fragmente austauschen kann.
     * @param context         Der Kontext in dem die Main Activity läuft.
     * @param activity        Die eigentliche Activity.
     */
    public NavigationHelper(FragmentManager fragmentManager, Context context, Activity activity) {
        Log.i(LOG_NAME, "constructor");

        this.fragmentManager = fragmentManager;
        this.sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        this.context = context;
        this.activity = activity;
    }

    public void updateMenuEntries () {
        Log.i(LOG_NAME, "updateMenuEntries");

        NavigationView navigationView = (NavigationView) activity.findViewById(R.id.navigation_view);

        Boolean val;

        val = sharedPreferences.getBoolean(PreferenceKeys.SHOW_NOW, true);
        navigationView.getMenu().findItem(R.id.navigation_now).setVisible(val);

        val = sharedPreferences.getBoolean(PreferenceKeys.SHOW_LECTURES, true);
        navigationView.getMenu().findItem(R.id.navigation_lectures).setVisible(val);

        val = sharedPreferences.getBoolean(PreferenceKeys.SHOW_MEETINGS, true);
        navigationView.getMenu().findItem(R.id.navigation_students).setVisible(val);

        val = sharedPreferences.getBoolean(PreferenceKeys.SHOW_STUV, true);
        navigationView.getMenu().findItem(R.id.navigation_about_stuv).setVisible(val);

        val = sharedPreferences.getBoolean(PreferenceKeys.SHOW_TUTORIAL, true);
        navigationView.getMenu().findItem(R.id.navigation_tutorial).setVisible(val);

        val = sharedPreferences.getBoolean(PreferenceKeys.SHOW_SETTINGS, true);
        navigationView.getMenu().findItem(R.id.navigation_settings).setVisible(val);

        val = sharedPreferences.getBoolean(PreferenceKeys.SHOW_ISSUE, true);
        navigationView.getMenu().findItem(R.id.navigation_report_issue).setVisible(val);

        val = sharedPreferences.getBoolean(PreferenceKeys.SHOW_ABOUT, true);
        navigationView.getMenu().findItem(R.id.navigation_about).setVisible(val);

        val = sharedPreferences.getBoolean(PreferenceKeys.SHOW_NEWS, true);
        navigationView.getMenu().findItem(R.id.navigation_news).setVisible(val);

        val = sharedPreferences.getBoolean(PreferenceKeys.SHOW_LUNCH, true);
        navigationView.getMenu().findItem(R.id.navigation_lunch).setVisible(val);
    }

    /**
     * Wir setzen die Activity auf einen generischen Ausgangszustand zurück, damit keine fehlerhafte
     * Darstellung entstehen kann.
     */
    private void removeCustoms() {
        ActionBar actionBar;
        if ((actionBar = ((MainActivity) activity).getSupportActionBar()) != null)
            actionBar.setSubtitle("");

        MySwipeRefreshLayout swipeRefreshLayout = (MySwipeRefreshLayout) activity.findViewById(R.id.swiperefresh);
        swipeRefreshLayout.setEnabled(true);
    }

    /**
     * Kleiner Workaround, um die Zeile Code nicht immer schreiben zu müssen.
     *
     * @param id Die String id. Muss mindestens in der englischsprachingen strings.xml vorhanden sein.
     */
    private void setTitle(int id) {
        activity.setTitle(context.getString(id));
    }

    /**
     * Hier wird der aktive Eintrag im Navigation Drawer markiert. Wenn keiner explizit gemarkt werden muss,
     * werden alle Einträge durchgegangen um eine eventuelle Markierung zu entfernen.
     */
    private void markActiveEntry() {
        NavigationView navigationView = (NavigationView) activity.findViewById(R.id.navigation_view);

        if (lastOpenedFragment != -1) {
            navigationView.getMenu().findItem(lastOpenedFragment).setChecked(true);
        } else {
            int size = navigationView.getMenu().size();
            for (int n = 0; n < size; n++) {
                navigationView.getMenu().getItem(n).setChecked(false);
            }
        }
    }

    /**
     * Wenn der Nutzer beim letzten Schließen der App ein Fragment offen haben, öffnet diese Funktion
     * das entsprechende Fragment erneut.
     *
     * @return True, falls ein vorheriges Fragment existiert hat.
     */
    private boolean navigateToLast() {
        if (lastOpenedFragment != -1) {
            navigateById(lastOpenedFragment);
            markActiveEntry();
            return true;
        }
        return false;
    }

    /**
     * Diese Funktion öffnet basierend auf der Einstellung des Nutzer das Standard Fragment.
     */
    private void openDefaultFragment() {
        removeCustoms();

        if (navigateToLast())
            return;

        String defaultFragment = sharedPreferences.getString(PreferenceKeys.STANDARD_FRAGMENT, "LatestListFragment");
        switch (defaultFragment) {
            case "Lectures":
                showLecturesFragment();
                break;
            case "Meetings":
                showMeetingsFragment();
                break;
            default:
                showTodayFragment();
        }

        markActiveEntry();
    }

    /**
     * Wenn der App beim Öffnen ein Intent übergeben wird, so wird in dieser Funktion überprüft ob
     * dieser eine Aktion enthält, die zu einem veränderten Verhalten und somit zu einem anderem
     * Fragment führt. Wenn nicht wird {@link #openDefaultFragment()} ausgeführt.
     *
     * @param intent Der Intent, der beim Aufruf der App mit übergeben wird.
     */
    public void navigateByIntent(Intent intent) {
        removeCustoms();

        String action = intent.getAction();
        if (action == null || action.equals("") || action.equals(Intent.ACTION_MAIN)) {
            openDefaultFragment();
            return;
        }

        switch (intent.getAction()) {
            case ConfigGlobal.AUTHORITY + ".SHOW_DINNER":
                showStudentServiceFragment();
                break;
            case ConfigGlobal.AUTHORITY + ".OPEN_LECTURES":
                showLecturesFragment();
                break;
            case ConfigGlobal.AUTHORITY + ".OPEN_MEETINGS":
                showMeetingsFragment();
                break;
            case ConfigGlobal.AUTHORITY + ".SHOW_LUNCHPLAN":
                showStudentServiceFragment();
                StudentFragment.openLunchplan(context);
                break;
            default:
                openDefaultFragment();
        }

        markActiveEntry();
    }

    /**
     * Wenn der Nutzer im Navigation Drawer einen Eintrag antippt, so erhalten wir von diesem nur eine
     * ID. Diese wird in diesem Switch auf die entsprechende Funktion gemappt.
     *
     * @param id Die ID.
     */
    public void navigateById(int id) {
        Log.i(LOG_NAME, "navigateById");

        removeCustoms();

        switch (id) {
            case R.id.navigation_lunch:
                showStudentServiceFragment();
                break;
            case R.id.navigation_lectures:
                showLecturesFragment();
                break;
            case R.id.navigation_students:
                showMeetingsFragment();
                break;
            case R.id.navigation_now:
                showTodayFragment();
                break;
            case R.id.navigation_news:
                showFeedFragment();
                break;
            case R.id.navigation_settings:
                showSettingsActivity();
                break;
            case R.id.navigation_report_issue:
                sendEmailBugReport();
                break;
            case R.id.navigation_about:
                showAboutActivity();
                break;
            case R.id.navigation_tutorial:
                showTutorialFragment();
                break;
            default:
                openDefaultFragment();
        }

        markActiveEntry();
    }

    /**
     * Diese Funktion zeigt das Fragment mit den Vorlesungen des Nutzers an. Als Subtitel wird in der
     * Action Bar der ausgewählte Kurs angezeigt.
     */
    private void showLecturesFragment() {
        Log.i(LOG_NAME, "showLecturesFragment");
        lastOpenedFragment = R.id.navigation_lectures;
        LectureListFragment lectureListFragment = new LectureListFragment();
        lectureListFragment.setRetainInstance(false);
        fragmentManager.beginTransaction()
                .replace(R.id.contentPane, lectureListFragment, "lectures")
                .commit();
        setTitle(R.string.lectures_title);

        ActionBar actionBar;
        if ((actionBar = ((MainActivity) activity).getSupportActionBar()) != null)
            actionBar.setSubtitle(sharedPreferences.getString("pref_class_value", ""));
    }

    /**
     * Diese Funktion zeigt das Fragment mit den Terminen der Studierendenvertretung an.
     */
    private void showMeetingsFragment() {
        Log.i(LOG_NAME, "showMeetingsFragment");
        lastOpenedFragment = R.id.navigation_students;
        MeetingsListFragment meetingsListFragment = new MeetingsListFragment();
        meetingsListFragment.setRetainInstance(false);
        fragmentManager.beginTransaction()
                .replace(R.id.contentPane, meetingsListFragment, "meetings")
                .commit();
        setTitle(R.string.meetings_title);
    }

    /**
     * Diese Funktion zeit das Fragment mit mit den kombinierten Terminen der nächsten beiden Tagen an.
     */
    private void showTodayFragment() {
        Log.i(LOG_NAME, "showTodayFragment");
        lastOpenedFragment = R.id.navigation_now;
        LatestListFragment latestListFragment = new LatestListFragment();
        latestListFragment.setCallback((MainActivity) activity);
        fragmentManager.beginTransaction()
                .replace(R.id.contentPane, latestListFragment, "home")
                .commit();
        setTitle(R.string.latest_title);
    }

    private void showFeedFragment() {
        Log.i(LOG_NAME, "showFeedFragment");
        lastOpenedFragment = R.id.navigation_news;
        fragmentManager.beginTransaction()
                .replace(R.id.contentPane, new FeedListFragment(), "feed")
                .commit();
        setTitle(R.string.news_title);
    }

    /**
     * Diese Funktion zeigt das Fragment mit den Links zum Studierendenwerk an. Besonders dabei ist,
     * dass wir an dieser Stelle das SwipeToRefresh Layout deaktivieren.
     */
    private void showStudentServiceFragment() {
        Log.i(LOG_NAME, "showStudentServiceFragment");
        lastOpenedFragment = R.id.navigation_lunch;
        fragmentManager.beginTransaction()
                .replace(R.id.contentPane, new StudentFragment(), "lunch")
                .commit();
        setTitle(R.string.lunch_title);
    }

    /**
     * Diese Funktion zeigt das App Tutorial an.
     */
    private void showTutorialFragment() {
        Log.i(LOG_NAME, "showTutorialFragment");
        lastOpenedFragment = -1;
        fragmentManager.beginTransaction()
                .replace(R.id.contentPane, new TutorialFragment(), "tutorial")
                .commit();
        setTitle(R.string.tutorial_title);
    }

    /**
     * Öffne die Activity um Einstellungen innerhalb der App zu verändern.
     */
    private void showSettingsActivity() {
        Log.i(LOG_NAME, "showSettingsActivity");
        Intent settings = new Intent(context, SettingsActivity.class);
        context.startActivity(settings);
    }

    /**
     * Get the logcat from the app. And send it to an email application.
     */
    private void sendEmailBugReport() {
        final String EMAIL = "it@stuv-mosbach.de";
        final String SUBJECT = "Fehler: StuVApp-Android";
        final String LOG_FILE_NAME = File.separator + "log.txt";

        File outputFile = new File(context.getCacheDir() + LOG_FILE_NAME);
        try {
            Runtime.getRuntime().exec("logcat -f "+outputFile.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Log.i("SharedPreferences", sharedPreferences.getAll().toString());

        String release = Build.VERSION.RELEASE;
        int sdkVersion = Build.VERSION.SDK_INT;
        Log.i("System", "Release:" + release + " SDK: " + sdkVersion);

        Intent emailIntent = new Intent(Intent.ACTION_SEND_MULTIPLE);
        emailIntent.setData(Uri.parse("mailto" + EMAIL));
        emailIntent.setType("message/rfc822");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{EMAIL});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, SUBJECT);

        ArrayList<Uri> uris = new ArrayList<>();
        uris.add(Uri.withAppendedPath(Uri.parse("content://" + ConfigGlobal.AUTHORITY), "file" + LOG_FILE_NAME));

        emailIntent.putExtra(Intent.EXTRA_STREAM, uris);
        context.startActivity(Intent.createChooser(emailIntent, "Send email..."));
    }

    /**
     * Öffne die Activity mit den Informationen über die Entwickler und die App.
     */
    private void showAboutActivity() {
        Log.i(LOG_NAME, "showAboutActivity");
        Intent about = new Intent(context, AboutActivity.class);
        context.startActivity(about);
    }
}
