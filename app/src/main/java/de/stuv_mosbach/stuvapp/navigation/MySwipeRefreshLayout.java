package de.stuv_mosbach.stuvapp.navigation;

import android.content.Context;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ListView;

/**
 * Diese Datei stellt eine eigene Implementierung für die {@link #canChildScrollUp()} Methode zur
 * Verfügung. Dies ist notwending, weil das RefreshLayout nicht direkt die Liste enthält. Hierzu
 * erben wir das Standard {@link SwipeRefreshLayout} und überschreiben dadrin die Methode.
 *
 * @author Peer Beckmann <peer@pbeckmann.de>
 */
public class MySwipeRefreshLayout extends SwipeRefreshLayout {

    public MySwipeRefreshLayout(Context context) {
        super(context);
    }

    public MySwipeRefreshLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Die Funktion sucht die ListView innerhalb des {@link SwipeRefreshLayout} und schaut ob das
     * oberste Element der List ganz oben in der Ansicht ist.
     *
     * @return true, wenn die List noch scrollbar ist.
     */
    @Override
    public boolean canChildScrollUp() {
        ListView listView = (ListView) this.findViewById(android.R.id.list);
        if (listView != null) {
            View child0 = listView.getChildAt(0);
            if (child0 != null)
                return !(child0.getTop() >= -1 && listView.getFirstVisiblePosition() == 0);
        }

        return false;
    }
}
