package de.stuv_mosbach.stuvapp.about;

import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import de.stuv_mosbach.stuvapp.BuildConfig;
import de.stuv_mosbach.stuvapp.R;
import de.stuv_mosbach.stuvapp.util.ThemeFunctions;

/**
 * Diese Activity zeigt dem Nutzer den Inhalt "Über die App" an. Da die eigentliche Logik in dem
 * Fragment steckt, hat diese Activity keine besondere Logik.
 *
 * @author Peer Beckmann
 */
public class AboutActivity extends AppCompatActivity {

    /**
     * Wir initialisieren das Standard Layout, dann binden wir das Fragment in das Layout ein und
     * zuletzt erstellen wir die ActionBar.
     *
     * @param savedInstanceState Nachzulesen bei
     *                           {@link de.stuv_mosbach.stuvapp.MainActivity#onCreate(Bundle)}
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ThemeFunctions.changeTheme(this, PreferenceManager.getDefaultSharedPreferences(this));

        setContentView(R.layout.fragment_recycler_view_container);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setDisplayHomeAsUpEnabled(true);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);

        //Add entries
        final String[] titles = getResources().getStringArray(R.array.about_content_titles);
        final String[] contents_old = getResources().getStringArray(R.array.about_content_contents);

        final String[] contents = new String[contents_old.length + 1];

        System.arraycopy(contents_old, 0, contents, 0, contents_old.length);

        contents[contents.length - 1] = "Build-Nummer: " + BuildConfig.VERSION_CODE + "\nVersionsname: " + BuildConfig.VERSION_NAME;

        recyclerView.setAdapter(new AboutAdapter(titles, contents));
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

    }
}
