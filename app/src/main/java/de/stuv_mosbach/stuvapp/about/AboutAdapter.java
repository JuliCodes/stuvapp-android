package de.stuv_mosbach.stuvapp.about;

import android.content.Context;
import android.preference.PreferenceManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import de.stuv_mosbach.stuvapp.R;
import de.stuv_mosbach.stuvapp.settings.PreferenceKeys;

/**
 * Nimm die übergebenen Titel und Einträge und bau aus diesen die einzelnen Ansichten.
 */
class AboutAdapter extends RecyclerView.Adapter<AboutAdapter.LViewHolder> {

    private final String[] titles;
    private final String[] contents;

    /**
     * Standard Konstruktor, der die Werte entgegennimmt und in den Klassenvariablen abspeichert.
     *
     * @param titles   Die Titel für die einzelnen Einträge.
     * @param contents Die Inhalte der einzelnen Einträge.
     */
    AboutAdapter(final String[] titles, final String[] contents) {
        this.titles = titles;
        this.contents = contents;
    }

    /**
     * Hier führen wir die Operationen aus, die benötigt werden, um den ViewHolder zu erstellen.
     * Wir benötigen hier keine komplexen Operationen, deswegen extrahieren wir hier nur die Ansicht
     * aus den XML und übergeben das an die Constructor der ViewHolder Klasse und geben das Resultat
     * zurück.
     *
     * @param parent   Die Eltern-Ansicht, in das wir das Element einbauen wollen.
     * @param viewType Der ViewType. Können wir hier ignorieren, da wir nur eine Ansicht haben.
     * @return Der fertige ViewHolder.
     */
    @Override
    public AboutAdapter.LViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new LViewHolder(((LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.item_abouts, parent, false));
    }

    /**
     * Hier fügen wir die Inhalte in die einzelnen Einträge ein.
     *
     * @param lViewHolder Der ViewHolder, der exemplarisch für die Ansicht steht.
     * @param position    Die Position der Ansicht.
     */
    @Override
    public void onBindViewHolder(LViewHolder lViewHolder, int position) {
        lViewHolder.body_text.setText(contents[position]);
        lViewHolder.header_title.setText(titles[position]);
    }

    /**
     * Wieviele Ansichten benötigen wir.
     *
     * @return Die Anzahl der Titel.
     */
    @Override
    public int getItemCount() {
        return titles.length;
    }

    /**
     * Der ViewHolder. Dient dazu, nicht bei jeder Ansicht lauter Basis-Funktionen ausführen zu
     * müssen.
     */
    static class LViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public final TextView header_title;
        public final TextView body_text;
        private int amount = 0;

        /**
         * Hier erstellen wir den ViewHolder. Genauer, wir führen alle Operationen aus, um später
         * einfach die Ansichten mit Inhalten füllen zu können.
         *
         * @param view Die Ansicht, von der wir den ViewHolder benötigen.
         */
        LViewHolder(View view) {
            super(view);

            header_title = (TextView) view.findViewById(R.id.header_title);
            body_text = (TextView) view.findViewById(R.id.body_text);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (getLayoutPosition() == 10) {
                amount ++;
            }
            if (amount == 6) {
                PreferenceManager.getDefaultSharedPreferences(view.getContext()).edit().putBoolean(PreferenceKeys.SHOW_INTERNALS, true).apply();
                Toast.makeText(view.getContext(), "With great power comes great responsibility, you Geek!", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
