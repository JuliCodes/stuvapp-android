package de.stuv_mosbach.stuvapp.widget;

import android.content.Intent;
import android.widget.RemoteViewsService;

/**
 * @author Peer Beckmann <peer@pbeckmann.de>
 *         <p/>
 *         Declare the service for the widget.
 */
public class WidgetService extends RemoteViewsService {

    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        return new ListProvider(this.getApplicationContext());
    }
}
