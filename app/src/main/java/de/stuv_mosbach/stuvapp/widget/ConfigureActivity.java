package de.stuv_mosbach.stuvapp.widget;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.WallpaperManager;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import de.stuv_mosbach.stuvapp.R;
import de.stuv_mosbach.stuvapp.settings.PreferenceKeys;
import de.stuv_mosbach.stuvapp.util.ThemeFunctions;

/**
 * Created by Kevin Lackmann on 25.09.2015.
 */
public class ConfigureActivity extends Activity {
    private final WidgetSettingsFragment fragment = new WidgetSettingsFragment();
    private View listItem1;
    private View listItem2;
    private View listItem3;
    private int mAppWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID;
    private ViewGroup widget;
    private final SharedPreferences.OnSharedPreferenceChangeListener listener =
            (sharedPreferences, key) -> {
                switch (key) {
                    case PreferenceKeys.WIDGET_THEME:
                    case PreferenceKeys.WIDGET_HIDE_HEADER:
                        updateWidgetPreview();
                        break;
                    default:
                        break;
                }
            };

    public ConfigureActivity() {
        super();
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        //Wenn nicht auf den Fertig-Button geklickt wird(zB Abbrechen Button oder Zurück Taste), wird auch kein Widget erstellt
        setResult(RESULT_CANCELED);

        ThemeFunctions.changeTheme(this, sharedPreferences);
        setContentView(R.layout.widget_configuration);

        getFragmentManager().beginTransaction()
                .replace(R.id.contentPane, fragment, "widget_settings")
                .commit();


        //Den Hintergrund des Nutzers als Vorschau-Hintergrund benutzen. (Als Fallback ist die food.jpg im XML definiert)
        final WallpaperManager wallpaperManager = WallpaperManager.getInstance(this);
        final Drawable wallpaperDrawable = wallpaperManager.getDrawable();
        ((ImageView) findViewById(R.id.widget_preview_container_background)).setImageDrawable(wallpaperDrawable);

        sharedPreferences.registerOnSharedPreferenceChangeListener(listener);

        //WidgetPreview initalisieren und Beispiel Vorlesungen erstellen
        widget = (ViewGroup) findViewById(R.id.widget);

        widget.findViewById(R.id.list_view).setVisibility(View.GONE);
        widget.findViewById(R.id.empty_list).setVisibility(View.GONE);
        widget.findViewById(R.id.emptyview_image).setVisibility(View.GONE);

        listItem1 = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.widget_lecture_item, widget, false);
        ((TextView) listItem1.findViewById(R.id.date)).setText("16");
        listItem1.findViewById(R.id.date).setVisibility(View.VISIBLE);
        ((TextView) listItem1.findViewById(R.id.day)).setText("Fr.");
        listItem1.findViewById(R.id.day).setVisibility(View.VISIBLE);
        ((TextView) listItem1.findViewById(R.id.event_header)).setText("Mathematik Vorlesung");
        ((TextView) listItem1.findViewById(R.id.event_location)).setText("E-0.180");
        ((TextView) listItem1.findViewById(R.id.event_start)).setText("8:45");
        ((TextView) listItem1.findViewById(R.id.event_end)).setText("12:00");

        listItem2 = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.widget_lecture_item, widget, false);
        ((TextView) listItem2.findViewById(R.id.date)).setText("17");
        listItem2.findViewById(R.id.date).setVisibility(View.VISIBLE);
        ((TextView) listItem2.findViewById(R.id.day)).setText("Sa.");
        listItem2.findViewById(R.id.day).setVisibility(View.VISIBLE);
        ((TextView) listItem2.findViewById(R.id.event_header)).setText("Sommerfest");
        ((TextView) listItem2.findViewById(R.id.event_location)).setText("Großer Elzpark");
        ((TextView) listItem2.findViewById(R.id.event_start)).setText("15:00");
        ((TextView) listItem2.findViewById(R.id.event_end)).setText("22:00");

        listItem3 = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.widget_lecture_item, widget, false);
        ((TextView) listItem3.findViewById(R.id.date)).setText("19");
        listItem3.findViewById(R.id.date).setVisibility(View.VISIBLE);
        ((TextView) listItem3.findViewById(R.id.day)).setText("Mo.");
        listItem3.findViewById(R.id.day).setVisibility(View.VISIBLE);
        ((TextView) listItem3.findViewById(R.id.event_header)).setText("BWL Vorlesung");
        ((TextView) listItem3.findViewById(R.id.event_location)).setText("B-1.230");
        ((TextView) listItem3.findViewById(R.id.event_start)).setText("13:15");
        ((TextView) listItem3.findViewById(R.id.event_end)).setText("16:15");

        widget.addView(listItem1);
        widget.addView(listItem2);
        widget.addView(listItem3);

        updateWidgetPreview();


        // Widget aus dem Intend finden
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            mAppWidgetId = extras.getInt(
                    AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
        }

        // Wenn im Intend eine falsche Widget ID steht wird abgebrochen
        //
        //Muss auskommentiert werden wenn diese Activity auch aus den Einstellungen heraus funktionieren soll.
        //Widget Erstellung funktioniert auch ohne diese Funktion erwartungsgemäß...
        //
        if (mAppWidgetId == AppWidgetManager.INVALID_APPWIDGET_ID) {
            finish();
        }

        Log.i("AppWidgetId", String.valueOf(mAppWidgetId));
        Log.i("Invalid", String.valueOf(AppWidgetManager.INVALID_APPWIDGET_ID));

        Button cancel = (Button) findViewById(R.id.button_cancel);
        cancel.setOnClickListener(v -> finish());

        Button doneButton = (Button) findViewById(R.id.button_ready);
        doneButton.setOnClickListener(v -> {
            // RESULT_OK damit das Widget als korrekt erkannt und auch tatsächlich erstellt wird
            Intent resultValue = new Intent();
            resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, mAppWidgetId);
            setResult(RESULT_OK, resultValue);
            finish();

            //Widget updaten damit die gewählten Einstellungen sofort in Kraft treten
            new WidgetProvider()
                    .onUpdate(getApplicationContext(),
                            AppWidgetManager.getInstance(getBaseContext()),
                            new int[]{R.layout.widget_list}
                    );
        });
    }

    /**
     * Bei Änderung einer Einstellung wird die Vorschau des Widgets(und alle darin enthaltenden TextViews) live angepasst.
     */
    private void updateWidgetPreview() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String widgetTheme = sharedPreferences.getString(PreferenceKeys.WIDGET_THEME, "Light");
        switch (widgetTheme) {
            case "Light":
                widget.setBackgroundColor(Color.parseColor("#ffffff"));
                widget.findViewById(R.id.header).setBackgroundColor(Color.parseColor("#e82426"));
                ((TextView) listItem1.findViewById(R.id.date)).setTextColor(Color.BLACK);
                ((TextView) listItem1.findViewById(R.id.day)).setTextColor(Color.BLACK);
                ((TextView) listItem1.findViewById(R.id.event_header)).setTextColor(Color.BLACK);
                ((TextView) listItem1.findViewById(R.id.event_location)).setTextColor(Color.BLACK);
                ((TextView) listItem1.findViewById(R.id.event_start)).setTextColor(Color.BLACK);
                ((TextView) listItem1.findViewById(R.id.event_end)).setTextColor(Color.BLACK);
                ((TextView) listItem1.findViewById(R.id.time_delimiter)).setTextColor(Color.BLACK);

                ((TextView) listItem2.findViewById(R.id.date)).setTextColor(Color.BLACK);
                ((TextView) listItem2.findViewById(R.id.day)).setTextColor(Color.BLACK);
                ((TextView) listItem2.findViewById(R.id.event_header)).setTextColor(Color.BLACK);
                ((TextView) listItem2.findViewById(R.id.event_location)).setTextColor(Color.BLACK);
                ((TextView) listItem2.findViewById(R.id.event_start)).setTextColor(Color.BLACK);
                ((TextView) listItem2.findViewById(R.id.event_end)).setTextColor(Color.BLACK);
                ((TextView) listItem2.findViewById(R.id.time_delimiter)).setTextColor(Color.BLACK);

                ((TextView) listItem3.findViewById(R.id.date)).setTextColor(Color.BLACK);
                ((TextView) listItem3.findViewById(R.id.day)).setTextColor(Color.BLACK);
                ((TextView) listItem3.findViewById(R.id.event_header)).setTextColor(Color.BLACK);
                ((TextView) listItem3.findViewById(R.id.event_location)).setTextColor(Color.BLACK);
                ((TextView) listItem3.findViewById(R.id.event_start)).setTextColor(Color.BLACK);
                ((TextView) listItem3.findViewById(R.id.event_end)).setTextColor(Color.BLACK);
                ((TextView) listItem3.findViewById(R.id.time_delimiter)).setTextColor(Color.BLACK);
                break;
            case "Dark":
                widget.setBackgroundColor(Color.parseColor("#424242"));
                widget.findViewById(R.id.header).setBackgroundColor(Color.parseColor("#881619"));
                ((TextView) listItem1.findViewById(R.id.date)).setTextColor(Color.WHITE);
                ((TextView) listItem1.findViewById(R.id.day)).setTextColor(Color.WHITE);
                ((TextView) listItem1.findViewById(R.id.event_header)).setTextColor(Color.WHITE);
                ((TextView) listItem1.findViewById(R.id.event_location)).setTextColor(Color.WHITE);
                ((TextView) listItem1.findViewById(R.id.event_start)).setTextColor(Color.WHITE);
                ((TextView) listItem1.findViewById(R.id.event_end)).setTextColor(Color.WHITE);
                ((TextView) listItem1.findViewById(R.id.time_delimiter)).setTextColor(Color.WHITE);

                ((TextView) listItem2.findViewById(R.id.date)).setTextColor(Color.WHITE);
                ((TextView) listItem2.findViewById(R.id.day)).setTextColor(Color.WHITE);
                ((TextView) listItem2.findViewById(R.id.event_header)).setTextColor(Color.WHITE);
                ((TextView) listItem2.findViewById(R.id.event_location)).setTextColor(Color.WHITE);
                ((TextView) listItem2.findViewById(R.id.event_start)).setTextColor(Color.WHITE);
                ((TextView) listItem2.findViewById(R.id.event_end)).setTextColor(Color.WHITE);
                ((TextView) listItem2.findViewById(R.id.time_delimiter)).setTextColor(Color.WHITE);

                ((TextView) listItem3.findViewById(R.id.date)).setTextColor(Color.WHITE);
                ((TextView) listItem3.findViewById(R.id.day)).setTextColor(Color.WHITE);
                ((TextView) listItem3.findViewById(R.id.event_header)).setTextColor(Color.WHITE);
                ((TextView) listItem3.findViewById(R.id.event_location)).setTextColor(Color.WHITE);
                ((TextView) listItem3.findViewById(R.id.event_start)).setTextColor(Color.WHITE);
                ((TextView) listItem3.findViewById(R.id.event_end)).setTextColor(Color.WHITE);
                ((TextView) listItem3.findViewById(R.id.time_delimiter)).setTextColor(Color.WHITE);
                break;
            case "Transparent":
                widget.setBackgroundColor(Color.TRANSPARENT);
                widget.findViewById(R.id.header).setBackgroundColor(Color.parseColor("#55ffffff"));
                ((TextView) listItem1.findViewById(R.id.date)).setTextColor(Color.WHITE);
                ((TextView) listItem1.findViewById(R.id.day)).setTextColor(Color.WHITE);
                ((TextView) listItem1.findViewById(R.id.event_header)).setTextColor(Color.WHITE);
                ((TextView) listItem1.findViewById(R.id.event_location)).setTextColor(Color.WHITE);
                ((TextView) listItem1.findViewById(R.id.event_start)).setTextColor(Color.WHITE);
                ((TextView) listItem1.findViewById(R.id.event_end)).setTextColor(Color.WHITE);
                ((TextView) listItem1.findViewById(R.id.time_delimiter)).setTextColor(Color.WHITE);

                ((TextView) listItem2.findViewById(R.id.date)).setTextColor(Color.WHITE);
                ((TextView) listItem2.findViewById(R.id.day)).setTextColor(Color.WHITE);
                ((TextView) listItem2.findViewById(R.id.event_header)).setTextColor(Color.WHITE);
                ((TextView) listItem2.findViewById(R.id.event_location)).setTextColor(Color.WHITE);
                ((TextView) listItem2.findViewById(R.id.event_start)).setTextColor(Color.WHITE);
                ((TextView) listItem2.findViewById(R.id.event_end)).setTextColor(Color.WHITE);
                ((TextView) listItem2.findViewById(R.id.time_delimiter)).setTextColor(Color.WHITE);

                ((TextView) listItem3.findViewById(R.id.date)).setTextColor(Color.WHITE);
                ((TextView) listItem3.findViewById(R.id.day)).setTextColor(Color.WHITE);
                ((TextView) listItem3.findViewById(R.id.event_header)).setTextColor(Color.WHITE);
                ((TextView) listItem3.findViewById(R.id.event_location)).setTextColor(Color.WHITE);
                ((TextView) listItem3.findViewById(R.id.event_start)).setTextColor(Color.WHITE);
                ((TextView) listItem3.findViewById(R.id.event_end)).setTextColor(Color.WHITE);
                ((TextView) listItem3.findViewById(R.id.time_delimiter)).setTextColor(Color.WHITE);
                break;
            default:
                break;
        }
        if (sharedPreferences.getBoolean(PreferenceKeys.WIDGET_HIDE_HEADER, false))
            widget.findViewById(R.id.header).setVisibility(View.GONE);
        else
            widget.findViewById(R.id.header).setVisibility(View.VISIBLE);
    }
}
