package de.stuv_mosbach.stuvapp.widget;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import org.chaosdaten.commons.DateHelper;

import java.util.Date;
import java.util.Locale;

import de.stuv_mosbach.stuvapp.MainActivity;
import de.stuv_mosbach.stuvapp.R;
import de.stuv_mosbach.stuvapp.contentProvider.HomeContract;
import de.stuv_mosbach.stuvapp.contentProvider.LectureContract;
import de.stuv_mosbach.stuvapp.listFragments.ParentListFragment;
import de.stuv_mosbach.stuvapp.settings.PreferenceKeys;
import de.stuv_mosbach.stuvapp.sync.SyncAdapter;
import de.stuv_mosbach.stuvapp.util.ConfigGlobal;

import static de.stuv_mosbach.stuvapp.contentProvider.HomeContract.KEY_LECTURES;
import static de.stuv_mosbach.stuvapp.contentProvider.HomeContract.KEY_MEETINGS;

/**
 * @author Peer Beckmann <peer@pbeckmann.de>
 *         <p/>
 *         Bind the widget content to the widget.
 */
class ListProvider implements RemoteViewsService.RemoteViewsFactory {


    private static final int COLUMN_TITLE = 1;
    private static final int COLUMN_LOCATION = 2;
    private static final int COLUMN_BEGIN = 3;
    private static final int COLUMN_END = 4;
    private static final int COLUMN_FULL_DAY = 6;
    private static final int COLUMN_DESCRIPTION = 5;
    private static final int COLUMN_MULTI_DAY = 7;
    private static final int COLUMN_ORIGIN = 8;
    private static Cursor cursor;
    private final String[] PROJECTION = new String[]{
            LectureContract.LectureEntry._ID,
            LectureContract.LectureEntry.COLUMN_TITLE,
            LectureContract.LectureEntry.COLUMN_LOCATION,
            LectureContract.LectureEntry.COLUMN_BEGIN,
            LectureContract.LectureEntry.COLUMN_END,
            LectureContract.LectureEntry.COLUMN_DESCRIPTION,
            LectureContract.LectureEntry.COLUMN_FULL_DAY,
            LectureContract.LectureEntry.COLUMN_MORE_DAYS,
            "origin"
    };
    private Context context = null;

    public ListProvider(Context context) {
        this.context = context;
    }

    /**
     * How many rows did we get.
     *
     * @return An integer value, how many rows did we get.
     */
    @Override
    public int getCount() {
        return cursor.getCount();
    }

    /**
     * Here we build the connection to the database.
     */
    @Override
    public void onCreate() {
        String query;
        String[] queryArgs;

        String eventsShouldBeVisibleAfter = ParentListFragment.getEventStartTime();

        if (PreferenceManager.getDefaultSharedPreferences(context).getBoolean(PreferenceKeys.HIDE_STUDY_DAYS, false)) {
            query = LectureContract.LectureEntry.COLUMN_END + " BETWEEN ? AND ? AND " + LectureContract.LectureEntry.COLUMN_TITLE + " != ?";
            queryArgs = new String[]{
                    String.valueOf(eventsShouldBeVisibleAfter),
                    String.valueOf(DateHelper.getTimestampZeroClock(new Date(), Locale.getDefault()) + 2 * DateHelper.DAY_IN_MILLI),
                    "Studientag"};
        } else {
            query = LectureContract.LectureEntry.COLUMN_END + " BETWEEN ? AND ?";
            queryArgs = new String[]{
                    String.valueOf(eventsShouldBeVisibleAfter),
                    String.valueOf(DateHelper.getTimestampZeroClock(new Date(), Locale.getDefault()) + 2 * DateHelper.DAY_IN_MILLI)};
        }

        cursor = context.getContentResolver().query(
                HomeContract.CONTENT_URI,
                PROJECTION,
                query,
                queryArgs,
                null);
    }

    /**
     * What should happen, if the data in the database are changed. Here we just recreate the
     * database connection.
     */
    @Override
    public void onDataSetChanged() {
        String query;
        String[] queryArgs;
        String eventsShouldBeVisibleAfter = ParentListFragment.getEventStartTime();

        if (PreferenceManager.getDefaultSharedPreferences(context).getBoolean(PreferenceKeys.HIDE_STUDY_DAYS, false)) {
            query = LectureContract.LectureEntry.COLUMN_END + " BETWEEN ? AND ? AND " + LectureContract.LectureEntry.COLUMN_TITLE + " != ?";
            queryArgs = new String[]{
                    String.valueOf(eventsShouldBeVisibleAfter),
                    String.valueOf(DateHelper.getTimestampZeroClock(new Date(), Locale.getDefault()) + 14 * DateHelper.DAY_IN_MILLI),
                    "Studientag"};
        } else {
            query = LectureContract.LectureEntry.COLUMN_END + " BETWEEN ? AND ?";
            queryArgs = new String[]{
                    String.valueOf(eventsShouldBeVisibleAfter),
                    String.valueOf(DateHelper.getTimestampZeroClock(new Date(), Locale.getDefault()) + 14 * DateHelper.DAY_IN_MILLI)};
        }

        cursor = context.getContentResolver().query(
                HomeContract.CONTENT_URI,
                PROJECTION,
                query,
                queryArgs,
                null);
    }

    /**
     * This function will be called, if the Provider ist destroyed.
     */
    @Override
    public void onDestroy() {
        cursor.close();
    }

    /**
     * Fill the view at the position with values.
     *
     * @param position THe position of the view.
     * @return The with data filled view.
     */
    @Override
    public RemoteViews getViewAt(int position) {
        cursor.moveToPosition(position);

        Boolean FLAG_FULL_DAY = (cursor.getInt(COLUMN_FULL_DAY) == 1);
        Boolean FLAG_MULTI_DAY = (cursor.getInt(COLUMN_MULTI_DAY) == 1);
        String title = cursor.getString(COLUMN_TITLE);

        if (FLAG_FULL_DAY) {
            if (cursor.getLong(COLUMN_BEGIN) - cursor.getLong(COLUMN_END) == DateHelper.DAY_IN_MILLI)
                FLAG_MULTI_DAY = false;
        }

        RemoteViews remoteView;

        String professor = cursor.getString(COLUMN_DESCRIPTION);

        if (professor == null) {
            remoteView = new RemoteViews(
                    context.getPackageName(), R.layout.widget_stuv_item);
        } else {
            remoteView = new RemoteViews(
                    context.getPackageName(), R.layout.widget_lecture_item);
        }

        remoteView.setTextViewText(R.id.event_location, cursor.getString(COLUMN_LOCATION));

        remoteView.setViewVisibility(R.id.date, View.INVISIBLE);
        remoteView.setViewVisibility(R.id.day, View.INVISIBLE);

        if (position == 0 || isFirstAtDay(cursor, position)) {
            remoteView.setTextViewText(R.id.date, ConfigGlobal.date.format(cursor.getLong(COLUMN_BEGIN)));
            remoteView.setTextViewText(R.id.day, ConfigGlobal.day.format(cursor.getLong(COLUMN_BEGIN)));

            remoteView.setViewVisibility(R.id.date, View.VISIBLE);
            remoteView.setViewVisibility(R.id.day, View.VISIBLE);
        }

        if (FLAG_FULL_DAY && FLAG_MULTI_DAY) {
            Date date = new Date();
            date.setTime(cursor.getLong(COLUMN_END));
            remoteView.setTextViewText(R.id.event_start, context.getString(R.string.widget_message_end) + ConfigGlobal.dayMonth.format(date));
            remoteView.setViewVisibility(R.id.event_end, View.GONE);
            remoteView.setViewVisibility(R.id.time_delimiter, View.GONE);
        } else if (FLAG_MULTI_DAY) {
            Date start = new Date();
            start.setTime(cursor.getLong(COLUMN_BEGIN));
            Date end = new Date();
            end.setTime(cursor.getLong(COLUMN_END));
            remoteView.setTextViewText(R.id.event_start, ConfigGlobal.hourMinute.format(start));
            remoteView.setTextViewText(R.id.event_end, ConfigGlobal.dayMonthHourMinute.format(end));
        } else if (FLAG_FULL_DAY) {
            remoteView.setTextViewText(R.id.event_start, context.getString(R.string.widget_message_fullday));
            remoteView.setViewVisibility(R.id.event_end, View.GONE);
            remoteView.setViewVisibility(R.id.time_delimiter, View.GONE);
        } else {
            Date start = new Date();
            start.setTime(cursor.getLong(COLUMN_BEGIN));
            Date end = new Date();
            end.setTime(cursor.getLong(COLUMN_END));
            remoteView.setTextViewText(R.id.event_start, ConfigGlobal.hourMinute.format(start));
            remoteView.setTextViewText(R.id.event_end, ConfigGlobal.hourMinute.format(end));
        }

        if (PreferenceManager.getDefaultSharedPreferences(context).getBoolean(PreferenceKeys.SHOW_LECTURE_IN_TITLE, false)) {
            title = title.replace(" Vorlesung", "");
        }

        remoteView.setTextViewText(R.id.event_header, title);

        //Theme Coloring
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String widgetTheme = sharedPreferences.getString(PreferenceKeys.WIDGET_THEME, "Light");
        switch (widgetTheme) {
            case "Dark":
            case "Transparent":
                remoteView.setTextColor(R.id.day, Color.WHITE);
                remoteView.setTextColor(R.id.date, Color.WHITE);
                remoteView.setTextColor(R.id.event_header, Color.WHITE);
                remoteView.setTextColor(R.id.event_location, Color.WHITE);
                remoteView.setTextColor(R.id.event_start, Color.WHITE);
                remoteView.setTextColor(R.id.event_end, Color.WHITE);
                remoteView.setTextColor(R.id.time_delimiter, Color.WHITE);
                break;
            default:
                break;
        }

        Intent startActivity = new Intent(context, MainActivity.class);
        if (KEY_LECTURES.equals(cursor.getString(COLUMN_ORIGIN))) {
            startActivity = new Intent(SyncAdapter.OPEN_LECTURES);
        } else if (KEY_MEETINGS.equals(cursor.getString(COLUMN_ORIGIN))) {
            startActivity = new Intent(SyncAdapter.OPEN_MEETINGS);
        }
        remoteView.setOnClickFillInIntent(R.id.body, startActivity);

        return remoteView;
    }

    /**
     * If we want to give a custom loading view, we should do it here.
     *
     * @return null, because we don't use a custom loading view.
     */
    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    /**
     * How many different views are used.
     *
     * @return 2, this is hardcoded and must be updated manually.
     */
    @Override
    public int getViewTypeCount() {
        return 4;
    }

    /**
     * Return an id of the row.
     *
     * @param position The position the system needs to know.
     * @return In this case we return the uid of the row.
     */
    //TODO: Proof if it's an problem, having two times the same id.
    @Override
    public long getItemId(int position) {
        return position;
    }


    /**
     * Whether the given {@link #getItemId(int) getItemId} is stable or can be changeable.
     *
     * @return Boolean, whether the ids are stable or not.
     */
    @Override
    public boolean hasStableIds() {
        return true;
    }

    private boolean isFirstAtDay(Cursor cursor, int position) {
        cursor.moveToPosition((position - 1));
        Date start1 = new Date();
        start1.setTime(cursor.getLong(COLUMN_BEGIN));
        cursor.moveToPosition(position);
        Date start2 = new Date();
        start2.setTime(cursor.getLong(COLUMN_BEGIN));
        return !DateHelper.getDateZeroClock(start1, Locale.getDefault()).equals(DateHelper.getDateZeroClock(start2, Locale.getDefault()));
    }
}
