package de.stuv_mosbach.stuvapp.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import androidx.annotation.NonNull;
import android.view.View;
import android.widget.RemoteViews;

import de.stuv_mosbach.stuvapp.MainActivity;
import de.stuv_mosbach.stuvapp.R;
import de.stuv_mosbach.stuvapp.settings.PreferenceKeys;
import de.stuv_mosbach.stuvapp.util.ConfigGlobal;

/**
 * Broadcast the update from the system to the ListProvider
 * @author Peer Beckmann <peer@pbeckmann.de>
 */
public class WidgetProvider extends AppWidgetProvider {

    /**
     * The function will be called by the system on every boot, and whenever the time in the
     * widget-info.xml is up.
     *
     * @param context          The context of the widget.
     * @param appWidgetManager The Manager of all widgets on the HomeScreen.
     * @param appWidgetIds     The ids, where we might want to change something.
     */
    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        for (int appWidgetId : appWidgetIds) {
            RemoteViews remoteViews = updateWidgetListView(context);
            appWidgetManager.updateAppWidget(appWidgetId, remoteViews);
        }

        super.onUpdate(context, appWidgetManager, appWidgetIds);
    }

    /**
     * In some cases, we broadcast an intent, to update the widgets. This function handle this.
     *
     * @param context The context of the widget.
     * @param intent  The intent of the broadcast. If given, we can get additional data from the
     *                intent.
     */
    @Override
    public void onReceive(@NonNull Context context, @NonNull Intent intent) {
        super.onReceive(context, intent);
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
        int appWidgetIds[] = appWidgetManager.getAppWidgetIds(new ComponentName(context, WidgetProvider.class));
        appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetIds, R.id.list_view);
    }

    /**
     * Create the views for the widget. For this we make an intent to the {@link ListProvider}.
     *
     * @param context The context of the widget.
     * @return The created remote views.
     */
    private RemoteViews updateWidgetListView(Context context) {
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_list);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String widgetTheme = sharedPreferences.getString(PreferenceKeys.WIDGET_THEME, "Light");
        switch (widgetTheme) {
            case "Dark":
                remoteViews.setInt(R.id.widget, "setBackgroundColor", Color.parseColor("#424242"));
                remoteViews.setInt(R.id.header, "setBackgroundColor", Color.parseColor("#881619"));
                remoteViews.setTextColor(R.id.empty_list, Color.WHITE);
                remoteViews.setInt(R.id.emptyview_image, "setColorFilter", Color.WHITE);
                break;
            case "Transparent":
                remoteViews.setInt(R.id.widget, "setBackgroundColor", android.R.color.transparent);
                remoteViews.setInt(R.id.header, "setBackgroundColor", Color.parseColor("#55ffffff"));
                remoteViews.setTextColor(R.id.empty_list, Color.WHITE);
                remoteViews.setInt(R.id.emptyview_image, "setColorFilter", Color.WHITE);
                break;
            default:
                break;
        }
        if (sharedPreferences.getBoolean(PreferenceKeys.WIDGET_HIDE_HEADER, false)) {
            remoteViews.setViewVisibility(R.id.header, View.GONE);
        } else {
            remoteViews.setViewVisibility(R.id.header, View.VISIBLE);
        }

        // List content
        Intent svcIntent = new Intent(context, WidgetService.class);
        remoteViews.setRemoteAdapter(R.id.list_view, svcIntent);

        // App start shortcut
        Intent startActivity = new Intent(context, MainActivity.class);
        PendingIntent pendingStartActivityIntent = PendingIntent.getActivity(context, 0, startActivity, 0);
        remoteViews.setOnClickPendingIntent(R.id.widget, pendingStartActivityIntent);
        remoteViews.setPendingIntentTemplate(R.id.list_view, pendingStartActivityIntent);

        // Lunch Menu Shortcut
        Intent startLunch = new Intent(context, MainActivity.class);
        startLunch.setAction(ConfigGlobal.AUTHORITY + ".SHOW_LUNCHPLAN");
        PendingIntent pendingStartLunchIntent = PendingIntent.getActivity(context, 0, startLunch, 0);
        remoteViews.setOnClickPendingIntent(R.id.dinner_icon, pendingStartLunchIntent);

        // Empty view
        remoteViews.setEmptyView(R.id.list_view, R.id.empty_list);

        return remoteViews;
    }
}
