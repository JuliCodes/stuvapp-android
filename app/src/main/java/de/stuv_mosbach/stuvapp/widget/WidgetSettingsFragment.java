package de.stuv_mosbach.stuvapp.widget;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import de.stuv_mosbach.stuvapp.R;

/**
 * Created by Kevin Lackmann on 16.02.2016.
 */
public class WidgetSettingsFragment extends PreferenceFragment {

    public WidgetSettingsFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.widget_preferences);
    }

}
