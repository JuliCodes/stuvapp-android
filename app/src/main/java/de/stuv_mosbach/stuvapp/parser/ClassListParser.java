package de.stuv_mosbach.stuvapp.parser;

import android.content.Context;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Comparator;
import java.util.Set;
import java.util.TreeMap;

/**
 * Diese Klasse ermöglicht es, die Kursliste herunterzuladen und zu parsen. Realisiert als Singleton,
 * um die Arbeitsspeicher-Nutzung zu verringern und gleichzeitige Zugriffe von verschiedenen Objekten
 * zu verhindern.
 *
 * @author Peer Beckmann
 */
public class ClassListParser {
    private static final String fileName = "/calendars.csv";
    private static final ClassListParser ourInstance = new ClassListParser();

    private String absFileName, downloadPath;
    private String[] keys, values;
    private TreeMap<String, String> classIds;
    private boolean downloadActive;
    private DownloadClassList downloadClassList;

    private ClassListParser() {
    }

    /**
     * Obwohl es ein Singleton ist, benötigen wir für einige Funktionalitäten den Kontext des Aufrufers
     *
     * @param context Den Kontext des Aufrufers
     * @return Die Singleton-Instance.
     */
    public static ClassListParser getInstance(Context context) {
        ClassListParser classListParser = ourInstance;
        classListParser.setContext(context);
        return classListParser;
    }

    /**
     * Hier führen wir das eigentliche Kontext hinzufügen aus. Zum einen erstellen wir den absoluten
     * lokalen Pfad der Kursliste, zum anderen lesen wir aus den Einstellungen aus, wo wir die
     * Kursliste herunterladen können.
     *
     * @param context Der Kontext des Aufrufers.
     */
    private void setContext(Context context) {
        absFileName = context.getFilesDir() + fileName;
        downloadPath = PreferenceManager.getDefaultSharedPreferences(context).getString("class_list_url", "");
    }

    public String[] getKeys() {
        return keys;
    }

    public String[] getValues() {
        return values;
    }

    public TreeMap<String, String> getClassIds() {
        return classIds;
    }

    public Boolean getDownloadActive() {
        return downloadActive;
    }

    /**
     * Liefert zurück, ob die Kursliste schon lokal existiert oder nicht.
     *
     * @return Boolean, true, wenn sie existiert, false wenn nicht.
     */
    public Boolean existClassList() {
        File classListFile = new File(absFileName);
        return classListFile.exists();
    }

    /**
     * Hier wird die lokale Kursliste eingelesen und geparst. Dabei werden die Variablen keys, values
     * und classIds befüllt, welche dann mit den Standardgettern abgefragt werden können.
     *
     * @return Boolean, ob die Aktion erfolgreich war. True bei erfolgreichem Abschluss.
     */
    public Boolean parse() {
        File classListFile = new File(absFileName);
        if (!classListFile.exists()) {
            return false;
        }

        try {
            classIds = new TreeMap<>(new Comparator<String>() {
                @Override
                public int compare(String lhs, String rhs) {
                    return lhs.compareTo(rhs);
                }
            });

            BufferedReader bufferedReader = new BufferedReader(
                    new FileReader(absFileName));
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                String[] lineArr = line.split(";");
                if ((lineArr[0].length() != 0) && (!lineArr[0].equals("CALENDARS"))) {
                    classIds.put(lineArr[0], lineArr[1]);
                }
            }

            keys = new String[classIds.size()];
            values = new String[classIds.size()];
            Integer i = 0;
            Set<String> keySet = classIds.keySet();
            for (String key : keySet) {
                keys[i] = key;
                values[i] = classIds.get(key);
                i++;
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    /**
     * Hier geben wir dem Aufrufer die Möglichkeit, den Download der Kursliste anzustoßen.
     */
    public void startDownload() {
        downloadClassList = new DownloadClassList();
        downloadClassList.execute(downloadPath);
    }

    public void cancelDownload() {
        downloadClassList.cancel(true);
    }

    /**
     * Diese Subklasse lädt die neue Kursliste im Hintegrund runter.
     */
    private class DownloadClassList extends AsyncTask<String, Integer, Boolean> {

        /**
         * Hier setzen die Boolean downloadActive auf true, damit der Aufrufer feststellen kann,
         * dass der Download aktuell läuft. Außerdem erstellen wir ein Backup der vorhandenen
         * ClassList um diese bei Fehlern zurückrollen zu können.
         */
        @SuppressWarnings("ResultOfMethodCallIgnored")
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            downloadActive = true;
            File fileOld = new File(absFileName + ".old");
            fileOld.delete();
            File file = new File(absFileName);
            file.renameTo(fileOld);
        }

        /**
         * Hier wird der eigentliche Download ausgeführt.
         *
         * @param params Ein String-Array mit Parametern. Als Parameter 0 wird die URL angegeben.
         * @return Ein Boolean, ob der Download erfolgreich abgeschlossen werden konnte.
         */
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                URL url = new URL(params[0]);
                URLConnection con = url.openConnection();
                con.setConnectTimeout(2000);
                con.setReadTimeout(5000);
                InputStream input = new BufferedInputStream(con.getInputStream(), 4096);

                OutputStream output = new FileOutputStream(absFileName);
                byte data[] = new byte[4096];
                int count;

                while (((count = input.read(data)) != -1) && (!isCancelled())) {
                    output.write(data, 0, count);
                }

                input.close();
                output.flush();
                output.close();
                return !isCancelled();
            } catch (Exception e) {
                return false;
            }
        }

        /**
         * Reroll the class list, if the process is cancelled.
         *
         * @param aBoolean Das Ergebnis aus {@link #doInBackground(String...)}
         */
        @SuppressWarnings("ResultOfMethodCallIgnored")
        @Override
        protected void onCancelled(Boolean aBoolean) {
            super.onCancelled(aBoolean);
            Log.d("Cancelled", "True");
            File fileOld = new File(absFileName + ".old");
            File file = new File(absFileName);
            file.delete();
            fileOld.renameTo(file);
            downloadActive = false;
        }

        /**
         * Hier setzen wir die Boolean downloadActive auf false, damit der Aufrufer festellen kann,
         * dass der Download abgeschlossen ist. Außerdem wird je nach Ergebnis die Liste
         * zurückgesetzt oder das Backup gelöscht.
         *
         * @param aBoolean Der Boolean Wert von {@link #doInBackground(String...)}
         */
        @SuppressWarnings("ResultOfMethodCallIgnored")
        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if (!aBoolean) {
                File fileOld = new File(absFileName + ".old");
                File file = new File(absFileName);
                file.delete();
                fileOld.renameTo(file);
            } else {
                File fileOld = new File(absFileName + ".old");
                fileOld.delete();
            }
            downloadActive = false;
            super.onPostExecute(aBoolean);
        }
    }
}