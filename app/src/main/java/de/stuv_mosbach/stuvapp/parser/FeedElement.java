package de.stuv_mosbach.stuvapp.parser;

import android.content.ContentValues;

import de.stuv_mosbach.stuvapp.contentProvider.FeedContract;

/**
 * A wrapper for feed items.
 *
 * @author Peer Beckmann
 */
public class FeedElement {
    private String title;
    private String content;
    private String description;
    private String imageName;
    private long pubDate;
    private String link;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public long getPubDate() {
        return pubDate;
    }

    public void setPubDate(long pubDate) {
        this.pubDate = pubDate;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    @SuppressWarnings("RedundantIfStatement")
    @Override
    public boolean equals(Object object) {
        if (!(object instanceof FeedElement)) {
            return false;
        }
        FeedElement o = (FeedElement) object;
        if (! title.equals(o.getTitle())) return false;
        if (! content.equals(o.getContent())) return false;
        if (! description.equals(o.getDescription())) return false;
        if (! imageName.equals(o.imageName)) return false;
        if (pubDate != o.getPubDate()) return false;
        if (! link.equals(o.getLink())) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = title != null ? title.hashCode() : 0;
        result = 31 * result + (content != null ? content.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (imageName != null ? imageName.hashCode() : 0);
        result = 31 * result + (int) (pubDate ^ (pubDate >>> 32));
        result = 31 * result + (link != null ? link.hashCode() : 0);
        return result;
    }

    public ContentValues mapToDB() {
        ContentValues cv = new ContentValues();
        cv.put(FeedContract.FeedEntry.COLUMN_TITLE, title);
        cv.put(FeedContract.FeedEntry.COLUMN_CONTENT, content);
        cv.put(FeedContract.FeedEntry.COLUMN_DESCRIPTION, description);
        cv.put(FeedContract.FeedEntry.COLUMN_IMAGE_NAME, imageName);
        cv.put(FeedContract.FeedEntry.COLUMN_LINK, link);
        cv.put(FeedContract.FeedEntry.COLUMN_PUBDATE, pubDate);
        return cv;
    }
}
