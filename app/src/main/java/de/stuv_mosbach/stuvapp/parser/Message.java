package de.stuv_mosbach.stuvapp.parser;

import android.content.ContentValues;

import de.stuv_mosbach.stuvapp.contentProvider.MessageContract;

/**
 * Ein Datenobjekt für die Nachrichten der StuV an die Nutzer.
 *
 * @author Peer Beckmann
 */
@SuppressWarnings("WeakerAccess")
public class Message {
    private String title;
    private String content;
    private int uid = -1;

    private int priority = -1;

    private long begin = -1;
    private long end = -1;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public long getBegin() {
        return begin;
    }

    public void setBegin(long begin) {
        this.begin = begin;
    }

    public long getEnd() {
        return end;
    }

    public void setEnd(long end) {
        this.end = end;
    }

    @SuppressWarnings("RedundantIfStatement")
    public boolean isValid() {
        if (title == null || title.length() <= 0) return false;
        if (content == null || content.length() <= 0) return false;
        if (uid == -1) return false;
        if (priority == -1) return false;
        if (!((priority >= 0 && priority <= 2) || priority == 42)) return false;
        //if (begin != -1) return false;
        //if (end != -1) return false;
        return true;
    }

    @Override
    @SuppressWarnings("SimplifiableIfStatement")
    public boolean equals(Object object) {
        if (!(object instanceof Message)) {
            return false;
        }
        Message o = (Message) object;
        if (uid != o.getUid()) return false;
        if (!title.equals(o.getTitle())) return false;
        if (!content.equals(o.getContent())) return false;
        if (begin != o.getBegin()) return false;
        if (end != o.getEnd()) return false;
        return (priority == o.getPriority());
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += 31 * uid;
        hash += 31 * title.hashCode();
        hash += 31 * content.hashCode();
        hash += 31 * begin;
        hash += 31 * end;
        hash += 31 * priority;
        return hash;
    }

    public ContentValues mapToDB() {
        ContentValues cv = new ContentValues();
        cv.put(MessageContract.MessageEntry.COLUMN_TITLE, title);
        cv.put(MessageContract.MessageEntry.COLUMN_CONTENT, content);
        cv.put(MessageContract.MessageEntry.COLUMN_BEGIN, begin);
        cv.put(MessageContract.MessageEntry.COLUMN_END, end);
        cv.put(MessageContract.MessageEntry.COLUMN_PRIORITY, priority);
        cv.put(MessageContract.MessageEntry.COLUMN_UID, uid);
        return cv;
    }
}
