package de.stuv_mosbach.stuvapp.util;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Build;
import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.cardview.widget.CardView;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Calendar;

import de.stuv_mosbach.stuvapp.R;
import de.stuv_mosbach.stuvapp.settings.PreferenceKeys;

/**
 * Location for the static theme functions.
 *
 * @author Kevin Lackmann
 */
public class ThemeFunctions {

    /**
     * Je nach gewählten Einstellungen wird hier das Theme der App eingestellt und auf Geräten mit mindestens Android Lollipop auch die Statusbar entsprechend eingefärbt.
     */
    public static void changeTheme(Activity activity, SharedPreferences sharedPref) {
        final boolean nightOnly = sharedPref.getBoolean(PreferenceKeys.DARK_THEME_NIGHT_ONLY, false);
        final int currentHour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        final boolean itsNight = (currentHour > 18 || currentHour < 7);

        if (!nightOnly || itsNight) {
            final String theme = sharedPref.getString(PreferenceKeys.THEME, "Light");
            switch (theme) {
                case "Dark":
                    activity.setTheme(R.style.AppThemeDark);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        activity.getWindow().setStatusBarColor(ContextCompat.getColor(activity.getApplicationContext(), R.color.darkThemeStatusBarColor));
                    }
                    break;
                case "Black":
                    activity.setTheme(R.style.AppThemeAmoled);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        activity.getWindow().setStatusBarColor(ContextCompat.getColor(activity.getApplicationContext(), android.R.color.black));
                    }
            }
        }
    }

    /**
     * Manche Views lassen sich mithilfe des XML Layouts nicht theme-abhängig einfärben. Deswegen wird mit folgenden drei Funktionen nachgeholfen.
     */
    public static void colorMessageView(Activity activity, View emptyView) {
        final TypedValue cardColor = new TypedValue();
        final TypedValue titleColor = new TypedValue();
        final TypedValue textColor = new TypedValue();

        final Resources.Theme theme = activity.getTheme();

        theme.resolveAttribute(R.attr.cardColor, cardColor, true);
        theme.resolveAttribute(android.R.attr.textColorPrimary, titleColor, true);
        theme.resolveAttribute(android.R.attr.textColorSecondary, textColor, true);

        CardView card = (CardView) emptyView.findViewById(R.id.card_view);
        card.setCardBackgroundColor(cardColor.data);
        ((TextView) emptyView.findViewById(R.id.header_title)).setTextColor(titleColor.data);
        ((TextView) emptyView.findViewById(R.id.body_text)).setTextColor(textColor.data);
        ((ImageView) emptyView.findViewById(R.id.body_image)).setColorFilter(Color.argb(125, 255, 255, 255));
    }

    public static void colorSwipeRefresh(SwipeRefreshLayout swipeLayout, SharedPreferences sharedPreferences) {
        final boolean nightOnly = sharedPreferences.getBoolean(PreferenceKeys.DARK_THEME_NIGHT_ONLY, false);
        final int currentHour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        final boolean itsNight = (currentHour > 18 || currentHour < 7);
        if (!nightOnly || itsNight) {
            final String theme = sharedPreferences.getString(PreferenceKeys.THEME, "Light");
            switch (theme) {
                case "Dark":
                    swipeLayout.setProgressBackgroundColorSchemeResource(R.color.cardColorDark);
                    break;
                case "Black":
                    swipeLayout.setProgressBackgroundColorSchemeResource(android.R.color.black);
            }
        }
    }
}
