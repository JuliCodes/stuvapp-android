package de.stuv_mosbach.stuvapp.util;

import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.pdf.PdfRenderer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.preference.PreferenceManager;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.widget.ImageView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import de.stuv_mosbach.stuvapp.R;
import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * Created by kevin on 20.05.2016.
 */
//Wird theoretisch nur auf >= Lollipop aufgerufen
@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class PDFRendererActivity extends AppCompatActivity {

    /**
     * Muss im Intend einen korrekten Dateipfad übergeben bekommen
     *
     * @param savedInstanceState See {@link de.stuv_mosbach.stuvapp.MainActivity#onCreate(Bundle)}
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        String filePath = (String)intent.getExtras().get("Filepath");

        ThemeFunctions.changeTheme(this, PreferenceManager.getDefaultSharedPreferences(this));
        setContentView(R.layout.pdf_renderer_layout);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setDisplayHomeAsUpEnabled(true);

        if(filePath==null||filePath.equals("")){
            setTitle("PDF Viewer");
            Snackbar.make(findViewById(R.id.pdf_layout),R.string.pdf_file_not_found,Snackbar.LENGTH_LONG).show();
        }else{
            ImageView imageView = (ImageView) findViewById(R.id.image);
            renderPDF(filePath);

            //Zoom Funktionalität durch Bibliothek
            new PhotoViewAttacher(imageView);

            if(filePath.contains("mensaplan")){
                setTitle(R.string.lunchplan);
            }else if(filePath.contains("buildingplan")){
                setTitle(R.string.buildingplan_title);
            }else {
                setTitle("PDF Viewer");
            }
        }
    }

    private void renderPDF(String filePath){
        // create a new renderer
        PdfRenderer renderer = null;
        //final String pdfPath = getFilesDir() + File.separator + "mensaplan.pdf";
        ParcelFileDescriptor pfd = openFile(Uri.parse(filePath));
        try {
            renderer = new PdfRenderer(pfd);
        } catch (IOException e) {
            e.printStackTrace();
            finish();
        }

        assert renderer != null;
        PdfRenderer.Page page = renderer.openPage(0);

        ImageView image = (ImageView)findViewById(R.id.image);
        // say we render for showing on the screen
        // Important: the destination bitmap must be ARGB (not RGB).
        //Über den Faktor vor "page.getWidth/Height" wird die Auflösung bestimmt
        //Da wir ranzoomen können muss die Auflösung von vornherein etwas höher geschraubt werden
        Bitmap bitmap = Bitmap.createBitmap(4*page.getWidth(), 4*page.getHeight(),
                Bitmap.Config.ARGB_8888);
        // Here, we render the page onto the Bitmap.
        // To render a portion of the page, use the second and third parameter. Pass nulls to get
        // the default result.
        //
        page.render(bitmap, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);

        image.setImageBitmap(bitmap);
        // do stuff with the bitmap
    }

    public ParcelFileDescriptor openFile(Uri uri){
        File f=new File(getFilesDir(),uri.getLastPathSegment());
        ParcelFileDescriptor pfd;
        try {
            pfd=ParcelFileDescriptor.open(f,ParcelFileDescriptor.MODE_READ_ONLY);
        }
        catch (  FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
        return pfd;
    }
}
