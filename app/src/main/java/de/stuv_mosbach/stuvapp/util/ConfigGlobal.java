package de.stuv_mosbach.stuvapp.util;

import android.content.res.Resources;

import org.chaosdaten.commons.DateHelper;

import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Diese Datei enthält alle globalen Konstanten.
 *
 * @author Peer Beckmann
 */
public class ConfigGlobal {
    public static final String AUTHORITY = "de.stuv_mosbach.stuvapp.provider";

    public static final String ACCOUNT_TYPE = "stuv-mosbach.de";

    public static final String ACCOUNT = "StuV Mosbach";

    public static final String NOTIFICATION_CHANNEL_ID = "basic";

    public static final Long timeAfterEventIsHidden = DateHelper.HOUR_IN_MILLI / 2L;

    public static final SimpleDateFormat hourMinute = new SimpleDateFormat("HH:mm", Locale.getDefault());
    public static final SimpleDateFormat dayMonthYear = new SimpleDateFormat("EEE dd.MM.yyyy", Locale.getDefault());
    public static final SimpleDateFormat dayMonthHourMinute = new SimpleDateFormat("dd.LLL HH:mm' Uhr'", Locale.getDefault());

    public static final SimpleDateFormat dayMonth = new SimpleDateFormat("dd.MM", Locale.getDefault());

    public static final SimpleDateFormat date = new SimpleDateFormat("dd", Locale.getDefault());
    public static final SimpleDateFormat day = new SimpleDateFormat("EEE", Locale.getDefault());

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }
}
