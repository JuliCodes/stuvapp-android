package de.stuv_mosbach.stuvapp.util;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.widget.Toast;

import de.stuv_mosbach.stuvapp.R;

/**
 * Diese Klasse hilft an verschiedenen Stellen in der App, den Account zu finden, der für die
 * Synchronisierung notwendig ist.
 *
 * @author Peer Beckmann
 */
public class AccountHelper {
    private static Account account;

    public static Account getAccount(Context context) {
        if (account != null) {
            return account;
        }

        account = new Account(ConfigGlobal.ACCOUNT, ConfigGlobal.ACCOUNT_TYPE);
        AccountManager accountManager = (AccountManager) context.getSystemService(Context.ACCOUNT_SERVICE);

        if (!accountManager.addAccountExplicitly(account, null, null)) {
            // We don't need the permission on API 23 and above, so we don't have to ask the user...
            // See https://developer.android.com/reference/android/Manifest.permission.html#GET_ACCOUNTS
            @SuppressWarnings({"MissingPermission"})
            Account[] accounts = AccountManager.get(context).getAccountsByType(ConfigGlobal.ACCOUNT_TYPE);
            if (accounts[0] != null) {
                account = accounts[0];
            } else {
                Toast.makeText(context, R.string.error_account_creation, Toast.LENGTH_LONG).show();
                throw new RuntimeException("Account initialization failed.");
            }
        }

        return account;
    }
}
