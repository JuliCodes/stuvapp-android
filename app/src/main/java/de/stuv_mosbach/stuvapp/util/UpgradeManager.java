package de.stuv_mosbach.stuvapp.util;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.Log;

import java.io.File;

import de.stuv_mosbach.stuvapp.R;
import de.stuv_mosbach.stuvapp.settings.PreferenceKeys;

/**
 * In dieser Klasse führen wir alle nötigen Operationen aus, um ein Update der App reibunglos
 * durchführen zu können.
 *
 * @author Peer Beckmann
 */
public class UpgradeManager {

    private static final String LOG_NAME = UpgradeManager.class.getName();

    private final Context context;
    private final int oldVersion;
    private int newVersion;

    /**
     * Diese Funktion wird jedes mal aufgerufen, wenn die App echt neu gestartet wird. Über den Context
     * zieht sie sich die zuletzt gespeicherte Versionsnummer und über die PackageInfo die aktuelle.
     * Nur bei einem Unterschied wird die {@link #performUpgrade(SharedPreferences)} aufgerufen, welche
     * dann das eigentliche Upgrade übernimmt. Als letztes speichert diese Funktion dann noch die neue
     * Versionsnummer in den Einstellungen.
     *
     * @param context Der Kontext der App.
     */
    public UpgradeManager(Context context) {
        this.context = context;
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        oldVersion = sharedPref.getInt(PreferenceKeys.APP_VERSION, 0);

        try {
            final PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            newVersion = packageInfo.versionCode;
        } catch (Exception e) {
            //ignore
        }

        if (oldVersion != newVersion)
            performUpgrade(sharedPref);

        sharedPref.edit().putInt(PreferenceKeys.APP_VERSION, newVersion).apply();
    }

    /**
     * Diese Funktion übernimmt die eigentliche Upgrade Aufgabe. Auf Basis der neuen Versionsnummer
     * werden verschiedene IF-Causes abgearbeitet.
     */
    private void performUpgrade(SharedPreferences sharedPreferences) {
        Log.i(LOG_NAME, "performUpgrade from " + oldVersion + " to " + newVersion);

        //Mit der Einführung dieser Funktion wurden einige Sachen mehr variabel gemacht, dadurch müssen wir hier diese Einstellungen festlegen, respektive löschen.
        if (oldVersion == 0) {
            sharedPreferences.edit()
                    .putString(PreferenceKeys.CLASS_LIST_URL, "http://ics.mosbach.dhbw.de/ics/calendars.list")
                    .putString(PreferenceKeys.MESSAGES_URL, "https://stuv-mosbach.de/survival/api/getMessages.php")
                    .putString(PreferenceKeys.MEETINGS_URL, "https://www.google.com/calendar/ical/asta.dhbw.de_08mkcuqcrppq8cg8vlutdsgpjg%40group.calendar.google.com/public/basic.ics")
                    .putString(PreferenceKeys.FEED_URL, "https://stuv-mosbach.de/feed/")
                    .apply();

            sharedPreferences.edit()
                    .remove("lectures_id")
                    .remove("meetings_id")
                    .remove("messages_id")
                    .remove("pref_sync_syscal_meetings")
                    .remove("pref_sync_syscal_lectures")
                    .apply();
        }

        if (oldVersion < 62) {
            File syncStatus = new File(context.getFilesDir() + "/successful_sync");
            if (syncStatus.exists())
                syncStatus.delete();
        }

        if (oldVersion < 68) {
            sharedPreferences.edit()
                    .putString(PreferenceKeys.FEED_URL, "https://stuv-mosbach.de/feed/")
                    .putString(PreferenceKeys.MESSAGES_URL, "https://stuv-mosbach.de/survival/api/getMessages.php")
                    .apply();
        }

        if (oldVersion < 69) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                CharSequence name = context.getString(R.string.notification_channel_name);
                String description = context.getString(R.string.notification_channel_desc);
                int importance = NotificationManager.IMPORTANCE_DEFAULT;
                NotificationChannel channel = new NotificationChannel(ConfigGlobal.NOTIFICATION_CHANNEL_ID, name, importance);
                channel.setDescription(description);
                // Register the channel with the system; you can't change the importance
                // or other notification behaviors after this
                NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
                notificationManager.createNotificationChannel(channel);
            }

            sharedPreferences.edit()
                    .putString(PreferenceKeys.LUNCH_URL, "https://www.studentenwerk.uni-heidelberg.de/sites/default/files/download/pdf/sp-mos-mensa-aktuell.pdf")
                    .putString(PreferenceKeys.PDF_DOCS_URL, "https://docs.google.com/gview?embedded=true&url=")
                    .apply();

            sharedPreferences.edit()
                    .putBoolean(PreferenceKeys.SHOW_NOW, true)
                    .putBoolean(PreferenceKeys.SHOW_LECTURES, true)
                    .putBoolean(PreferenceKeys.SHOW_MEETINGS, true)
                    .putBoolean(PreferenceKeys.SHOW_LUNCH, true)
                    .putBoolean(PreferenceKeys.SHOW_NEWS, false)
                    .putBoolean(PreferenceKeys.SHOW_ABOUT, true)
                    .putBoolean(PreferenceKeys.SHOW_ISSUE, true)
                    .putBoolean(PreferenceKeys.SHOW_SETTINGS, true)
                    .putBoolean(PreferenceKeys.SHOW_TUTORIAL, true)
                    .putBoolean(PreferenceKeys.SHOW_STUV, false)
                    .putBoolean(PreferenceKeys.ENABLE_SYNC_IN_SYSTEM, true)
                    .apply();

        }
    }
}
