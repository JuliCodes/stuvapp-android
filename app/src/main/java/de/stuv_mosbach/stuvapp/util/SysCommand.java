package de.stuv_mosbach.stuvapp.util;

import android.Manifest;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.CalendarContract;
import androidx.core.app.ActivityCompat;
import android.util.Log;

import de.stuv_mosbach.stuvapp.settings.PreferenceKeys;

/**
 * Diese Datei führt die verschiedenen Befehle respektive Kommandos aus.
 *
 * @author Peer Beckmann <peer@pbeckmann.de>
 */
public class SysCommand {
    private final Context context;

    public SysCommand(Context context) {
        this.context = context;
    }

    public void evalCommand(String command, String value) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor sharedPrefEditor = sharedPref.edit();

        Boolean tmp;

        switch (command) {
            case "classListUrl":
                sharedPrefEditor.putString(PreferenceKeys.CLASS_LIST_URL, value).apply();
                break;
            case "classListValue":
                sharedPrefEditor.putString(PreferenceKeys.CLASS_VALUE, value).apply();
                break;
            case "changeMeetingsUrl":
                sharedPrefEditor.putString(PreferenceKeys.MEETINGS_URL, value).apply();
                break;
            case "changeMessagesUrl":
                sharedPrefEditor.putString(PreferenceKeys.MESSAGES_URL, value).apply();
                break;
            case "changeLunchPlanUrl":
                sharedPrefEditor.putString(PreferenceKeys.LUNCH_URL, value).apply();
                break;
            case "changePDFDocsUrl":
                sharedPrefEditor.putString(PreferenceKeys.PDF_DOCS_URL, value).apply();
                break;
            case "changeFeedUrl":
                sharedPrefEditor.putString(PreferenceKeys.FEED_URL, value).apply();
                break;
            case "show_now":
                tmp = value.equals("1");
                sharedPrefEditor.putBoolean(PreferenceKeys.SHOW_NOW, tmp).apply();
                break;
            case "show_lectures":
                tmp = value.equals("1");
                sharedPrefEditor.putBoolean(PreferenceKeys.SHOW_LECTURES, tmp).apply();
                break;
            case "show_meetings":
                tmp = value.equals("1");
                sharedPrefEditor.putBoolean(PreferenceKeys.SHOW_MEETINGS, tmp).apply();
                break;
            case "show_news":
                tmp = value.equals("1");
                sharedPrefEditor.putBoolean(PreferenceKeys.SHOW_NEWS, tmp).apply();
                break;
            case "show_lunch":
                tmp = value.equals("1");
                sharedPrefEditor.putBoolean(PreferenceKeys.SHOW_LUNCH, tmp).apply();
                break;
            case "show_stuv":
                tmp = value.equals("1");
                sharedPrefEditor.putBoolean(PreferenceKeys.SHOW_STUV, tmp).apply();
                break;
            case "show_settings":
                tmp = value.equals("1");
                sharedPrefEditor.putBoolean(PreferenceKeys.SHOW_SETTINGS, tmp).apply();
                break;
            case "show_tutorial":
                tmp = value.equals("1");
                sharedPrefEditor.putBoolean(PreferenceKeys.SHOW_TUTORIAL, tmp).apply();
                break;
            case "show_issue":
                tmp = value.equals("1");
                sharedPrefEditor.putBoolean(PreferenceKeys.SHOW_ISSUE, tmp).apply();
                break;
            case "show_about":
                tmp = value.equals("1");
                sharedPrefEditor.putBoolean(PreferenceKeys.SHOW_ABOUT, tmp).apply();
                break;
            case "disable_sync_in_system":
                if (value.equals("1")) {
                    sharedPrefEditor.putBoolean(PreferenceKeys.ENABLE_SYNC_IN_SYSTEM, false).apply();
                } else {
                    sharedPrefEditor.putBoolean(PreferenceKeys.ENABLE_SYNC_IN_SYSTEM, true).apply();
                    break;
                }

                if (sharedPref.getBoolean(PreferenceKeys.SYNC_IN_SYSTEM, false)) {
                    break;
                }

                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_CALENDAR) == PackageManager.PERMISSION_GRANTED &&
                        ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_CALENDAR) == PackageManager.PERMISSION_GRANTED) {
                    break;
                }

                // Remove the existing calendars
                String[] CALENDAR_PROJECTION = new String[]{
                        CalendarContract.Calendars._ID
                };

                ContentResolver cr = context.getContentResolver();
                Uri uri = CalendarContract.Calendars.CONTENT_URI;
                String selection = "(" + CalendarContract.Calendars.ACCOUNT_TYPE + " = ?)";
                String[] selectionArgs = new String[]{ConfigGlobal.ACCOUNT_TYPE};

                Cursor cursor = cr.query(uri, CALENDAR_PROJECTION, selection, selectionArgs, null);

                while (cursor != null && cursor.moveToNext()) {
                    Uri uri_tmp = ContentUris.withAppendedId(CalendarContract.Calendars.CONTENT_URI, cursor.getInt(0));
                    uri.buildUpon()
                            .appendQueryParameter(CalendarContract.CALLER_IS_SYNCADAPTER, "true")
                            .appendQueryParameter(CalendarContract.Calendars.ACCOUNT_NAME, ConfigGlobal.ACCOUNT)
                            .appendQueryParameter(CalendarContract.Calendars.ACCOUNT_TYPE, ConfigGlobal.ACCOUNT_TYPE).build();

                    cr.delete(uri_tmp, null, null);
                }

                if (cursor != null) {
                    cursor.close();
                }

                //Disable the setting. But the user can reactivate it...
                sharedPrefEditor.putBoolean(PreferenceKeys.SYNC_IN_SYSTEM, false).apply();
                break;
            case "enable" + PreferenceKeys.BSOD:
                sharedPrefEditor.putBoolean(PreferenceKeys.BSOD, true).apply();
                sharedPrefEditor.putString(PreferenceKeys.BSOD_MESSAGE, value).apply();
                break;
            case "disable" + PreferenceKeys.BSOD:
                sharedPrefEditor.putBoolean(PreferenceKeys.BSOD, false).apply();
                sharedPrefEditor.putString(PreferenceKeys.BSOD_MESSAGE, "").apply();
                break;
            default:
                Log.i("Sys", command);
        }
    }

}
