package de.stuv_mosbach.stuvapp;

import android.Manifest;
import android.accounts.Account;
import android.app.AlertDialog;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.app.ActivityCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import org.chaosdaten.commons.DateHelper;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import de.stuv_mosbach.stuvapp.listFragments.latest.LatestListFragment;
import de.stuv_mosbach.stuvapp.navigation.NavigationHelper;
import de.stuv_mosbach.stuvapp.settings.PreferenceKeys;
import de.stuv_mosbach.stuvapp.startup.Startup;
import de.stuv_mosbach.stuvapp.sync.MySyncStatusObserver;
import de.stuv_mosbach.stuvapp.util.AccountHelper;
import de.stuv_mosbach.stuvapp.util.ConfigGlobal;
import de.stuv_mosbach.stuvapp.util.ThemeFunctions;
import de.stuv_mosbach.stuvapp.util.UpgradeManager;

/**
 * Die ist die Hauptklasse der App, aus der alle anderen Funktionen, Klassen und App-Elemente
 * indirekt aufgerufen werden.
 *
 * @author Peer Beckmann
 */

public class MainActivity extends AppCompatActivity implements
        MySyncStatusObserver.Callback,
        LatestListFragment.onClickEvent,
        SwipeRefreshLayout.OnRefreshListener {

    private static final String LOG_NAME = MainActivity.class.getName();

    //Eine Referenz zum Account. Wird in der {@link #onCreate} erstellt.
    private static Account account;

    //Referenz auf die Navigations View.
    private NavigationView navigationView;

    //Referenz aus das DrawerLayout, nötig um ihn auf Kommando zu schließen.
    private DrawerLayout drawerLayout;

    //Swipe, brauchen wir für das Pull-To-Refresh
    private SwipeRefreshLayout swipeLayout;
    private Boolean syncIsActive = false;
    private Handler handler;
    private final Runnable refreshing = new Runnable() {
        @Override
        public void run() {
            if (syncIsActive) {
                handler.postDelayed(this, 100);
            } else {
                swipeLayout.setRefreshing(false);
            }
        }
    };
    private NavigationHelper navigationHelper;

    //Merken des Themes, um eine Änderung dessen festzustellen und die Activity gegebenenfalls neuzustarten.
    private String activeTheme;
    private boolean nightModePref;
    private boolean lastIsNight;

    //Wird für das TabletLayout gebraucht. Wenn das TabletLayout verwendet wird darf der Drawer nicht geöffnet oder geschlossen werden. Das wird hierüber geprüft.
    private boolean isDrawerLocked = false;

    private static final int INTRO_REQUEST_CODE = 1;

    /**
     * Diese Funktion wird immer aufgerufen, wenn die App vom System von 0 aufgebaut werden muss.
     * Hier setzen wir das Layout fest, initialisieren die ActionBar und die Navigation. Zuletzt
     * registrieren wir einen Receiver um zu erkennen, wann eine Synchronisierung gestartet bzw.
     * beendet wird.
     *
     * @param savedInstanceState Wird bei verschiedenen Anlässen vom System erstellt und übergeben.
     *                           Kann dazu verwendet werden Argumente zu übergeben, oder die App neu
     *                           aufzubauen, ohne viel Logik nutzen zu müssen.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(LOG_NAME, "onCreate");

        account = AccountHelper.getAccount(this);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        ThemeFunctions.changeTheme(this, sharedPreferences);
        //Merken des Themes, um eine Änderung dessen aus den Einstellungen festzustellen und die Activity gegebenenfalls neuzustarten.
        activeTheme = sharedPreferences.getString(PreferenceKeys.THEME, "Light");
        nightModePref = sharedPreferences.getBoolean(PreferenceKeys.DARK_THEME_NIGHT_ONLY, false);

        //Hier wird sich die Tageszeit gemerkt, und in der onResume wird geschaut ob sie sich geändert hat damit das Theme eventuell angepasst werden kann
        int currentHour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        lastIsNight = (currentHour > 18 || currentHour < 7);

        setContentView(R.layout.activity_main);

        initActionBar();

        initDrawer();

        ContentResolver.addStatusChangeListener(
                ContentResolver.SYNC_OBSERVER_TYPE_PENDING
                        | ContentResolver.SYNC_OBSERVER_TYPE_ACTIVE,
                new MySyncStatusObserver(account, this));

        swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);
        swipeLayout.setColorSchemeResources(R.color.colorPrimary);

        ThemeFunctions.colorSwipeRefresh(swipeLayout, sharedPreferences);

        swipeLayout.setOnRefreshListener(this);

        new UpgradeManager(this);

        if (!sharedPreferences.getBoolean(PreferenceKeys.INTRO_FINISHED, false)) {
            Intent intent = new Intent(this, Startup.class);
            startActivityForResult(intent, INTRO_REQUEST_CODE);
        }

        navigationHelper = new NavigationHelper(getFragmentManager(), this, this);
        navigationHelper.navigateByIntent(getIntent());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        if (requestCode == INTRO_REQUEST_CODE && resultCode == Startup.RESULT_OK) {
            sharedPreferences.edit().putBoolean(PreferenceKeys.INTRO_FINISHED, true).apply();
        }
    }

    /**
     * Diese Funktion wird immer aufgerufen, wenn die App in den Vordergrund kommt. Sie wird auch
     * immer ausgeführt, wenn vorher {@link #onCreate(Bundle)} ausgeführt wurde.
     */
    @Override
    protected void onResume() {
        super.onResume();
        Log.i(LOG_NAME, "onResume");

        //Auf Tablets im Landscape wird der Drawer immer angezeigt und das Hamburger Icon ausgeblendet.
        if (drawerLayout.getTag().equals("large-land")) {
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_OPEN);
            drawerLayout.setScrimColor(Color.parseColor("#00000000"));
            final ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.setDisplayShowHomeEnabled(false);
                actionBar.setDisplayHomeAsUpEnabled(false);
            }
            isDrawerLocked = true;
        } else {
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            final ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.setDisplayShowHomeEnabled(true);
                actionBar.setDisplayHomeAsUpEnabled(true);
            }
            isDrawerLocked = false;
        }

        //Fall1: Recreate wenn das Theme in den Einstellungen geändert wurde, ansonten würde es erst bei einem Neustart der App sichtbar werden
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        boolean newNightOnlyPref = sharedPreferences.getBoolean(PreferenceKeys.DARK_THEME_NIGHT_ONLY, false);
        String newActiveTheme = sharedPreferences.getString(PreferenceKeys.THEME, "Light");

        //Fall 2: Recreate wenn das Theme nur Nachts genutzt wird und sich die Tageszeit geändert hat
        int currentHour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        boolean newIsNight = (currentHour > 18 || currentHour < 7);

        //  <--------------------------------Fall 1------------------------------------>     <------------------Fall 2-------------------->
        if ((nightModePref != newNightOnlyPref) || (!activeTheme.equals(newActiveTheme)) || (nightModePref && (lastIsNight != newIsNight))) {
            lastIsNight = newIsNight;
            Intent intent2 = new Intent(this, MainActivity.class);
            intent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent2);
            Log.d(LOG_NAME, "Recreate für Themewechsel");
        }

        if (ContentResolver.isSyncActive(MainActivity.account, ConfigGlobal.AUTHORITY) ||
                ContentResolver.isSyncPending(MainActivity.account, ConfigGlobal.AUTHORITY)) {
            swipeLayout.setRefreshing(true);
            syncIsActive = true;
            handler = new Handler();
            handler.post(refreshing);
        }
    }

    /**
     * Diese Funktion wird immer direkt nach {@link #onResume()} aufgerufen. Hier sollen Funktionen
     * ausgerufen werden, die möglicherweise den Aufbau der App verlangsamen könnten.
     * Hier überprüfen wir, wie alt die lokalen Daten sind und geben, wenn diese älter als 3 Tage
     * sind eine Fehlermeldung aus.
     */
    @Override
    protected void onPostResume() {
        super.onPostResume();
        Log.i(LOG_NAME, "onPostResume");

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        long lastSuccessSync = sharedPreferences.getLong(PreferenceKeys.LAST_SUCCESS_SYNC, 0);
        if (lastSuccessSync + (3 * DateHelper.DAY_IN_MILLI) > new Date().getTime() && sharedPreferences.getString(PreferenceKeys.CLASS, null) != null) {
            Snackbar.make(findViewById(R.id.contentPane), getString(R.string.messages_synchronisation_lasttime) + " " + ConfigGlobal.dayMonthHourMinute.format(lastSuccessSync), Snackbar.LENGTH_LONG).show();
        } else if (sharedPreferences.getString(PreferenceKeys.CLASS, null) != null && lastSuccessSync != 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(getString(R.string.error_synchronisation_title));
            builder.setMessage(getString(R.string.error_synchronisation_content));
            builder.setPositiveButton(getString(R.string.error_synchronisation_accept), (dialog, id) -> {});
            builder.show();
        }

        if (sharedPreferences.getBoolean(PreferenceKeys.SYNC_IN_SYSTEM, false) && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!((ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CALENDAR) == PackageManager.PERMISSION_GRANTED) &&
                    (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_CALENDAR) == PackageManager.PERMISSION_GRANTED))) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(this);
                dialog.setTitle(R.string.error_permission_title);
                dialog.setMessage(R.string.error_permission_description);
                dialog.setCancelable(false);

                dialog.setPositiveButton(R.string.error_permission_repair, (dialog1, which) -> requestPermissions(new String[]{Manifest.permission.READ_CALENDAR, Manifest.permission.WRITE_CALENDAR}, 23));
                dialog.setNegativeButton(R.string.error_permission_ignore, (dialog12, which) -> {});

                dialog.create().show();
            }
        }

        navigationHelper.updateMenuEntries();

        if (sharedPreferences.getBoolean(PreferenceKeys.BSOD, false)) {
            String message = sharedPreferences.getString(PreferenceKeys.BSOD_MESSAGE, "This is a blue screen of death.");

            TextView textView = new TextView(this);
            textView.setAutoLinkMask(RESULT_OK);
            textView.setMovementMethod(LinkMovementMethod.getInstance());
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                textView.setTextAppearance(android.R.style.TextAppearance_Small);
            } else {
                //noinspection deprecation
                textView.setTextAppearance(this, android.R.style.TextAppearance_Small);
            }
            final int dp5 = ConfigGlobal.dpToPx(25);
            textView.setPadding(dp5, 0, dp5, 0);
            textView.setText(new SpannableString(message));

            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle("Blue Screen of Death");
            alert.setView(textView);
            alert.setCancelable(false);
            alert.setPositiveButton("Ok", (dialog, which) -> finish());
            alert.create().show();
        }
    }

    /**
     * Diese Funktion wird aufgerufen, wenn einer der Einträge im ActionBar Menü angeklickt wird.
     * Welcher Eintrag angeklickt wird, kann über die Item Id festgestellt werden. Die Fragmente die
     * eingebunden werden rufen zuerst auch diese auf. Deswegen gibt es keine Doppelimplementationen
     * gemeinsamer Menüeinträge.
     *
     * @param menuItem Der angeklickte Menüeintrag.
     * @return Nur ein Rückgabewert.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int id = menuItem.getItemId();

        if (id == android.R.id.home) {
            drawerLayout.openDrawer(GravityCompat.START);
        } else if (id == R.id.action_cleanup) {
            invalidateOptionsMenu();
            ArrayList<ContentProviderOperation> operations = new ArrayList<>();
            operations.add(ContentProviderOperation.newDelete(Uri.parse("content://" + ConfigGlobal.AUTHORITY + "/cleanup")).build());
            try {
                getContentResolver().applyBatch(ConfigGlobal.AUTHORITY, operations);
                return true;
            } catch (RemoteException | OperationApplicationException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    /**
     * Wenn ein Eintrag in der "Aktuell" Ansicht angeklickt wird, wird hierher getriggert, welche
     * Ansicht jetzt zu öffnen ist.
     *
     * @param type Der Typ, übergeben als Integer Wert.
     */
    @Override
    public void clickHome(int type) {
        Log.i(LOG_NAME, "clickHome");
        navigationHelper.navigateById(type);
        navigationView.getMenu().findItem(type).setChecked(true);
    }

    /**
     * Wird aufgerufen in dem Moment, wenn eine Synchronisation beendet wurde.
     * Lasst uns das shit pull to refresh beenden.
     */
    @Override
    public void onSyncsFinished() {
        Log.i(LOG_NAME, "onSyncsFinished");
        syncIsActive = false;
        invalidateOptionsMenu();
    }

    @Override
    public void onRefresh() {
        Log.i(LOG_NAME, "onRefresh");
        syncIsActive = true;
        Bundle settingsBundle = new Bundle();
        settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        ContentResolver.requestSync(account, ConfigGlobal.AUTHORITY, settingsBundle);
        handler = new Handler();
        handler.post(refreshing);
    }

    /**
     * Erstellung der ActionBar. Wir machen dies manuell, damit wir unser eigenes Design verwenden
     * können.
     */
    private void initActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);

        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    /**
     * Erstellung der klappbaren Navigation. Zuerst finden wir es. :D Dann erstellen wir einen
     * leeren OnClick Listener für die HeaderView, da dies die OnClick Animation verhindert.
     * Anschließend folgt der echte OnClick Listener für die eigentlichen Elemente.
     */
    private void initDrawer() {
        Log.i(LOG_NAME, "initDrawer");
        drawerLayout = (DrawerLayout) findViewById(R.id.DrawerLayout);
        navigationView = (NavigationView) findViewById(R.id.navigation_view);

        navigationView.setNavigationItemSelectedListener(menuItem -> {
            menuItem.setChecked(true);
            if (!isDrawerLocked) {
                drawerLayout.closeDrawer(GravityCompat.START);
            }

            navigationHelper.navigateById(menuItem.getItemId());
            return true;
        });

        drawerLayout.addDrawerListener(new DrawerLayout.SimpleDrawerListener() {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

                SearchView searchView = (SearchView) drawerView.getRootView().findViewById(R.id.action_search);
                if (searchView != null) {
                    searchView.clearFocus();
                }
            }
        });
    }

    //Diese Funktion wird von dem Button in der Mittagsessen-Nachricht aufgerufen.
    public void showLunchplan(View view) {
        navigationHelper.navigateById(R.id.navigation_lunch);
    }
}
