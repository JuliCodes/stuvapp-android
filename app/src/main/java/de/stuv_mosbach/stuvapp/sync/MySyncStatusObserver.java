package de.stuv_mosbach.stuvapp.sync;

import android.accounts.Account;
import android.content.ContentResolver;
import android.content.SyncStatusObserver;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import de.stuv_mosbach.stuvapp.util.ConfigGlobal;

/**
 * Sync status observer that reports back via a callback interface when syncing has begun
 * and finished.
 */
public class MySyncStatusObserver implements SyncStatusObserver {
    /**
     * The original list of accounts that are being synced.
     */
    @NonNull
    private final Account mAccounts;
    /**
     * The calendar authority we're listening for syncs on.
     */
    @NonNull
    private final String mCalendarAuthority;
    /**
     * Callback implementation.
     */
    @Nullable
    private final Callback mCallback;
    /**
     * Map of accounts and their current sync states.
     */
    private SyncState mAccountSyncState;
    /**
     * {@code true} when a "sync started" callback has been called.
     * <p/>
     * <p>Keeps us from reporting this event more than once.</p>
     */
    private boolean mSyncStartedReported;

    /**
     * Default constructor.
     *
     * @param accounts the accounts to monitor syncing for
     * @param callback optional callback interface to receive events
     */
    public MySyncStatusObserver(@NonNull final Account accounts,
                                @Nullable final Callback callback) {
        mAccounts = accounts;
        mCalendarAuthority = ConfigGlobal.AUTHORITY;
        mCallback = callback;
    }

    @Override
    public void onStatusChanged(int which) {
        if (which == ContentResolver.SYNC_OBSERVER_TYPE_PENDING) {
            if (ContentResolver.isSyncPending(mAccounts, mCalendarAuthority)) {
                // There is now a pending sync.
                mAccountSyncState = SyncState.PENDING;
            } else {
                // There is no longer a pending sync.
                mAccountSyncState = SyncState.PENDING_ACTIVE;
            }
        } else if (which == ContentResolver.SYNC_OBSERVER_TYPE_ACTIVE) {
            if (ContentResolver.isSyncActive(mAccounts, mCalendarAuthority)) {
                // There is now an active sync.
                mAccountSyncState = SyncState.ACTIVE;

                if (!mSyncStartedReported && mCallback != null) {
                    mSyncStartedReported = true;
                }
            } else {
                // There is no longer an active sync.
                mAccountSyncState = SyncState.FINISHED;
            }
        }

        if (mAccountSyncState != SyncState.FINISHED) return;

        // 2. Report back that all syncs are finished
        if (mCallback != null) {
            mCallback.onSyncsFinished();
        }
    }

    /**
     * Defines the various sync states for an account.
     */
    private enum SyncState {
        /**
         * Indicates a sync is pending.
         */
        PENDING,
        /**
         * Indicates a sync is no longer pending but isn't active yet.
         */
        PENDING_ACTIVE,
        /**
         * Indicates a sync is active.
         */
        ACTIVE,
        /**
         * Indicates syncing is finished.
         */
        FINISHED
    }

    /**
     * Lifecycle events.
     */
    public interface Callback {

        /**
         * Indicates syncing of calendars has finished.
         */
        void onSyncsFinished();
    }
}