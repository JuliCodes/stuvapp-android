package de.stuv_mosbach.stuvapp.sync.syncer;

import android.content.ContentProviderClient;
import android.content.Context;
import android.content.SyncResult;

import java.net.URL;

import de.stuv_mosbach.stuvapp.sync.localSources.LocalSource;

/**
 * Dieses Interface soll alle Synchronisierungsadapter auf ein gleiches Niveau ziehen, damit diese
 * performant und einfach aufzurufen sind.
 *
 * @author Peer Beckmann
 */
public interface Syncer {

    /**
     * Diese Funktion dient dazu alle wichtigen Parameter an die einzelnen Syncer zu übergeben.
     *
     * @param context               Der Context der Anwendung.
     * @param contentProviderClient Der Client um auf die Datenbank schreibend zuzugreifen.
     * @param source                Die Quelle im Internet aus dem die Daten kommen soll.
     */
    void initialize(Context context, ContentProviderClient contentProviderClient, URL source, LocalSource localSource);

    /**
     * Diese Funktion trifft alle Vorbereitungen um den Sync erfolgreich durchzuführen. Hierzu zählt
     * unter anderem der Download der neuen Daten.
     */
    void prepare();

    /**
     * Hier soll der eigentliche Sync geschehen. Also die Kalkulierung der Differenzen zwischen der
     * lokalen und entfernten Quelle und auch die eigentlichen Zugriffe auf die Datenbank.
     */
    void run(SyncResult syncResult);

    void notificate(String title, String message);
}
