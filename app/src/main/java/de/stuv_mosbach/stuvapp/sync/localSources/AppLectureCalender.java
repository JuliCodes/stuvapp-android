package de.stuv_mosbach.stuvapp.sync.localSources;

import android.content.ContentProviderClient;
import android.content.ContentProviderOperation;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;

import biweekly.component.VEvent;
import de.stuv_mosbach.stuvapp.contentProvider.LectureContract.LectureEntry;
import de.stuv_mosbach.stuvapp.util.ConfigGlobal;
import de.stuv_mosbach.stuvapp.veventutils.VEventHelper;

/**
 * Ein Abbild des Vorlesungskalenders der App, basierend auf dem {@link VEventCalendar}.
 *
 * @author Peer Beckmann
 */
public class AppLectureCalender extends VEventCalendar {
    private static final String LOG_NAME = AppLectureCalender.class.getName();


    private static final int COLUMN_TITLE = 0;
    private static final int COLUMN_LOCATION = 1;
    private static final int COLUMN_DESCRIPTION = 2;
    private static final int COLUMN_BEGIN = 3;
    private static final int COLUMN_END = 4;
    private static final int COLUMN_UID = 5;

    private static final String[] LECTURE_PROJECTION = new String[]{
            LectureEntry.COLUMN_TITLE,
            LectureEntry.COLUMN_LOCATION,
            LectureEntry.COLUMN_DESCRIPTION,
            LectureEntry.COLUMN_BEGIN,
            LectureEntry.COLUMN_END,
            LectureEntry._ID
    };

    @Override
    public void setRemoteData(LinkedList<VEvent> serverVersion) {
        Log.i(LOG_NAME, "setRemoteData");
        this.serverVersion = serverVersion;
    }

    @Override
    public void initialize(Context context) {
        Log.i(LOG_NAME, "initialize");
        super.initialize(context);
        getLocalEvents();
    }

    @Override
    public void calculateDifferences() {
        Log.i(LOG_NAME, "calculateDifferences");
        super.calculateDifferences();
    }

    @Override
    public LocalSourceResult runDifferences(ContentProviderClient contentProviderClient) {
        Log.i(LOG_NAME, "runDifferences");
        ArrayList<ContentProviderOperation> operations = new ArrayList<>();
        LocalSourceResult result = new LocalSourceResult();
        result.setAmountOfInserts(serverVersion.size());
        result.setAmountOfDeletes(localVersion.size());
        result.setAmountOfUpdates(changedLocations.size() + changedDatesList.size());

        for (VEvent event : localVersion) {
            operations.add(
                    ContentProviderOperation.newUpdate(
                            Uri.parse("content://" + ConfigGlobal.AUTHORITY + "/lectures/"))
                            .withSelection(LectureEntry._ID + " = ?", new String[]{VEventHelper.getTextProperty(event.getUid())})
                            .withValue(LectureEntry.COLUMN_DELETED, 1)
                            .build()
            );
        }

        for (VEvent event : serverVersion) {
            operations.add(ContentProviderOperation.newInsert(LectureEntry.CONTENT_URI)
                    .withValue(LectureEntry.COLUMN_TITLE, VEventHelper.getTextProperty(event.getSummary()))
                    .withValue(LectureEntry.COLUMN_BEGIN, event.getDateStart().getValue().getTime())
                    .withValue(LectureEntry.COLUMN_DESCRIPTION, VEventHelper.getTextProperty(event.getDescription()))
                    .withValue(LectureEntry.COLUMN_END, event.getDateEnd().getValue().getTime())
                    .withValue(LectureEntry.COLUMN_LAST_MODIFIED, event.getLastModified().getValue().getTime())
                    .withValue(LectureEntry.COLUMN_UID, VEventHelper.getTextProperty(event.getUid()))
                    .withValue(LectureEntry.COLUMN_LOCATION, VEventHelper.getTextProperty(event.getLocation()))
                    .withValue(LectureEntry.COLUMN_NEW, 1)
                    .withValue(LectureEntry.COLUMN_FULL_DAY, VEventHelper.isFullDay(event.getDateStart(), event.getDateEnd()))
                    .withValue(LectureEntry.COLUMN_MORE_DAYS, VEventHelper.isMultiDay(event.getDateStart(), event.getDateEnd()))
                    .build()
            );
        }

        for (VEvent event : changedLocations) {
            operations.add(
                    ContentProviderOperation
                            .newUpdate(LectureEntry.CONTENT_URI)
                            .withSelection(LectureEntry.COLUMN_TITLE + " = ? AND " + LectureEntry.COLUMN_DESCRIPTION + " = ? AND " + LectureEntry.COLUMN_BEGIN + " = ? AND " + LectureEntry.COLUMN_END + " = ?",
                                    new String[]{VEventHelper.getTextProperty(event.getSummary()),
                                            VEventHelper.getTextProperty(event.getDescription()),
                                            String.valueOf(event.getDateStart().getValue().getTime()),
                                            String.valueOf(event.getDateEnd().getValue().getTime())})
                            .withValue(LectureEntry.COLUMN_UID, VEventHelper.getTextProperty(event.getUid()))
                            .withValue(LectureEntry.COLUMN_LOCATION, VEventHelper.getTextProperty(event.getLocation()))
                            .withValue(LectureEntry.COLUMN_CHANGED_LOCATION, 1)
                            .build()
            );
        }

        for (VEvent event : changedDatesList) {
            operations.add(
                    ContentProviderOperation
                            .newUpdate(LectureEntry.CONTENT_URI)
                            .withSelection(LectureEntry.COLUMN_TITLE + " = ? AND " + LectureEntry.COLUMN_LOCATION + " = ? AND " + LectureEntry.COLUMN_DESCRIPTION + " = ? AND " + LectureEntry.COLUMN_UID + " = ?",
                                    new String[]{VEventHelper.getTextProperty(event.getSummary()),
                                            VEventHelper.getTextProperty(event.getLocation()),
                                            VEventHelper.getTextProperty(event.getLocation()),
                                            VEventHelper.getTextProperty(event.getUid())})
                            .withValue(LectureEntry.COLUMN_UID, VEventHelper.getTextProperty(event.getUid()))
                            .withValue(LectureEntry.COLUMN_BEGIN, event.getDateStart().getValue().getTime())
                            .withValue(LectureEntry.COLUMN_END, event.getDateEnd().getValue().getTime())
                            .withValue(LectureEntry.COLUMN_CHANGED_DATE, 1)
                            .build()
            );
        }

        try {
            contentProviderClient.applyBatch(operations);
        } catch (Exception e) {
            Log.e(LOG_NAME + "#runDifferences", String.valueOf(e.getMessage()));
            result.setError(true);
        }

        return result;
    }

    private void getLocalEvents() {
        Log.i(LOG_NAME, "getLocalEvents");
        Cursor cursor = contentResolver.query(
                LectureEntry.CONTENT_URI,
                LECTURE_PROJECTION,
                LectureEntry.COLUMN_DELETED + " is not ?",
                new String[]{"1"}, null);

        if (cursor == null)
            return;

        while (cursor.moveToNext()) {
            VEvent event = new VEvent();
            event.setDateEnd(new Date(cursor.getLong(COLUMN_END)));
            event.setDateStart(new Date(cursor.getLong(COLUMN_BEGIN)));

            String description = cursor.getString(COLUMN_DESCRIPTION);
            if (description.length() > 0)
                event.setDescription(description);

            String location = cursor.getString(COLUMN_LOCATION);
            if (location.length() > 0)
                event.setLocation(location);

            event.setSummary(cursor.getString(COLUMN_TITLE));
            event.setUid(cursor.getString(COLUMN_UID));
            localVersion.add(event);
        }

        cursor.close();
    }
}
