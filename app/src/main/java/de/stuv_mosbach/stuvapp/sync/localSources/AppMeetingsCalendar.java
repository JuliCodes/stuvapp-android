package de.stuv_mosbach.stuvapp.sync.localSources;

import android.content.ContentProviderClient;
import android.content.ContentProviderOperation;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;

import biweekly.component.VEvent;
import de.stuv_mosbach.stuvapp.contentProvider.MeetingContract.MeetingEntry;
import de.stuv_mosbach.stuvapp.util.ConfigGlobal;
import de.stuv_mosbach.stuvapp.veventutils.VEventHelper;

/**
 * Ein Abbild des Veranstaltungskalenders der App, basierend auf dem {@link VEventCalendar}.
 *
 * @author Peer Beckmann
 */
public class AppMeetingsCalendar extends VEventCalendar {
    private static final String LOG_NAME = AppMeetingsCalendar.class.getName();


    private static final int COLUMN_TITLE = 0;
    private static final int COLUMN_LOCATION = 1;
    private static final int COLUMN_DESCRIPTION = 2;
    private static final int COLUMN_BEGIN = 3;
    private static final int COLUMN_END = 4;
    private static final int COLUMN_UID = 5;

    private static final String[] MEETING_PROJECTIONS = new String[]{
            MeetingEntry.COLUMN_TITLE,
            MeetingEntry.COLUMN_LOCATION,
            MeetingEntry.COLUMN_DESCRIPTION,
            MeetingEntry.COLUMN_BEGIN,
            MeetingEntry.COLUMN_END,
            MeetingEntry._ID
    };

    @Override
    public void setRemoteData(LinkedList<VEvent> serverVersion) {
        Log.i(LOG_NAME, "setRemoteData");
        this.serverVersion = serverVersion;
    }

    @Override
    public void initialize(Context context) {
        Log.i(LOG_NAME, "initialize");
        super.initialize(context);
        getLocalEvents();
    }

    @Override
    public LocalSourceResult runDifferences(ContentProviderClient contentProviderClient) {
        Log.i(LOG_NAME, "runDifferences");
        ArrayList<ContentProviderOperation> operations = new ArrayList<>();
        LocalSourceResult result = new LocalSourceResult();
        result.setAmountOfInserts(serverVersion.size());
        result.setAmountOfDeletes(localVersion.size());
        result.setAmountOfUpdates(changedLocations.size() + changedDatesList.size());

        for (VEvent event : localVersion) {
            operations.add(
                    ContentProviderOperation.newUpdate(
                            Uri.parse("content://" + ConfigGlobal.AUTHORITY + "/student_events/"))
                            .withSelection(MeetingEntry._ID + " = ?", new String[]{VEventHelper.getTextProperty(event.getUid())})
                            .withValue(MeetingEntry.COLUMN_DELETED, 1)
                            .build()
            );
        }

        for (VEvent event : serverVersion) {
            ContentValues values = new ContentValues();
            values.put(MeetingEntry.COLUMN_TITLE, VEventHelper.getTextProperty(event.getSummary()));
            values.put(MeetingEntry.COLUMN_BEGIN, event.getDateStart().getValue().getTime());
            values.put(MeetingEntry.COLUMN_DESCRIPTION, VEventHelper.getTextProperty(event.getDescription()));
            values.put(MeetingEntry.COLUMN_END, event.getDateEnd().getValue().getTime());
            values.put(MeetingEntry.COLUMN_LAST_MODIFIED, event.getLastModified().getValue().getTime());
            values.put(MeetingEntry.COLUMN_UID, VEventHelper.getTextProperty(event.getUid()));
            values.put(MeetingEntry.COLUMN_LOCATION, VEventHelper.getTextProperty(event.getLocation()));
            values.put(MeetingEntry.COLUMN_FULL_DAY, VEventHelper.isFullDay(event.getDateStart(), event.getDateEnd()));
            values.put(MeetingEntry.COLUMN_MORE_DAYS, VEventHelper.isMultiDay(event.getDateStart(), event.getDateEnd()));
            values.put(MeetingEntry.COLUMN_NEW, 1);
            operations.add(ContentProviderOperation.newInsert(MeetingEntry.CONTENT_URI).withValues(values).build());
        }

        for (VEvent event : changedLocations) {
            operations.add(
                    ContentProviderOperation
                            .newUpdate(MeetingEntry.CONTENT_URI)
                            .withSelection(MeetingEntry.COLUMN_TITLE + " = ? AND " + MeetingEntry.COLUMN_DESCRIPTION + " = ? AND " + MeetingEntry.COLUMN_BEGIN + " = ? AND " + MeetingEntry.COLUMN_END + " = ?",
                                    new String[]{VEventHelper.getTextProperty(event.getSummary()),
                                            VEventHelper.getTextProperty(event.getDescription()),
                                            String.valueOf(event.getDateStart().getValue().getTime()),
                                            String.valueOf(event.getDateEnd().getValue().getTime())})
                            .withValue(MeetingEntry.COLUMN_UID, VEventHelper.getTextProperty(event.getUid()))
                            .withValue(MeetingEntry.COLUMN_LOCATION, VEventHelper.getTextProperty(event.getLocation()))
                            .withValue(MeetingEntry.COLUMN_CHANGED_LOCATION, 1)
                            .build()
            );
        }

        for (VEvent event : changedDatesList) {
            operations.add(
                    ContentProviderOperation
                            .newUpdate(MeetingEntry.CONTENT_URI)
                            .withSelection(MeetingEntry.COLUMN_TITLE + " = ? AND " + MeetingEntry.COLUMN_LOCATION + " = ? AND " + MeetingEntry.COLUMN_DESCRIPTION + " = ? AND " + MeetingEntry.COLUMN_UID + " = ?",
                                    new String[]{VEventHelper.getTextProperty(event.getSummary()),
                                            VEventHelper.getTextProperty(event.getLocation()),
                                            VEventHelper.getTextProperty(event.getDescription()),
                                            VEventHelper.getTextProperty(event.getUid())})
                            .withValue(MeetingEntry.COLUMN_UID, VEventHelper.getTextProperty(event.getUid()))
                            .withValue(MeetingEntry.COLUMN_BEGIN, event.getDateStart().getValue().getTime())
                            .withValue(MeetingEntry.COLUMN_END, event.getDateEnd().getValue().getTime())
                            .withValue(MeetingEntry.COLUMN_CHANGED_DATE, 1)
                            .build()
            );
        }

        try {
            contentProviderClient.applyBatch(operations);
        } catch (Exception e) {
            Log.e(LOG_NAME + "#runDifferences", String.valueOf(e.getMessage()));
            result.setError(true);
        }

        return result;
    }

    private void getLocalEvents() {
        Log.i(LOG_NAME, "getLocalEvents");
        Cursor cursor = contentResolver.query(
                MeetingEntry.CONTENT_URI,
                MEETING_PROJECTIONS,
                MeetingEntry.COLUMN_DELETED + " is not ?",
                new String[]{"1"}, null);

        if (cursor == null)
            return;

        while (cursor.moveToNext()) {
            VEvent event = new VEvent();
            event.setDateEnd(new Date(cursor.getLong(COLUMN_END)));
            event.setDateStart(new Date(cursor.getLong(COLUMN_BEGIN)));
            event.setDescription(cursor.getString(COLUMN_DESCRIPTION));
            event.setLocation(cursor.getString(COLUMN_LOCATION));
            event.setSummary(cursor.getString(COLUMN_TITLE));
            event.setUid(cursor.getString(COLUMN_UID));
            localVersion.add(event);
        }

        cursor.close();
    }
}
