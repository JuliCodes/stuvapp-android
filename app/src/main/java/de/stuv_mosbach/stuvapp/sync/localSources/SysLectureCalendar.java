package de.stuv_mosbach.stuvapp.sync.localSources;

import android.Manifest;
import android.content.ContentProviderClient;
import android.content.ContentProviderOperation;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.provider.CalendarContract;
import androidx.annotation.RequiresPermission;
import android.util.Log;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;

import biweekly.component.VEvent;
import de.stuv_mosbach.stuvapp.contentProvider.LectureContract;
import de.stuv_mosbach.stuvapp.veventutils.VEventHelper;

/**
 * Abbild für den System Kalender der Vorlesungen.
 *
 * @author Peer Beckmann
 */
public class SysLectureCalendar extends VEventCalendar {
    private static final String LOG_NAME = SysLectureCalendar.class.getName();

    private static final String calendarName = LectureContract.SYSTEM_NAME;
    private static final String[] GET_SYSTEM_ID = new String[]{
            CalendarContract.Calendars._ID
    };
    private static final int COLUMN_ID = 0;
    private static final int COLUMN_TITLE = 1;
    private static final int COLUMN_DESCRIPTION = 2;
    private static final int COLUMN_BEGIN = 3;
    private static final int COLUMN_END = 4;
    private static final int COLUMN_LOCATION = 5;

    private final String[] SYS_PROJECTION = new String[]{
            CalendarContract.Events._ID,
            CalendarContract.Events.TITLE,
            CalendarContract.Events.DESCRIPTION,
            CalendarContract.Events.DTSTART,
            CalendarContract.Events.DTEND,
            CalendarContract.Events.EVENT_LOCATION
    };
    private Context context;

    @Override
    @SuppressWarnings("ResourceType")
    @RequiresPermission(anyOf = {
            Manifest.permission.READ_CALENDAR,
            Manifest.permission.WRITE_CALENDAR})
    public void initialize(Context context) {
        Log.i(LOG_NAME, "initialize");
        super.initialize(context);
        this.context = context;
        getLocalEvents();
    }

    @Override
    public void setRemoteData(LinkedList<biweekly.component.VEvent> serverVersion) {
        Log.i(LOG_NAME, "setRemoteData");
        this.serverVersion = serverVersion;
    }

    @Override
    @SuppressWarnings("ResourceType")
    @RequiresPermission(anyOf = {
            Manifest.permission.READ_CALENDAR,
            Manifest.permission.WRITE_CALENDAR})
    public LocalSourceResult runDifferences(ContentProviderClient contentProviderClient) {
        Log.i(LOG_NAME, "runDifferences");
        ArrayList<ContentProviderOperation> operations = new ArrayList<>();

        ContentValues cv = new ContentValues();
        cv.put(CalendarContract.Events.CALENDAR_ID, getCalendarId());

        for (VEvent event : localVersion) {
            if (VEventHelper.getTextProperty(event.getUid()).equals(""))
                break;

            operations.add(
                    ContentProviderOperation.newDelete(
                            CalendarContract.Events.CONTENT_URI)
                            .withSelection(CalendarContract.Events._ID + "= ?", new String[]{VEventHelper.getTextProperty(event.getUid())})
                            .build()
            );
        }

        for (VEvent event : serverVersion) {
            cv.put(CalendarContract.Events.TITLE, VEventHelper.getTextProperty(event.getSummary()));
            cv.put(CalendarContract.Events.DESCRIPTION, VEventHelper.getTextProperty(event.getDescription()));
            cv.put(CalendarContract.Events.EVENT_LOCATION, VEventHelper.getTextProperty(event.getLocation()));
            cv.put(CalendarContract.Events.EVENT_TIMEZONE, "Europe/Berlin");
            cv.put(CalendarContract.Events.DTSTART, event.getDateStart().getValue().getTime());
            cv.put(CalendarContract.Events.DTEND, event.getDateEnd().getValue().getTime());
            operations.add(ContentProviderOperation.newInsert(CalendarContract.Events.CONTENT_URI).withValues(cv).build());
        }

        for (VEvent event : changedLocations) {
            operations.add(
                    ContentProviderOperation
                            .newUpdate(CalendarContract.Events.CONTENT_URI)
                            .withSelection(CalendarContract.Events._ID + " = ?", new String[]{
                                    VEventHelper.getTextProperty(event.getUid())})
                            .withValue(CalendarContract.Events.EVENT_LOCATION, VEventHelper.getTextProperty(event.getLocation()))
                            .build()
            );
        }

        for (VEvent event : changedDatesList) {
            operations.add(
                    ContentProviderOperation
                            .newUpdate(CalendarContract.Events.CONTENT_URI)
                            .withSelection(CalendarContract.Events._ID + " = ?", new String[]{VEventHelper.getTextProperty(event.getUid())})
                            .withValue(CalendarContract.Events.DTSTART, event.getDateStart().getValue().getTime())
                            .withValue(CalendarContract.Events.DTEND, event.getDateEnd().getValue().getTime())
                            .build()
            );
        }

        try {
            context.getContentResolver().applyBatch(CalendarContract.Events.CONTENT_URI.getAuthority(), operations);
        } catch (Exception e) {
            Log.e(LOG_NAME + "#runDifferences", String.valueOf(e.getMessage()));
        }
        return new LocalSourceResult();
    }

    @SuppressWarnings("ResourceType")
    @RequiresPermission(anyOf = {
            Manifest.permission.READ_CALENDAR,
            Manifest.permission.WRITE_CALENDAR})
    private void getLocalEvents() {
        Log.i(LOG_NAME, "getLocalEvents");
        Cursor cursor = contentResolver.query(
                CalendarContract.Events.CONTENT_URI,
                SYS_PROJECTION,
                CalendarContract.Events.CALENDAR_ID + " = ?",
                new String[]{String.valueOf(getCalendarId())}, null);

        if (cursor == null)
            return;

        while (cursor.moveToNext()) {
            VEvent event = new VEvent();
            event.setDateStart(new Date(cursor.getLong(COLUMN_BEGIN)));
            event.setDateEnd(new Date(cursor.getLong(COLUMN_END)));

            String description = cursor.getString(COLUMN_DESCRIPTION);
            if (description != null && description.length() > 0)
                event.setDescription(description);

            String location = cursor.getString(COLUMN_LOCATION);
            if (location != null && location.length() > 0)
                event.setLocation(location);

            String summary = cursor.getString(COLUMN_TITLE);
            if (summary != null && summary.length() > 0)
                event.setSummary(summary);

            event.setUid(cursor.getString(COLUMN_ID));
            localVersion.add(event);
        }

        cursor.close();
    }

    @SuppressWarnings("ResourceType")
    @RequiresPermission(anyOf = {
            Manifest.permission.READ_CALENDAR,
            Manifest.permission.WRITE_CALENDAR})
    private long getCalendarId() {
        Log.i(LOG_NAME, "getCalendarId");
        long calendarId;

        Cursor cursor = contentResolver.query(
                CalendarContract.Calendars.CONTENT_URI,
                GET_SYSTEM_ID,
                CalendarContract.Calendars.NAME + " = ?",
                new String[]{calendarName},
                null
        );

        if (cursor == null || cursor.getCount() == 0)
            return -1;

        cursor.moveToFirst();
        calendarId = cursor.getLong(0);

        if (calendarId == -1) {
            return -1;
        }

        cursor.close();

        return calendarId;
    }
}
