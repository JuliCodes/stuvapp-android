package de.stuv_mosbach.stuvapp.sync.localSources;

import android.content.ContentProviderClient;
import android.content.ContentProviderOperation;
import android.content.Context;
import android.database.Cursor;
import androidx.annotation.NonNull;
import android.util.Log;

import org.chaosdaten.commons.CursorLoaderBuilder;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;

import de.stuv_mosbach.stuvapp.contentProvider.FeedContract;
import de.stuv_mosbach.stuvapp.parser.FeedElement;

/**
 * The local syncer for the feed elements.
 *
 * @author Peer Beckmann
 */
public class AppFeedSource implements LocalSource<FeedElement>{
    private static final String LOG_NAME = AppFeedSource.class.getName();

    private LinkedList<FeedElement> serverVersion = new LinkedList<>();
    private LinkedList<FeedElement> localVersion = new LinkedList<>();
    private Context context;

    private static final int COLUMN_TITLE = 0;
    private static final int COLUMN_DESCRIPTION = 1;
    private static final int COLUMN_CONTENT = 2;
    private static final int COLUMN_PUBDATE = 3;
    private static final int COLUMN_LINK = 4;

    private static final String[] FEED_PROJECTION = new String[]{
            FeedContract.FeedEntry.COLUMN_TITLE,
            FeedContract.FeedEntry.COLUMN_DESCRIPTION,
            FeedContract.FeedEntry.COLUMN_CONTENT,
            FeedContract.FeedEntry.COLUMN_PUBDATE,
            FeedContract.FeedEntry.COLUMN_LINK
    };

    @Override
    public void initialize(Context context) {
        Log.i(LOG_NAME, "initialize");
        this.context = context;
        getLocalEvents();
    }

    @Override
    public void setRemoteData(@NonNull LinkedList<FeedElement> serverVersion) {
        Log.i(LOG_NAME, "setRemoteData");
        this.serverVersion = serverVersion;
    }

    @Override
    public void calculateDifferences() {
        Log.i(LOG_NAME, "calculateDifferences");
        LinkedList<FeedElement> tmpLocalVersion = new LinkedList<>(this.localVersion);
        LinkedList<FeedElement> tmpServerVersion = new LinkedList<>(this.serverVersion);

        for (FeedElement serverElement : serverVersion) {
            for (FeedElement localElement : localVersion) {
                if (serverElement.equals(localElement)) {
                    tmpLocalVersion.remove(localElement);
                    tmpServerVersion.remove(serverElement);
                }
            }
        }

        this.localVersion = tmpLocalVersion;
        this.serverVersion = tmpServerVersion;
    }

    @Override
    public LocalSourceResult runDifferences(ContentProviderClient contentProviderClient) {
        Log.i(LOG_NAME, "runDifferences");
        ArrayList<ContentProviderOperation> operations = new ArrayList<>();
        LocalSourceResult result = new LocalSourceResult();
        result.setAmountOfDeletes(localVersion.size());
        result.setAmountOfInserts(serverVersion.size());

        //Delete the old elements in the database
        for (FeedElement element : localVersion) {
            operations.add(
                    ContentProviderOperation.newDelete(
                            FeedContract.FeedEntry.CONTENT_URI
                    ).withSelection(FeedContract.FeedEntry.COLUMN_LINK + " = ?", new String[]{element.getLink()}).build()
            );
            //TODO: Obsolet.
            File file = new File(context.getFilesDir() + "/images/" + element.getImageName());
            if (file.delete()) {
                Log.d(LOG_NAME + "#runDifferences", "(Should normally fail) Failed to delete local image");
                result.setError(true);
            }
        }

        //Add the new elements in the database
        for (FeedElement element : serverVersion) {
            operations.add(
                    ContentProviderOperation.newInsert(FeedContract.FeedEntry.CONTENT_URI).
                            withValues(element.mapToDB()).
                            build());
        }

        try {
            contentProviderClient.applyBatch(operations);
        } catch (Exception e) {
            Log.e(LOG_NAME + "#runDifferences", String.valueOf(e.getMessage()));
            result.setError(true);
        }

        return result;
    }

    /**
     * Get all local elements in the local linked list.
     */
    private void getLocalEvents() {
        Log.i(LOG_NAME, "getLocalEvents");
        Cursor cursor = new CursorLoaderBuilder(context)
                .setUri(FeedContract.FeedEntry.CONTENT_URI)
                .setProjection(FEED_PROJECTION)
                .build(context.getContentResolver());

        while (cursor.moveToNext()) {
            FeedElement element = new FeedElement();
            element.setTitle(cursor.getString(COLUMN_TITLE));
            element.setDescription(cursor.getString(COLUMN_DESCRIPTION));
            element.setContent(cursor.getString(COLUMN_CONTENT));
            element.setPubDate(cursor.getLong(COLUMN_PUBDATE));
            element.setLink(cursor.getString(COLUMN_LINK));
            localVersion.add(element);
        }

        cursor.close();
    }
}
