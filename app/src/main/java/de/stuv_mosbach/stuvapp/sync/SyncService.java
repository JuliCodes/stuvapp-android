package de.stuv_mosbach.stuvapp.sync;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

/**
 * Created by Peer Beckmann <peer@pbeckmann.de> on 08.04.15.
 * <p/>
 * SyncService.java
 * <p/>
 * This file register the sync service as a singleton pattern in the system.
 */
public class SyncService extends Service {
    private static final String LOG_NAME = SyncService.class.getName();

    private static final Object syncAdapterLock = new Object();
    private static SyncAdapter syncAdapter = null;

    @Override
    public void onCreate() {
        Log.i(LOG_NAME, "onCreate");
        synchronized (syncAdapterLock) {
            if (syncAdapter == null) {
                syncAdapter = new SyncAdapter(getApplicationContext(), false);
            }
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.i(LOG_NAME, "onBind");
        return syncAdapter.getSyncAdapterBinder();
    }
}
