package de.stuv_mosbach.stuvapp.sync.syncer;

import android.content.ContentProviderClient;
import android.content.Context;
import android.content.SyncResult;
import android.util.Log;

import org.json.JSONArray;
import org.mcsoxford.rss.RSSConfig;
import org.mcsoxford.rss.RSSFeed;
import org.mcsoxford.rss.RSSItem;
import org.mcsoxford.rss.RSSParser;

import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import de.stuv_mosbach.stuvapp.parser.FeedElement;
import de.stuv_mosbach.stuvapp.sync.SyncAdapter;
import de.stuv_mosbach.stuvapp.sync.localSources.LocalSource;
import de.stuv_mosbach.stuvapp.sync.localSources.LocalSourceResult;

/**
 * A syncer for RSS Feed Items.
 *
 * @author Peer Beckmann
 */
public class FeedSyncer implements Syncer {
    private static final String LOG_NAME = FeedSyncer.class.getName();

    private ContentProviderClient contentProviderClient;
    private URL remoteSource;
    private LocalSource<FeedElement> localSource;

    private RSSFeed feed;
    private boolean failed;

    /**
     * This function returns the first image link from the content.
     *
     * @param content The content of the rss entry.
     * @return The url to the first referenced image in the content.
     */
    private static ArrayList<String> getImageUrls(String content) {
        Log.i(LOG_NAME, "getImageUrls");
        ArrayList<String> images = new ArrayList<>();
        int position = 0;
        while (position < content.length()) {
            int startOfUrl = content.indexOf("<img", position);
            if (startOfUrl == -1)
                return images;
            startOfUrl = content.indexOf("src=\"", startOfUrl) + "src=\"".length();
            int endOfUrl = content.indexOf("\"", startOfUrl);
            if (!content.substring(startOfUrl, endOfUrl).contains("piwik.php")) {
                images.add(content.substring(startOfUrl, endOfUrl));
            }
            position = endOfUrl;
        }
        return images;
    }

    @Override
    public void initialize(Context context, ContentProviderClient contentProviderClient, URL source, LocalSource localSource) {
        Log.i(LOG_NAME, "initialize");
        this.contentProviderClient = contentProviderClient;
        this.remoteSource = source;

        this.localSource = localSource;
        this.localSource.initialize(context);
    }

    @Override
    public void prepare() {
        Log.i(LOG_NAME, "prepare");
        //Initialize the RSS Reader
        RSSParser rssParser = new RSSParser(new RSSConfig());
        try {
            feed = rssParser.parse(remoteSource.openStream());
        } catch (Exception e) {
            Log.e(LOG_NAME + "#prepare", String.valueOf(e.getMessage()));
            failed = true;
        }
    }

    @Override
    public void run(SyncResult syncResult) {
        Log.i(LOG_NAME, "run");
        if (failed) {
            syncResult.stats.numIoExceptions += 1;
            return;
        }

        LinkedList<FeedElement> serverVersion = new LinkedList<>();
        List<RSSItem> entryList = feed.getItems();

        for (RSSItem entry : entryList) {
            FeedElement element = new FeedElement();

            element.setTitle(entry.getTitle());
            element.setDescription(optimizeDescription(entry.getDescription()));
            element.setContent(entry.getContent());
            ArrayList<String> images = getImageUrls(entry.getContent());
            JSONArray array = new JSONArray(images);
            element.setImageName(array.toString());
            element.setPubDate(entry.getPubDate().getTime());
            element.setLink(entry.getLink().toString());
            serverVersion.add(element);
        }

        localSource.setRemoteData(serverVersion);
        localSource.calculateDifferences();
        LocalSourceResult result = localSource.runDifferences(contentProviderClient);

        SyncAdapter.convertLocalResult(result, syncResult);
    }

    @Override
    public void notificate(String title, String message) {
        Log.i(LOG_NAME, "notificate");
        //TODO: Should be implemented
    }

    /**
     * This function remove the more link from the given description and appends three dots.
     *
     * @param desc The short text of the rss entry.
     * @return The given description without the more link.
     */
    private static String optimizeDescription(String desc) {
        Log.i(LOG_NAME, "optimizeDescription");
        final int endOfDesc = desc.indexOf("[&#8230;]");
        if (endOfDesc != -1)
            return desc.substring(0, endOfDesc) + "…";

        final int startOfPiwik = desc.indexOf("<img src=\"http://stuv-mosbach.de/piwik");
        if (startOfPiwik != -1)
            return desc.substring(0, startOfPiwik);

        return desc;
    }
}