package de.stuv_mosbach.stuvapp.sync.localSources;

import android.content.ContentResolver;
import android.content.Context;
import android.util.Log;

import java.util.LinkedList;

import biweekly.component.VEvent;
import de.stuv_mosbach.stuvapp.veventutils.VEventHelper;

/**
 * Ein Abbild für alle auf {@link VEvent} basierenden Kalender.
 *
 * @author Peer Beckmann
 */
abstract class VEventCalendar implements LocalSource<VEvent> {
    private static final String LOG_NAME = VEventCalendar.class.getName();


    LinkedList<VEvent> localVersion;
    LinkedList<VEvent> serverVersion;
    LinkedList<VEvent> changedLocations;
    LinkedList<VEvent> changedDatesList;
    ContentResolver contentResolver;

    @Override
    public void initialize(Context context) {
        Log.i(LOG_NAME, "initialize");
        this.localVersion = new LinkedList<>();
        this.serverVersion = new LinkedList<>();
        this.changedLocations = new LinkedList<>();
        this.changedDatesList = new LinkedList<>();
        this.contentResolver = context.getContentResolver();
    }

    @Override
    public void calculateDifferences() {
        Log.i(LOG_NAME, "calculateDifferences");
        final LinkedList<VEvent> tmpEqualsLocal = new LinkedList<>(this.localVersion);
        final LinkedList<VEvent> tmpEqualsServer = new LinkedList<>(this.serverVersion);

        for (VEvent serverEvent : this.serverVersion) {
            if (serverEvent.getSummary() == null && serverEvent.getDescription() == null && serverEvent.getLocation() == null) {
                tmpEqualsServer.remove(serverEvent);
                continue;
            }

            for (VEvent localEvent : this.localVersion) {
                final int comparedByStart = VEventHelper.compareByStart(serverEvent.getDateStart(), localEvent.getDateStart());
                final int comparedByEnd = VEventHelper.compareByEnd(serverEvent.getDateEnd(), localEvent.getDateEnd());
                final int comparedByDesc = VEventHelper.compareTextProperties(serverEvent.getDescription(), localEvent.getDescription());
                final int comparedByLoc = VEventHelper.compareTextProperties(serverEvent.getLocation(), localEvent.getLocation());
                final int comparedBySum = VEventHelper.compareTextProperties(serverEvent.getSummary(), localEvent.getSummary());

                final boolean changedDates = comparedByStart == 0 && comparedByEnd == 0;

                if (changedDates && comparedByDesc == 0 && comparedBySum == 0 && comparedByLoc == 0) {
                    tmpEqualsLocal.remove(localEvent);
                    tmpEqualsServer.remove(serverEvent);
                    break;
                }
            }
        }

        localVersion = tmpEqualsLocal;
        serverVersion = tmpEqualsServer;

        /*final LinkedList<VEvent> tmpLocationLocal = new LinkedList<>(localVersion);
        final LinkedList<VEvent> tmpLocationServer = new LinkedList<>(serverVersion);

//        if (serverVersion.size() > 0 && localVersion.size() > 0)
            for (VEvent serverEvent : serverVersion) {
                for (VEvent localEvent : localVersion) {
                    final int comparedByDesc = VEventHelper.compareTextProperties(serverEvent.getDescription(), localEvent.getDescription());
                    final int comparedBySum = VEventHelper.compareTextProperties(serverEvent.getSummary(), localEvent.getSummary());
                    final int comparedByStart = VEventHelper.compareByStart(serverEvent.getDateStart(), localEvent.getDateStart());
                    final int comparedByEnd = VEventHelper.compareByEnd(serverEvent.getDateEnd(), localEvent.getDateEnd());

                    final boolean changedDates = comparedByStart == 0 && comparedByEnd == 0;

                    if (comparedByDesc == 0 && changedDates && comparedBySum == 0) {
                        changedLocations.add(serverEvent);
                        tmpLocationLocal.remove(localEvent);
                        tmpLocationServer.remove(serverEvent);
                    }
                }
            }

        localVersion = new LinkedList<>(tmpLocationLocal);
        serverVersion = new LinkedList<>(tmpLocationServer);

        final LinkedList<VEvent> tmpDatesLocal = new LinkedList<>(localVersion);
        final LinkedList<VEvent> tmpDatesServer = new LinkedList<>(serverVersion);

//        if (serverVersion.size() > 0 && localVersion.size() > 0)
            for (VEvent serverEvent : serverVersion) {
                for (VEvent localEvent : localVersion) {
                    final int comparedByDesc = VEventHelper.compareTextProperties(serverEvent.getDescription(), localEvent.getDescription());
                    final int comparedByLoc = VEventHelper.compareTextProperties(serverEvent.getLocation(), localEvent.getLocation());
                    final int comparedBySum = VEventHelper.compareTextProperties(serverEvent.getSummary(), localEvent.getSummary());

                    if (comparedByDesc == 0 && comparedByLoc == 0 && comparedBySum == 0) {
                        changedDatesList.add(serverEvent);
                        tmpDatesLocal.remove(localEvent);
                        tmpDatesServer.remove(serverEvent);
                        break;
                    }
                }
            }

        localVersion = new LinkedList<>(tmpDatesLocal);
        serverVersion = new LinkedList<>(tmpDatesServer);*/
    }
}
