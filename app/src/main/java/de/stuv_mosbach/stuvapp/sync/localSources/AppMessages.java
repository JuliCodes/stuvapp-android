package de.stuv_mosbach.stuvapp.sync.localSources;

import android.content.ContentProviderClient;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;

import de.stuv_mosbach.stuvapp.contentProvider.MessageContract;
import de.stuv_mosbach.stuvapp.parser.Message;

/**
 * Ein Abbild der Datenbank in der App für die Nachrichten an die Nutzer.
 *
 * @author Peer Beckmann
 */
public class AppMessages implements LocalSource<Message> {
    private static final String LOG_NAME = AppMessages.class.getName();

    private static final int COLUMN_TITLE = 1;
    private static final int COLUMN_CONTENT = 2;
    private static final int COLUMN_BEGIN = 3;
    private static final int COLUMN_END = 4;
    private static final int COLUMN_PRIORITY = 5;
    private static final int COLUMN_UID = 6;
    private final String[] MESSAGE_PROJECTION = new String[]{
            MessageContract.MessageEntry._ID,
            MessageContract.MessageEntry.COLUMN_TITLE,
            MessageContract.MessageEntry.COLUMN_CONTENT,
            MessageContract.MessageEntry.COLUMN_BEGIN,
            MessageContract.MessageEntry.COLUMN_END,
            MessageContract.MessageEntry.COLUMN_PRIORITY,
            MessageContract.MessageEntry.COLUMN_UID
    };
    private LinkedList<Message> serverVersion, localVersion;
    private ContentResolver contentResolver;

    @Override
    public void initialize(Context context) {
        Log.i(LOG_NAME, "initialize");
        this.contentResolver = context.getContentResolver();
        localVersion = new LinkedList<>();
        serverVersion = new LinkedList<>();
        getLocalMessages();
    }

    @Override
    public void setRemoteData(LinkedList<Message> serverVersion) {
        Log.i(LOG_NAME, "setRemoteData");
        this.serverVersion = serverVersion;
    }

    @Override
    public void calculateDifferences() {
        Log.i(LOG_NAME, "calculateDifferences");
        if (localVersion == null || serverVersion == null) {
            return;
        }

        HashSet<Message> toRemove = new HashSet<>();

        for (Message localMessage : this.localVersion) {
            for (Message serverMessage : this.serverVersion) {
                if (localMessage.equals(serverMessage)) {
                    toRemove.add(localMessage);
                    break;
                }
            }
        }

        localVersion.removeAll(toRemove);
        serverVersion.removeAll(toRemove);
    }

    @Override
    public LocalSourceResult runDifferences(ContentProviderClient contentProviderClient) {
        Log.i(LOG_NAME, "runDifferences");
        ArrayList<ContentProviderOperation> operations = new ArrayList<>();

        for (Message message : localVersion) {
            operations.add(
                    ContentProviderOperation.newDelete(MessageContract.MessageEntry.CONTENT_URI)
                            .withSelection(MessageContract.MessageEntry.COLUMN_UID + " = ?",
                                    new String[]{String.valueOf(message.getUid())})
                            .build());
        }

        for (Message message : serverVersion) {
            ContentValues values = message.mapToDB();
            operations.add(ContentProviderOperation.newInsert(MessageContract.MessageEntry.CONTENT_URI).withValues(values).build());
        }

        try {
            contentProviderClient.applyBatch(operations);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new LocalSourceResult();
    }

    private void getLocalMessages() {
        Log.i(LOG_NAME, "getLocalMessages");
        Cursor cursor = contentResolver.query(
                MessageContract.MessageEntry.CONTENT_URI,
                MESSAGE_PROJECTION,
                null, null, null);

        int cursorCount = cursor != null ? cursor.getCount() : 0;
        for (int n = 0; n < cursorCount; n++) {
            cursor.moveToPosition(n);
            Message message = new Message();
            message.setTitle(cursor.getString(COLUMN_TITLE));
            message.setContent(cursor.getString(COLUMN_CONTENT));
            message.setPriority(cursor.getInt(COLUMN_PRIORITY));
            message.setBegin(cursor.getLong(COLUMN_BEGIN));
            message.setEnd(cursor.getLong(COLUMN_END));
            message.setUid(cursor.getInt(COLUMN_UID));
//            if (message.isValid())
            localVersion.add(message);
        }

        if (cursor != null) {
            cursor.close();
        }
    }
}
