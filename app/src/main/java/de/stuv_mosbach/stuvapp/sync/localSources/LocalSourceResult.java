package de.stuv_mosbach.stuvapp.sync.localSources;

/**
 * Diese Klasse dient als Datenobjekt für die Resultate des Parsens.
 *
 * @author Peer Beckmann
 */
public class LocalSourceResult {
    private int amountOfInserts = 0;
    private int amountOfDeletes = 0;
    private int amountOfUpdates = 0;
    private boolean error = false;

    public int getAmountOfInserts() {
        return amountOfInserts;
    }

    public void setAmountOfInserts(int amountOfInserts) {
        this.amountOfInserts = amountOfInserts;
    }

    public int getAmountOfDeletes() {
        return amountOfDeletes;
    }

    public void setAmountOfDeletes(int amountOfDeletes) {
        this.amountOfDeletes = amountOfDeletes;
    }

    public int getAmountOfUpdates() {
        return amountOfUpdates;
    }

    public void setAmountOfUpdates(int amountOfUpdates) {
        this.amountOfUpdates = amountOfUpdates;
    }

    public boolean isError() {
        return error;
    }

    @SuppressWarnings("SameParameterValue")
    public void setError(boolean error) {
        this.error = error;
    }
}
