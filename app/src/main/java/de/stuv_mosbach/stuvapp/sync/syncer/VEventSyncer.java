package de.stuv_mosbach.stuvapp.sync.syncer;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentProviderClient;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SyncResult;
import android.preference.PreferenceManager;
import androidx.core.app.NotificationCompat;
import android.util.Log;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import biweekly.Biweekly;
import biweekly.ICalendar;
import biweekly.component.VEvent;
import de.stuv_mosbach.stuvapp.MainActivity;
import de.stuv_mosbach.stuvapp.R;
import de.stuv_mosbach.stuvapp.settings.PreferenceKeys;
import de.stuv_mosbach.stuvapp.sync.SyncAdapter;
import de.stuv_mosbach.stuvapp.sync.localSources.AppLectureCalender;
import de.stuv_mosbach.stuvapp.sync.localSources.LocalSource;
import de.stuv_mosbach.stuvapp.sync.localSources.LocalSourceResult;
import de.stuv_mosbach.stuvapp.util.ConfigGlobal;
import de.stuv_mosbach.stuvapp.veventutils.VEventHelper;
import de.stuv_mosbach.stuvapp.veventutils.VEventNotificationHelper;

/**
 * Diese Klasse dient der Synchronisierung der Vorlesungen in den lokalen Kalender. Hierzu bedient
 * es sich der Methoden des {@link Syncer} Interfaces. Als lokale Quelle dient
 * {@link AppLectureCalender}.
 *
 * @author Peer Beckmann
 */
public class VEventSyncer implements Syncer {
    private static final String LOG_NAME = VEventSyncer.class.getName();

    private LocalSourceResult result = new LocalSourceResult();
    private LocalSource<VEvent> localSource;
    private ContentProviderClient contentProviderClient;
    private URL remoteSource;
    private Context context;

    @Override
    public void initialize(Context context, ContentProviderClient contentProviderClient, URL source, LocalSource localSource) {
        Log.i(LOG_NAME, "initialize");
        this.contentProviderClient = contentProviderClient;
        this.remoteSource = source;
        this.context = context;
        //noinspection unchecked
        this.localSource = localSource;

        this.localSource.initialize(context);
    }

    @Override
    public void prepare() {
        Log.i(LOG_NAME, "prepare");
    }

    @Override
    public void run(SyncResult syncResult) {
        Log.i(LOG_NAME, "run");
        try {
            HttpURLConnection connection;

            connection = (HttpURLConnection) remoteSource.openConnection();
            connection.connect();

            ICalendar ical = Biweekly.parse(connection.getInputStream()).first();

            if (ical == null) {
                Log.e(LOG_NAME + "#run", "No ical object found");
                syncResult.stats.numIoExceptions += 1;
                return;
            }

            localSource.setRemoteData(mergeSimilarEvents(ical.getEvents()));
            localSource.calculateDifferences();
            result = localSource.runDifferences(contentProviderClient);
            SyncAdapter.convertLocalResult(result, syncResult);
        } catch (IOException error) {
            Log.d(LOG_NAME + "#run", "IOException" + String.valueOf(error.getMessage()));
            syncResult.stats.numIoExceptions += 1;
            result.setError(true);
        }
    }

    @Override
    public void notificate(String appAction, String message) {
        Log.i(LOG_NAME, "notificate");
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Set<String> defaultNotifications = new HashSet<>(Arrays.asList(context.getResources().getStringArray(R.array.pref_notification_values)));
        Set<String> notificationPreference = sharedPreferences.getStringSet("pref.notifications", defaultNotifications);
        boolean notifyErrors = notificationPreference.contains("UpdateError");
        boolean notifyLectures = notificationPreference.contains("Lectures");
        boolean notifyMeetings = notificationPreference.contains("Meetings");
        boolean notifyChanged = ! ( (notifyMeetings && SyncAdapter.OPEN_MEETINGS.equals(appAction)) || (notifyLectures && SyncAdapter.OPEN_LECTURES.equals(appAction)) );

        // When nothing changed and no error occurred return instant
        if (!result.isError() &&
                result.getAmountOfInserts() == 0 && result.getAmountOfUpdates() == 0 && result.getAmountOfDeletes() == 0)
            return;

        // The synchronisation failed
        if (result.isError() && notifyErrors) {
            Intent intent = new Intent(context, MainActivity.class).setAction(ConfigGlobal.AUTHORITY);
            PendingIntent pendingIntent = PendingIntent.getActivity(
                            context,
                            0,
                            intent,
                            PendingIntent.FLAG_UPDATE_CURRENT);

            throwNotification(prepareNotification()
                    .setContentTitle(context.getResources().getString(R.string.notification_error))
                    .setContentText(context.getResources().getString(R.string.notification_error_msg) + message)
                    .setContentIntent(pendingIntent));
            return;
        } else if (result.isError()) {
            return;
        }

        if (notifyChanged)
            return;

        // Something changed
        VEventNotificationHelper helper = new VEventNotificationHelper(context.getResources());
        helper.setNewEvents(result.getAmountOfInserts());
        helper.setChangedEvents(result.getAmountOfUpdates());
        helper.setDeletedEvents(result.getAmountOfDeletes());
        NotificationCompat.Builder builder = prepareNotification();

        Intent intent = new Intent(context, MainActivity.class).setAction(appAction);
        PendingIntent pendingIntent = PendingIntent.getActivity(
                context,
                0,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        if (helper.isBigStyle()) {
            NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
            bigTextStyle.bigText(helper.getBigContent());
            builder.setStyle(bigTextStyle);
        }

        throwNotification(builder
                .setContentTitle(context.getResources().getString(R.string.notification_list).replace("%s", message))
                .setContentText(helper.getLittleContent())
                .setContentIntent(pendingIntent));
    }

    private NotificationCompat.Builder prepareNotification() {
        Log.i(LOG_NAME, "prepareNotification");
        return new NotificationCompat.Builder(context, ConfigGlobal.NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_school_white_24dp)
                .setAutoCancel(true);
    }

    private void throwNotification(NotificationCompat.Builder builder) {
        Log.i(LOG_NAME, "throwNotification");
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        final int notificationId = sharedPreferences.getInt(PreferenceKeys.NOTIFICATION_ID, 0);

        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(notificationId, builder.build());

        sharedPreferences.edit().putInt(PreferenceKeys.NOTIFICATION_ID, notificationId + 1).apply();
    }

    private LinkedList<VEvent> mergeSimilarEvents(List<VEvent> events) {
        Log.i(LOG_NAME, "mergeSimilarEvents");
        LinkedList<VEvent> result = new LinkedList<>();

        Collections.sort(events, (event1, event2) -> {
            if (event1 == null && event2 == null)
                return 0;
            if (event1 == null)
                return -1;
            if (event2 == null)
                return 1;

            final int comparedByStart = VEventHelper.compareByStart(event1.getDateStart(), event2.getDateStart());

            if (comparedByStart != 0)
                return comparedByStart;

            final int comparedByDuration = VEventHelper.compareByDuration(
                    event1.getDateStart(), event1.getDateEnd(),
                    event2.getDateStart(), event2.getDateEnd());

            if (comparedByDuration != 0)
                return comparedByDuration;

            final int comparedByLocation = VEventHelper.compareTextProperties(event1.getLocation(), event2.getLocation());

            if (comparedByLocation != 0)
                return comparedByLocation;

            return VEventHelper.compareTextProperties(event1.getDescription(), event2.getDescription());
        });

        for (int i = 0; i < events.size(); i++) {
            if (i == 0) {
                result.add(events.get(0));
                continue;
            }
            VEvent actualEvent = result.get(result.size() - 1);
            VEvent nextEvent = events.get(i);

            if (VEventHelper.compareTextProperties(actualEvent.getUid(), nextEvent.getUid()) == 0)
                if (VEventHelper.compareTextProperties(actualEvent.getDescription(), nextEvent.getDescription()) == 0) {
                    actualEvent.setLocation(VEventHelper.concatTextProperties(actualEvent.getLocation(), nextEvent.getLocation(), ", "));
                    continue;
                }
            result.add(nextEvent);
        }

        return result;
    }
}
