package de.stuv_mosbach.stuvapp.sync.localSources;

import android.content.ContentProviderClient;
import android.content.Context;

import java.util.LinkedList;

/**
 * Dieses Interface dient dazu, dass alle lokalen Quellen dieselben Funktionalitäten als Zugriff
 * anbietet. Es benötigt bei der Einbindung den Typ des Objekts, für den es die lokale Quelle
 * darstellt.
 *
 * @author Peer Beckmann
 */
public interface LocalSource<T> {

    /**
     * Statt hier jede nötige Variable zu übergeben, sollen diese in dieser Funktion aus dem Context
     * generiert werden.
     *
     * @param context Der Context der Anwendung.
     */
    void initialize(Context context);

    /**
     * Dies dient als Möglichkeit um die Daten aus der externen Quelle an den Kalender übergeben.
     *
     * @param serverVersion Eine LinkedList mit den Elementen aus der externen Quelle.
     */
    void setRemoteData(LinkedList<T> serverVersion);

    /**
     * In dieser Methode soll in den Implementierungen die Berechnung zwischen der entfernten Quelle
     * und der lokalen Quelle erfolgen.
     */
    void calculateDifferences();

    /**
     * Erst hier sollen die Veränderungen auf der Datenbank ausgeführt werden.
     *
     * @param contentProviderClient Der Client auf dem die Änderungen ausgeführt werden.
     */
    LocalSourceResult runDifferences(ContentProviderClient contentProviderClient);
}
