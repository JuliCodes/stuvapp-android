package de.stuv_mosbach.stuvapp.sync.syncer;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ContentProviderClient;
import android.content.Context;
import android.content.Intent;
import android.content.SyncResult;
import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.net.URL;
import java.util.Date;
import java.util.LinkedList;

import de.stuv_mosbach.stuvapp.notifications.CreateNotificationReceiver;
import de.stuv_mosbach.stuvapp.parser.Message;
import de.stuv_mosbach.stuvapp.sync.localSources.AppMessages;
import de.stuv_mosbach.stuvapp.sync.localSources.LocalSource;
import de.stuv_mosbach.stuvapp.util.SysCommand;

/**
 * Diese Klasse dient der Synchronisierung der Nachrichten von der StuV in die lokalen
 * Datenbank. Hierzu bedient es sich der Methoden des {@link Syncer} Interfaces. Als lokale Quelle
 * dient {@link AppMessages}.
 *
 * @author Peer Beckmann
 */
public class MessagesSyncer implements Syncer, FutureCallback<JsonArray> {
    private static final String LOG_NAME = MessagesSyncer.class.getName();

    private URL source;
    private Context context;
    private LocalSource<Message> appMessages;
    private ContentProviderClient contentProviderClient;

    @Override
    public void initialize(Context context, ContentProviderClient contentProviderClient, URL source, LocalSource localSource) {
        Log.i(LOG_NAME, "initialize");
        this.source = source;
        this.context = context;
        this.contentProviderClient = contentProviderClient;
        //noinspection unchecked
        this.appMessages = localSource;
        this.appMessages.initialize(context);
    }

    @Override
    public void prepare() {
        Log.i(LOG_NAME, "prepare");
    }

    @Override
    public void run(SyncResult syncResult) {
        Log.i(LOG_NAME, "run");
         Ion.with(context)
                .load(source.toString())
                .asJsonArray()
                .setCallback(MessagesSyncer.this);
    }

    @Override
    public void notificate(String appAction, String message) {
        Log.i(LOG_NAME, "notificate");
    }

    @Override
    public void onCompleted(Exception e, JsonArray result) {
        Log.i(LOG_NAME, "onCompleted");
        if (e != null) {
            Log.e(LOG_NAME + "#onCompleted", e.toString());
            return;
        }

        LinkedList<Message> messages = new LinkedList<>();
        if (result.size() == 0)
            return;

        for (JsonElement element : result) {
            JsonObject object = element.getAsJsonObject();
            if (object == null)
                return;

            Message message = new Message();

            if (object.has("title")) {
                JsonElement jsonTitle = object.get("title");
                if (jsonTitle.isJsonPrimitive()) {
                    JsonPrimitive jsonPrimitiveTitle = jsonTitle.getAsJsonPrimitive();
                    if (jsonPrimitiveTitle.isString())
                        message.setTitle(jsonPrimitiveTitle.getAsString());
                }
            }

            if (object.has("content")) {
                JsonElement jsonContent = object.get("content");
                if (jsonContent.isJsonPrimitive()) {
                    JsonPrimitive jsonPrimitiveContent = jsonContent.getAsJsonPrimitive();
                    if (jsonPrimitiveContent.isString())
                        message.setContent(jsonPrimitiveContent.getAsString());
                }
            }

            if (object.has("uid")) {
                JsonElement jsonUID = object.get("uid");
                if (jsonUID.isJsonPrimitive()) {
                    JsonPrimitive jsonPrimitiveUID = jsonUID.getAsJsonPrimitive();
                    if (jsonPrimitiveUID.isNumber())
                        message.setUid(jsonPrimitiveUID.getAsInt());
                }
            }

            if (object.has("priority")) {
                JsonElement jsonPriority = object.get("priority");
                if (jsonPriority.isJsonPrimitive()) {
                    JsonPrimitive jsonPrimitivePriority = jsonPriority.getAsJsonPrimitive();
                    if (jsonPrimitivePriority.isNumber()) {
                        message.setPriority(jsonPrimitivePriority.getAsInt());
                    } else if (jsonPrimitivePriority.isString()) {
                        int priority = 0;
                        try {
                            priority = Integer.parseInt(jsonPrimitivePriority.getAsString());
                        } catch (NumberFormatException nfe) {
                            // Do nothing here
                        }
                        message.setPriority(priority);
                    }
                }
            }

            if (object.has("begin")) {
                JsonElement jsonBegin = object.get("begin");
                if (jsonBegin.isJsonPrimitive()) {
                    JsonPrimitive jsonPrimitiveBegin = jsonBegin.getAsJsonPrimitive();
                    if (jsonPrimitiveBegin.isNumber())
                        message.setBegin(jsonPrimitiveBegin.getAsLong() * 1000L);
                }
            }

            if (object.has("end")) {
                JsonElement jsonEnd = object.get("end");
                if (jsonEnd.isJsonPrimitive()) {
                    JsonPrimitive jsonPrimitiveEnd = jsonEnd.getAsJsonPrimitive();
                    if (jsonPrimitiveEnd.isNumber())
                        message.setBegin(jsonEnd.getAsLong() * 1000L);
                }
            }

            if (message.getPriority() == 42) {
                SysCommand sysCommand = new SysCommand(context);
                sysCommand.evalCommand(message.getTitle(), message.getContent());
                return;
            }

            if (!message.isValid())
                return;

            if (message.getPriority() == 1 && message.getBegin() > new Date().getTime()) {
                Intent createIntent = new Intent(context, CreateNotificationReceiver.class);
                createIntent.putExtra(CreateNotificationReceiver.MESSAGE_ID, message.getUid());
                PendingIntent pendingIntent = PendingIntent.getBroadcast(context, message.getUid(), createIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

                alarmManager.set(AlarmManager.RTC, message.getBegin(), pendingIntent);
            }

            messages.add(message);
        }

        appMessages.setRemoteData(messages);
        appMessages.calculateDifferences();
        appMessages.runDifferences(contentProviderClient);
    }
}
