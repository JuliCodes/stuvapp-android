package de.stuv_mosbach.stuvapp.sync;

import android.Manifest;
import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SyncResult;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.core.app.ActivityCompat;
import android.util.Log;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedList;

import de.stuv_mosbach.stuvapp.R;
import de.stuv_mosbach.stuvapp.settings.PreferenceKeys;
import de.stuv_mosbach.stuvapp.sync.localSources.AppFeedSource;
import de.stuv_mosbach.stuvapp.sync.localSources.AppLectureCalender;
import de.stuv_mosbach.stuvapp.sync.localSources.AppMeetingsCalendar;
import de.stuv_mosbach.stuvapp.sync.localSources.AppMessages;
import de.stuv_mosbach.stuvapp.sync.localSources.LocalSource;
import de.stuv_mosbach.stuvapp.sync.localSources.LocalSourceResult;
import de.stuv_mosbach.stuvapp.sync.localSources.SysLectureCalendar;
import de.stuv_mosbach.stuvapp.sync.localSources.SysMeetingsCalendar;
import de.stuv_mosbach.stuvapp.sync.syncer.FeedSyncer;
import de.stuv_mosbach.stuvapp.sync.syncer.MessagesSyncer;
import de.stuv_mosbach.stuvapp.sync.syncer.Syncer;
import de.stuv_mosbach.stuvapp.sync.syncer.VEventSyncer;
import de.stuv_mosbach.stuvapp.util.ConfigGlobal;

/**
 * Der SyncAdapter. Konzept ist hierbei, dass es für jede Art von Remote-Quelle einen Syncer gibt,
 * der auf den entsprechenden Typ hin optimiert ist. Für lokalen Quellen gibt es einen ähnlichen
 * Mechanismus. Hierbei soll jede lokale Quelle eine Klasse haben.
 *
 * @author Peer Beckmann
 */
public class SyncAdapter extends AbstractThreadedSyncAdapter {
    private static final String LOG_NAME = SyncAdapter.class.getName();
    public static final String OPEN_LECTURES = ConfigGlobal.AUTHORITY + ".OPEN_LECTURES";
    public static final String OPEN_MEETINGS = ConfigGlobal.AUTHORITY + ".OPEN_MEETINGS";

    private ContentProviderClient provider;
    private SyncResult syncResult;

    @SuppressWarnings("SameParameterValue")
    public SyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
    }

    /**
     * Diese Funktion dient dem Download von einer entfernten Quelle zu einer lokalen Datei.
     * Dabei öffnet sie zwei Streams und liest immer 4096 Bytes von der externen Quelle und schreibt
     * diese in den lokalen Stream.
     * <p/>
     * Die entfernte Quelle wird dabei im Systemlog hinterlegt.
     *
     * @param source      Die URL von der die Datei heruntergeladen werden soll.
     * @param destination Die Quelle wo die Datei gespeichert werden soll. Dies muss bereits
     *                    entsprechende Prefixe enthalten.
     * @throws IOException Sollte beim Zugriff auf einer der Streams ein Fehler auftreten, so wird
     *                     dieser Fehler geworfen.
     */
    public static void downloadFileToDestination(URL source, String destination) throws IOException {
        Log.i(LOG_NAME, "downloadFileToDestination");
        InputStream inputStream;
        OutputStream outputStream;
        HttpURLConnection connection;
        Log.d("Url", source.toString());

        connection = (HttpURLConnection) source.openConnection();
        connection.connect();

        inputStream = connection.getInputStream();
        outputStream = new FileOutputStream(destination);

        byte data[] = new byte[4096];
        int count;
        while ((count = inputStream.read(data)) != -1) {
            outputStream.write(data, 0, count);
        }

        outputStream.flush();
        outputStream.close();
    }

    /**
     * Konvertiere das gegebene {@link LocalSourceResult}, welches man von den LocalSource zurück-
     * bekommt zu dem mitgegeben {@link SyncResult}.
     *
     * @param result     Ein {@link LocalSourceResult}.
     * @param syncResult Ein {@link SyncResult}
     */
    public static void convertLocalResult(LocalSourceResult result, SyncResult syncResult) {
        Log.i(LOG_NAME, "convertLocalResult");
        syncResult.stats.numDeletes += result.getAmountOfDeletes();
        syncResult.stats.numInserts += result.getAmountOfInserts();
        syncResult.stats.numUpdates += result.getAmountOfUpdates();
        syncResult.stats.numIoExceptions += (result.isError()) ? 1 : 0;
    }

    /**
     * Diese Funktion führt die eigentliche Synchronisation aus. Zuerst wird hierbei überprüft, ob
     * eine Internetverbindung besteht, um nicht auf die Timeouts von den HTTP-Anfragen warten zu
     * müssen. Anschließend übergeben wir an die sync() Funktionen.
     * die entsprechenden Prameter, damit diese für uns die Synchronisation auführt.
     *
     * @param account    Vom System der Account auf dem die Synchronisation ausgeführt werden soll.
     * @param extras     Ein Bundle mit den übergebenen Extras.
     * @param authority  Die Authority auf der die Synchronisation stattfinden soll.
     * @param provider   Der Provider für die lokalen Daten.
     * @param syncResult Das Resultat für das System auf dessen Basis es dann entscheidet, ob die
     *                   Synchronisation erfolgreich war, oder eben nicht.
     */
    @Override
    public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {
        Log.i(LOG_NAME, "onPerformSync");
        this.provider = provider;
        this.syncResult = syncResult;

        ConnectivityManager connectivityManager = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        Boolean isOnline = networkInfo != null && networkInfo.isConnected();
        if (!isOnline) {
            syncResult.stats.numIoExceptions += 1;
            Log.d(LOG_NAME + "#onPerformSync", "No network connection found");
            return;
        }

        LinkedList<AsyncTask> futures = new LinkedList<>();

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());

        Boolean showLectures = sharedPreferences.getBoolean(PreferenceKeys.SHOW_LECTURES, false);
        Boolean showMeetings = sharedPreferences.getBoolean(PreferenceKeys.SHOW_MEETINGS, false);
        Boolean showNews = sharedPreferences.getBoolean(PreferenceKeys.SHOW_NEWS, false);

        Boolean sync_in_system = sharedPreferences.getBoolean(PreferenceKeys.SYNC_IN_SYSTEM, false);

        sync_in_system = sync_in_system &&
                (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.READ_CALENDAR) == PackageManager.PERMISSION_GRANTED) &&
                (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_CALENDAR) == PackageManager.PERMISSION_GRANTED);

        if (showLectures && sharedPreferences.getString(PreferenceKeys.CLASS, "").length() > 0) {
            futures.add(sync(new VEventSyncer(), new AppLectureCalender(), sharedPreferences.getString(PreferenceKeys.CLASS, ""), true, OPEN_LECTURES, getContext().getResources().getString(R.string.lectures_title)));
        }

        if (showMeetings) {
            futures.add(sync(new VEventSyncer(), new AppMeetingsCalendar(), sharedPreferences.getString(PreferenceKeys.MEETINGS_URL, ""), true, OPEN_MEETINGS, getContext().getResources().getString(R.string.meetings_title)));
        }

        if (sync_in_system) {
            if (showLectures && sharedPreferences.getString(PreferenceKeys.CLASS, "").length() > 0)
                futures.add(sync(new VEventSyncer(), new SysLectureCalendar(), sharedPreferences.getString(PreferenceKeys.CLASS, "")));

            if (showMeetings)
                futures.add(sync(new VEventSyncer(), new SysMeetingsCalendar(), sharedPreferences.getString(PreferenceKeys.MEETINGS_URL, "")));
        }

        String messagesUrl = sharedPreferences.getString(PreferenceKeys.MESSAGES_URL, "");
        if (sharedPreferences.getString(PreferenceKeys.CLASS_VALUE, "").length() > 0)
            messagesUrl += "?course=" + sharedPreferences.getString(PreferenceKeys.CLASS_VALUE, "");
//            messagesUrl += "?course=beta";
        futures.add(sync(new MessagesSyncer(), new AppMessages(), messagesUrl));

        if (showNews) {
            futures.add(sync(new FeedSyncer(), new AppFeedSource(), sharedPreferences.getString(PreferenceKeys.FEED_URL, "")));
        }

        for (int i = 0; i < futures.size(); i++) {
            try {
                futures.get(i).get();
            } catch (Exception e) {
                Log.e(LOG_NAME + "#onPerformSync", "Error getting the result of Syncer " + i + String.valueOf(e.getMessage()));
            }
        }

        if (!syncResult.hasError()) {
            sharedPreferences.edit().putLong(PreferenceKeys.LAST_SUCCESS_SYNC, System.currentTimeMillis()).apply();
        }

        Log.d(LOG_NAME + "#onPerformSync", syncResult.stats.toString());
    }

    private AsyncTask<SyncOptions, Object, Void> sync(Syncer syncer, LocalSource localSource, String remoteSource) {
        Log.i(LOG_NAME, "sync");
        return sync(syncer, localSource, remoteSource, false, "", "");
    }

    private AsyncTask<SyncOptions, Object, Void> sync(Syncer syncer, LocalSource localSource, String remoteSource, Boolean notify, String appAction, String message) {
        Log.i(LOG_NAME, "sync");

        SyncOptions options = new SyncOptions(
            syncer,
            localSource,
            syncResult,
            getContext(),
            provider,
            remoteSource,
            appAction,
            message,
            notify);

        return new SyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, options);
    }

    static class SyncTask extends AsyncTask<SyncOptions, Object, Void> {
        @Override
        protected Void doInBackground(SyncOptions... syncOptions) {
            SyncOptions options = syncOptions[0];
            try {
                URL url = new URL(options.remoteSource);
                options.syncer.initialize(options.context, options.providerClient, url, options.localSource);
                options.syncer.prepare();
                options.syncer.run(options.syncResult);
                if (options.notify)
                    options.syncer.notificate(options.appAction, options.message);
            } catch (MalformedURLException e) {
                Log.i(LOG_NAME + "#sync", String.valueOf(e.getMessage()));
            }
            return null;
        }
    }

    class SyncOptions {
        Syncer syncer;
        LocalSource localSource;
        SyncResult syncResult;
        Context context;
        ContentProviderClient providerClient;
        String remoteSource;
        String appAction;
        String message;
        Boolean notify;

        SyncOptions(Syncer syncer, LocalSource localSource, SyncResult syncResult, Context context, ContentProviderClient providerClient, String remoteSource, String appAction, String message, Boolean notify) {
            this.syncer = syncer;
            this.localSource = localSource;
            this.syncResult = syncResult;
            this.context = context;
            this.providerClient = providerClient;
            this.remoteSource = remoteSource;
            this.appAction = appAction;
            this.message = message;
            this.notify = notify;
        }
    }
}
