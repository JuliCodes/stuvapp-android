package de.stuv_mosbach.stuvapp.veventutils;

import android.content.res.Resources;

import de.stuv_mosbach.stuvapp.R;

/**
 * Concat different strings and create the needed contents for the notification.
 * @author Peer Beckmann
 */

public class VEventNotificationHelper {
    private final Resources resources;
    private int newEvents;
    private int changedEvents;
    private int deletedEvents;
    private int changes = 0;

    public VEventNotificationHelper(Resources resources) {
        this.resources = resources;
    }

    public void setNewEvents(int newEvents) {
        this.newEvents = newEvents;
        changes = newEvents + changedEvents + deletedEvents;
    }

    public void setChangedEvents(int changedEvents) {
        this.changedEvents = changedEvents;
        changes = newEvents + changedEvents + deletedEvents;
    }

    public void setDeletedEvents(int deletedEvents) {
        this.deletedEvents = deletedEvents;
        changes = newEvents + changedEvents + deletedEvents;
    }

    @SuppressWarnings("SimplifiableIfStatement") // Do not simplify... Would be annoying code.
    public boolean isBigStyle() {
        if (newEvents == 0 && changedEvents == 0 && deletedEvents == 0)
            return false;

        if (newEvents != 0 && changedEvents == 0 && deletedEvents == 0)
            return false;

        if (newEvents == 0 && changedEvents != 0 && deletedEvents ==0)
            return false;

        return !(newEvents == 0 && changedEvents == 0);

    }

    public String getLittleContent() {
        if (isBigStyle())
            return resources.getString(R.string.notification_changes).replace("%s", String.valueOf(changes));

        if (newEvents != 0) {
            if (newEvents == 1)
                return resources.getString(R.string.notification_new_event);
            return resources.getString(R.string.notification_new_events).replace("%s", String.valueOf(newEvents));
        }

        if (changedEvents != 0) {
            if (changedEvents == 1)
                return resources.getString(R.string.notification_changed_event);
            return resources.getString(R.string.notification_changed_events).replace("%s", String.valueOf(changedEvents));
        }

        if (deletedEvents != 0) {
            if (deletedEvents == 1)
                return resources.getString(R.string.notification_deleted_event);
            return resources.getString(R.string.notification_deleted_events).replace("%s", String.valueOf(deletedEvents));
        }

        return "";
    }

    public String getBigContent() {
        StringBuilder bigContent = new StringBuilder();

        if (newEvents != 0)
            bigContent.append(resources.getString(R.string.prefix_new)).append(newEvents);

        if (changedEvents != 0) {
            if (bigContent.length() > 0)
                bigContent.append("\n");
            bigContent.append(resources.getString(R.string.prefix_updated)).append(changedEvents);
        }

        if (deletedEvents != 0) {
            if (bigContent.length() > 0)
                bigContent.append("\n");
            bigContent.append(resources.getString(R.string.prefix_deleted)).append(deletedEvents);
        }

        return bigContent.toString();
    }
}
