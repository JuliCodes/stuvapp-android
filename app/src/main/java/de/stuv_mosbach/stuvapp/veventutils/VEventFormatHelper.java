package de.stuv_mosbach.stuvapp.veventutils;

import android.content.SharedPreferences;
import android.content.res.Resources;
import androidx.annotation.NonNull;

import java.util.Date;

import de.stuv_mosbach.stuvapp.R;
import de.stuv_mosbach.stuvapp.settings.PreferenceKeys;
import de.stuv_mosbach.stuvapp.util.ConfigGlobal;

/**
 * This class helps on formatting the different elements of a VEvent.
 */

public class VEventFormatHelper {
    // VEvent Date
    final private String summary;
    final private String description;
    final private String location;
    final private String origin;

    final private Date start;
    final private Date end;

    // Useful information about the event. Self-calculated
    final private boolean firstAtDay;
    final private boolean allDay;
    final private boolean multiDay;

    // Custom display Flags
    final private boolean FLAG_NEW;
    final private boolean FLAG_DELETED;
    final private boolean FLAG_CHANGED_LOCATION;
    final private boolean FLAG_CHANGED_DATE;

    final private SharedPreferences sharedPreferences;
    final private Resources resources;

    private VEventFormatHelper(Builder builder) {
        if (builder.sharedPreferences == null)
            throw new RuntimeException("No SharedPreferences given.");

        if (builder.resources == null)
            throw new RuntimeException("No resources object given.");

        summary                     = builder.summary;
        description                 = builder.description;
        location                    = builder.location;
        origin                      = builder.origin;

        start                       = builder.start;
        end                         = builder.end;

        firstAtDay                  = builder.firstAtDay;
        allDay                      = builder.allDay;
        multiDay                    = builder.multiDay;

        FLAG_NEW                    = builder.FLAG_NEW;
        FLAG_DELETED                = builder.FLAG_DELETED;
        FLAG_CHANGED_LOCATION       = builder.FLAG_CHANGED_LOCATION;
        FLAG_CHANGED_DATE           = builder.FLAG_CHANGED_DATE;

        sharedPreferences           = builder.sharedPreferences;
        resources                   = builder.resources;
    }

    public String getHeader() {
        return ConfigGlobal.dayMonthYear.format(start);
    }

    /**
     * Build the summary for the actual VEvent.
     * @return The final summary.
     */
    public String getSummary() {
        if (summary.isEmpty())
            return summary;

        final boolean FLAG_SHOW_LECTURE_IN_TITLE =
                sharedPreferences.getBoolean(PreferenceKeys.SHOW_LECTURE_IN_TITLE, false);

        int position_of_lecture = summary.indexOf(" Vorlesung");
        if (position_of_lecture == -1)
            position_of_lecture = summary.length();

        StringBuilder builder = new StringBuilder();

        if (FLAG_DELETED) {
            builder.append(resources.getString(R.string.prefix_deleted));
        } else if (FLAG_NEW) {
            builder.append(resources.getString(R.string.prefix_new));
        }

        if (! FLAG_SHOW_LECTURE_IN_TITLE) {
            builder.append(summary);
        } else {
            builder.append(summary.substring(0, position_of_lecture));
        }

        return builder.toString();
    }

    public boolean isDescription() {
        return !description.isEmpty();
    }

    public String getDescription() {
        return description;
    }

    public boolean isLocation() {
        return !location.isEmpty();
    }

    public String getLocation() {
        return location;
    }

    public String getOrigin() {
        return origin;
    }

    public Date getStart() {
        return start;
    }

    public Date getEnd() {
        return end;
    }

    public boolean isFirstAtDay() {
        return firstAtDay;
    }

    public boolean isAllDay() {
        return allDay;
    }

    public boolean isMultiDay() {
        return multiDay;
    }

    public boolean isFLAG_NEW() {
        return FLAG_NEW;
    }

    public boolean isFLAG_DELETED() {
        return FLAG_DELETED;
    }

    public boolean isFLAG_CHANGED_LOCATION() {
        return FLAG_CHANGED_LOCATION;
    }

    public boolean isFLAG_CHANGED_DATE() {
        return FLAG_CHANGED_DATE;
    }

    /**
     * Format the dates and return a simple string.
     * @return The times for the event as a String.
     */
    public String getFormattedDates() {
        StringBuilder builder = new StringBuilder();

        if (allDay && multiDay) {
            builder.append(resources.getString(R.string.prefix_finish));
            builder.append(ConfigGlobal.dayMonth.format(end));
        } else if (multiDay) {
            builder.append(ConfigGlobal.hourMinute.format(start));
            builder.append(resources.getString(R.string.minus_delimiter));
            builder.append(ConfigGlobal.dayMonthHourMinute.format(end));
        } else if (allDay) {
            builder.append(resources.getString(R.string.fullday));
        } else {
            builder.append(ConfigGlobal.hourMinute.format(start));
            builder.append(resources.getString(R.string.minus_delimiter));
            builder.append(ConfigGlobal.hourMinute.format(end));
        }

        return builder.toString();
    }

    // Return true if the event is now running. Exactly at second level.
    public boolean isRunning() {
        long now = new Date().getTime()/1000;
        return start.getTime()/1000 <= now && end.getTime()/1000 >= now;
    }

    @SuppressWarnings("ConstantConditions")
    public static class Builder {
        // VEvent Date
        private String summary                      = "";
        private String description                  = "";
        private String location                     = "";
        private String origin                       = "";

        private Date start                          = new Date();
        private Date end                            = new Date();

        // Useful information about the event. Self-calculated
        private boolean firstAtDay                  = false;
        private boolean allDay                      = false;
        private boolean multiDay                    = false;

        // Custom display options
        private boolean FLAG_NEW                    = false;
        private boolean FLAG_DELETED                = false;
        private boolean FLAG_CHANGED_LOCATION       = false;
        private boolean FLAG_CHANGED_DATE           = false;

        private SharedPreferences sharedPreferences = null;
        private Resources resources                 = null;

        public VEventFormatHelper build() {
            return new VEventFormatHelper(this);
        }

        public Builder setSummary(@NonNull String summary) {
            if (summary != null)    // DO NOT REMOVE THIS LINE!!
                this.summary = summary;
            return this;
        }

        public Builder setDescription(@NonNull String description) {
            if (description != null)    // DO NOT REMOVE THIS LINE!!
                this.description = description;
            return this;
        }

        public Builder setLocation(@NonNull String location) {
            if (location != null)    // DO NOT REMOVE THIS LINE!!
                this.location = location;
            return this;
        }

        public Builder setOrigin(@NonNull String origin) {
            if (origin != null)    // DO NOT REMOVE THIS LINE!!
                this.origin = origin;
            return this;
        }

        public Builder setStart(long startTime) {
            start = new Date();
            start.setTime(startTime);
            return this;
        }

        public Builder setEnd(long endTime) {
            end = new Date();
            end.setTime(endTime);
            return this;
        }

        public Builder setFirstAtDay(boolean firstAtDay) {
            this.firstAtDay = firstAtDay;
            return this;
        }

        public Builder setAllDay(boolean allDay) {
            this.allDay = allDay;
            return this;
        }

        public Builder setMultiDay(boolean multiDay) {
            this.multiDay = multiDay;
            return this;
        }

        public Builder setFLAG_NEW(boolean FLAG_NEW) {
            this.FLAG_NEW = FLAG_NEW;
            return this;
        }

        public Builder setFLAG_DELETED(boolean FLAG_DELETED) {
            this.FLAG_DELETED = FLAG_DELETED;
            return this;
        }

        public Builder setFLAG_CHANGED_LOCATION(boolean FLAG_CHANGED_LOCATION) {
            this.FLAG_CHANGED_LOCATION = FLAG_CHANGED_LOCATION;
            return this;
        }

        public Builder setFLAG_CHANGED_DATE(boolean FLAG_CHANGED_DATE) {
            this.FLAG_CHANGED_DATE = FLAG_CHANGED_DATE;
            return this;
        }

        public Builder setSharedPreferences(@NonNull SharedPreferences sharedPreferences) {
            this.sharedPreferences = sharedPreferences;
            return this;
        }

        public Builder setResources(@NonNull Resources resources) {
            this.resources = resources;
            return this;
        }
    }

}
