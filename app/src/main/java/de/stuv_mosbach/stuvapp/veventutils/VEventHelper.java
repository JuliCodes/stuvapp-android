package de.stuv_mosbach.stuvapp.veventutils;

import androidx.annotation.Nullable;

import org.chaosdaten.commons.DateHelper;

import java.util.Date;
import java.util.Locale;

import biweekly.property.DateEnd;
import biweekly.property.DateStart;
import biweekly.property.TextProperty;
import biweekly.util.ICalDate;

final public class VEventHelper {

    /**
     * Return the duration of an VEvent or 0.
     *
     * @param start The start of the event.
     * @param end The end of the event.
     * @return The duration of the given VEvent or 0. Never null.
     */
    public static long getDurationOr0(@Nullable DateStart start, @Nullable DateEnd end) {
        if (start == null || end == null)
            return 0;

        ICalDate startValue = start.getValue();
        ICalDate endValue = end.getValue();

        if (startValue == null || endValue == null)
            return 0;

        return endValue.getTime() - startValue.getTime();
    }

    /**
     * This function compares two given properties of VEvents.
     * It'll make all necessary null pointer checks.
     *
     * @param left The left value.
     * @param right The right value
     * @return 0 if equals, -x if left is smaller, x if right is smaller. A given null value equals the
     * smallest possible value.
     */
    public static int compareTextProperties(TextProperty left, TextProperty right) {
        if (left == null && right == null)
            return 0;
        if (left == null)
            return -1;
        if (right == null)
            return 1;

        String leftValue = left.getValue();
        String rightValue = right.getValue();

        if (leftValue == null && rightValue == null)
            return 0;
        if (leftValue == null)
            return -1;
        if (rightValue == null)
            return 1;

        return leftValue.compareTo(rightValue);
    }

    /**
     * Compares two VEvents by the given DateStarts.
     *
     * @param left The left DateStart.
     * @param right The right DateStart.
     * @return 0 if equals, -x if left is smaller, x if right is smaller. A given null value equals
     * the smallest possible value.
     */
    public static int compareByStart(DateStart left, DateStart right) {
        if (left == null && right == null)
            return 0;
        if (left == null)
            return -1;
        if (right == null)
            return 1;

        return left.getValue().compareTo(right.getValue());
    }

    /**
     * Compares two VEvents by the given DateEnds.
     *
     * @param left The left DateEnd.
     * @param right The right DateEnd.
     * @return 0 if equals, -x if left is smaller, x if right is smaller. A given null value equals
     * the smallest possible value.
     */
    public static int compareByEnd(DateEnd left, DateEnd right) {
        if (left == null && right == null)
            return 0;
        if (left == null)
            return -1;
        if (right == null)
            return 1;

        return left.getValue().compareTo(right.getValue());
    }


    /**
     * Compare two VEvents by the given start and end values. If a value is null, the duration equals
     * the smallest possible value.
     *
     * @param startLeft The start of the left value.
     * @param endLeft The end of the left value.
     * @param startRight The start of the right value.
     * @param endRight The end of the right value.
     * @return 0 if durations are equals, -x if left duration is smaller, x if right duration is
     * smaller
     */
    public static int compareByDuration(DateStart startLeft, DateEnd endLeft, DateStart startRight, DateEnd endRight) {
        if ((startLeft == null && startRight == null) || (endLeft == null && endRight == null))
            return 0;
        if (startLeft == null || endLeft == null)
            return -1;
        if (startRight == null || endRight == null)
            return 1;

        long durationLeft = getDurationOr0(startLeft, endLeft);
        long durationRight = getDurationOr0(startRight, endRight);

        return (int) (durationLeft - durationRight);
    }

    /**
     * Concat two TextProperties with the given delimiter.
     *
     * @param left The left, aka base, value.
     * @param right The right value
     * @param delimiter The delimiter which will be in the middle of both. If null an empty String
     *                  will be used.
     * @return The joined String, or just one of both, if one was null.
     */
    public static String concatTextProperties(TextProperty left, TextProperty right, String delimiter) {
        if (delimiter == null)
            delimiter = "";

        String leftValue = null;
        String rightValue = null;

        if (left == null && right == null)
            return "";

        if (left != null)
            leftValue = left.getValue();

        if (right != null)
            rightValue = right.getValue();

        if (rightValue == null && leftValue == null)
            return "";
        if (leftValue == null)
            return rightValue;
        if (rightValue == null)
            return leftValue;

        return leftValue + delimiter + rightValue;
    }

    /**
     * This function calculates if the event starts at midnight and have a duration multiple days long.
     * @param start The start of the event.
     * @param end The end of the event.
     * @return True if the event runs a full day.
     */
    public static boolean isFullDay(@Nullable DateStart start, @Nullable DateEnd end) {
        if (start == null || end == null)
            return false;

        Date startValue = start.getValue();
        if (startValue == null)
            return false;

        final long duration = VEventHelper.getDurationOr0(start, end);
        if (duration == 0)
            return false;

        final boolean isFullDay = duration % DateHelper.DAY_IN_MILLI == 0;
        return isFullDay && startValue.getTime() == DateHelper.getTimestampZeroClock(startValue, Locale.getDefault());
    }

    /**
     * This function calculates if the event have a change of the day.
     * @param start The start of the event
     * @param end The end of the event
     * @return True if the event end not at the same day.
     */
    public static boolean isMultiDay(@Nullable DateStart start, @Nullable DateEnd end) {
        if (start == null || end == null)
            return false;

        Date startValue = start.getValue();
        if (startValue == null)
            return false;

        Date endValue = end.getValue();
        if (endValue == null)
            return false;

        final long duration = VEventHelper.getDurationOr0(start, end);
        if (duration == 0)
            return false;

        // Proof that the event goes over 0'clock
        boolean isMultiDay = DateHelper.getTimestampZeroClock(startValue, Locale.getDefault()) <
                DateHelper.getTimestampZeroClock(endValue, Locale.getDefault());

        // Validate, that the event runs not exactly 24h
        return isMultiDay && duration != DateHelper.DAY_IN_MILLI;
    }

    public static String getTextProperty(TextProperty textProperty) {
        if (textProperty == null)
            return "";

        String value = textProperty.getValue();
        if (value == null)
            return "";

        return value;
    }
}
