package de.stuv_mosbach.stuvapp.settings;

import android.Manifest;
import android.accounts.Account;
import android.app.ProgressDialog;
import android.appwidget.AppWidgetManager;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.PreferenceCategory;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.preference.SwitchPreference;
import android.provider.CalendarContract;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import org.chaosdaten.commons.DateHelper;

import java.util.Calendar;

import de.stuv_mosbach.stuvapp.R;
import de.stuv_mosbach.stuvapp.contentProvider.LectureContract;
import de.stuv_mosbach.stuvapp.contentProvider.MeetingContract;
import de.stuv_mosbach.stuvapp.parser.ClassListParser;
import de.stuv_mosbach.stuvapp.util.AccountHelper;
import de.stuv_mosbach.stuvapp.util.ConfigGlobal;
import de.stuv_mosbach.stuvapp.widget.WidgetProvider;

public class SettingsFragment extends PreferenceFragment
        implements SharedPreferences.OnSharedPreferenceChangeListener {

    private Boolean isSyncActivated;
    private Account account;
    private ListPreference listClassOptions;
    private ClassListParser classListParser;
    private Handler handler;
    private ProgressDialog progressDialog;
    private Boolean showInternals;
    private final Runnable downloading = new Runnable() {
        @Override
        public void run() {
            if (classListParser.getDownloadActive()) {
                handler.postDelayed(this, 1000);
            } else {
                fillWithOptions();
                progressDialog.dismiss();
            }
        }
    };

    public SettingsFragment() {
    }

    public void setContext(Context context) {
        account = AccountHelper.getAccount(context);

        isSyncActivated = (
                ContentResolver.getIsSyncable(account, ConfigGlobal.AUTHORITY) == 1 &&
                        ContentResolver.getSyncAutomatically(account, ConfigGlobal.AUTHORITY) &&
                        ContentResolver.getMasterSyncAutomatically());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        showInternals = sharedPreferences.getBoolean(PreferenceKeys.SHOW_INTERNALS, false);

        setContext(getActivity().getApplicationContext());

        addPreferencesFromResource(R.xml.preferences);

        classListParser = ClassListParser.getInstance(getActivity().getApplicationContext());

        initNormalPreferences();

        if (!classListParser.existClassList()) {

            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getResources().getString(R.string.prf_messages_refreshclasslist));
            progressDialog.setIndeterminate(true);
            progressDialog.show();

            classListParser.startDownload();
            handler = new Handler();
            handler.postDelayed(downloading, 1000);
        }

        Boolean isSystemSyncEnabled = sharedPreferences.getBoolean(PreferenceKeys.ENABLE_SYNC_IN_SYSTEM, true);
        if (!isSystemSyncEnabled) {
            findPreference(PreferenceKeys.SYNC_IN_SYSTEM).setEnabled(false);
        }

        if (!showInternals) {
            PreferenceScreen screen = getPreferenceScreen();
            PreferenceCategory cat = (PreferenceCategory) findPreference("internals");
            screen.removePreference(cat);
        } else {
            findPreference(PreferenceKeys.CLASS).setSummary(((ListPreference) findPreference(PreferenceKeys.CLASS)).getValue());
            findPreference(PreferenceKeys.CLASS_LIST_URL).setSummary(((EditTextPreference) findPreference(PreferenceKeys.CLASS_LIST_URL)).getText());
            findPreference(PreferenceKeys.MESSAGES_URL).setSummary(((EditTextPreference) findPreference(PreferenceKeys.MESSAGES_URL)).getText());
            findPreference(PreferenceKeys.MEETINGS_URL).setSummary(((EditTextPreference) findPreference(PreferenceKeys.MEETINGS_URL)).getText());
            findPreference(PreferenceKeys.FEED_URL).setSummary(((EditTextPreference) findPreference(PreferenceKeys.FEED_URL)).getText());
            findPreference(PreferenceKeys.LUNCH_URL).setSummary(((EditTextPreference) findPreference(PreferenceKeys.LUNCH_URL)).getText());
            findPreference(PreferenceKeys.PDF_DOCS_URL).setSummary(((EditTextPreference) findPreference(PreferenceKeys.PDF_DOCS_URL)).getText());
        }
    }

    private void initNormalPreferences() {
        listClassOptions = (ListPreference) findPreference(PreferenceKeys.CLASS);
        SwitchPreference switchAutoSync = (SwitchPreference) findPreference(PreferenceKeys.AUTO_SYNC);

        Boolean successFillingOptions = fillWithOptions();

        listClassOptions.setEnabled(successFillingOptions);

        switchAutoSync.setChecked(isSyncActivated);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_refresh_classlist) {

            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getResources().getString(R.string.prf_messages_refreshclasslist));
            progressDialog.setIndeterminate(true);
            progressDialog.setOnCancelListener(dialog -> classListParser.cancelDownload());
            progressDialog.show();

            classListParser.startDownload();
            handler = new Handler();
            handler.postDelayed(downloading, 1000);

        }
        return false;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_settings, menu);
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(this);
    }

    private boolean fillWithOptions() {
        ClassListParser classListParser = ClassListParser.getInstance(getActivity());

        if (!classListParser.parse())
            return false;

        listClassOptions.setEntries(classListParser.getKeys());
        listClassOptions.setEntryValues(classListParser.getValues());
        listClassOptions.setEnabled(true);

        return true;
    }

    public void generateSyncSettings(SharedPreferences sharedPref, Boolean forceSync) {
        ContentResolver.setIsSyncable(
                account,
                ConfigGlobal.AUTHORITY,
                1
        );

        ContentResolver.removePeriodicSync(
                account,
                ConfigGlobal.AUTHORITY,
                Bundle.EMPTY);

        if (sharedPref.getBoolean(PreferenceKeys.AUTO_SYNC, false)) {
            ContentResolver.setSyncAutomatically(
                    account,
                    ConfigGlobal.AUTHORITY,
                    true);

            ContentResolver.addPeriodicSync(
                    account,
                    ConfigGlobal.AUTHORITY,
                    Bundle.EMPTY,
                    Long.valueOf(sharedPref.getString(PreferenceKeys.SYNC_INTERVAL, String.valueOf(DateHelper.HOUR_IN_SEC * 6))));
        } else {
            ContentResolver.setSyncAutomatically(
                    account,
                    ConfigGlobal.AUTHORITY,
                    false);
        }

        if (forceSync) {
            Bundle settingsBundle = new Bundle();
            settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
            settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
            settingsBundle.putBoolean("CLASS_CHANGE", true);
            ContentResolver.requestSync(account, ConfigGlobal.AUTHORITY, settingsBundle);
        }
    }

    public void setSyncInSystemValue(Boolean value) {
        ((SwitchPreference) findPreference(PreferenceKeys.SYNC_IN_SYSTEM)).setChecked(value);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        switch (key) {
            case PreferenceKeys.CLASS:
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(PreferenceKeys.CLASS_VALUE, String.valueOf(listClassOptions.getEntry())).apply();
                generateSyncSettings(sharedPreferences, true);
                if (showInternals) {
                    findPreference(key).setSummary(((ListPreference) findPreference(key)).getValue());
                }
                break;
            case PreferenceKeys.AUTO_SYNC:
                if (sharedPreferences.getBoolean(PreferenceKeys.SYNC_IN_SYSTEM, false)) {
                    ((SwitchPreference) findPreference(PreferenceKeys.SYNC_IN_SYSTEM)).setChecked(false);
                    sharedPreferences.edit().putBoolean(PreferenceKeys.SYNC_IN_SYSTEM, false).apply();
                    String[] CALENDAR_PROJECTION = new String[]{
                            CalendarContract.Calendars._ID
                    };

                    ContentResolver cr = getActivity().getContentResolver();
                    Uri uri = CalendarContract.Calendars.CONTENT_URI;
                    String selection = "(" + CalendarContract.Calendars.ACCOUNT_TYPE + " = ?)";
                    String[] selectionArgs = new String[]{ConfigGlobal.ACCOUNT_TYPE};

                    try {
                        //TODO: Make this nice and running
                        Cursor cursor = cr.query(uri, CALENDAR_PROJECTION, selection, selectionArgs, null);

                        while (cursor != null && cursor.moveToNext()) {
                            Uri uri_tmp = ContentUris.withAppendedId(CalendarContract.Calendars.CONTENT_URI, cursor.getInt(0));
                            uri.buildUpon()
                                    .appendQueryParameter(CalendarContract.CALLER_IS_SYNCADAPTER, "true")
                                    .appendQueryParameter(CalendarContract.Calendars.ACCOUNT_NAME, ConfigGlobal.ACCOUNT)
                                    .appendQueryParameter(CalendarContract.Calendars.ACCOUNT_TYPE, ConfigGlobal.ACCOUNT_TYPE).build();

                            cr.delete(uri_tmp, null, null);
                        }

                        if (cursor != null) {
                            cursor.close();
                        }
                    } catch (SecurityException e) {
                        //Maybe the user has an Android Version > 6. Then maybe this operation fails due missing rights.
                        e.printStackTrace();
                    }
                }
            case PreferenceKeys.SYNC_INTERVAL:
                generateSyncSettings(sharedPreferences, false);
                break;
            case PreferenceKeys.HIDE_STUDY_DAYS:
                Intent intent = new Intent(getActivity(), WidgetProvider.class);
                intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
                getActivity().sendBroadcast(intent);
                break;
            case PreferenceKeys.AMOUNT_OF_DAYS:
                if (sharedPreferences.getString(key, "2").length() == 0)
                    sharedPreferences.edit().putString(key, "0").apply();
                break;
            case PreferenceKeys.THEME:
                final int currentHour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
                final boolean itsNight = (currentHour > 18 || currentHour < 7);
                if (itsNight || !(sharedPreferences.getBoolean(PreferenceKeys.DARK_THEME_NIGHT_ONLY, false))) {
                    Intent intent2 = new Intent(getActivity(), SettingsActivity.class);
                    intent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent2);
                }
                break;
            case PreferenceKeys.DARK_THEME_NIGHT_ONLY:
                final int currentHour1 = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
                final boolean itsNight1 = (currentHour1 > 18 || currentHour1 < 7);
                if (!itsNight1 && (sharedPreferences.getString(PreferenceKeys.THEME, "Light").equals("Dark")
                        || sharedPreferences.getString(PreferenceKeys.THEME, "Light").equals("Black"))) {
                    getActivity().recreate();
                }
                break;
            case PreferenceKeys.CLASS_LIST_URL:
            case PreferenceKeys.MEETINGS_URL:
            case PreferenceKeys.MESSAGES_URL:
            case PreferenceKeys.LUNCH_URL:
            case PreferenceKeys.PDF_DOCS_URL:
            case PreferenceKeys.FEED_URL:
                findPreference(key).setSummary(((EditTextPreference) findPreference(key)).getText());
                break;
            case PreferenceKeys.SYNC_IN_SYSTEM:
            case PreferenceKeys.ENABLE_SYNC_IN_SYSTEM:
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_CALENDAR) == PackageManager.PERMISSION_GRANTED &&
                        ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_CALENDAR) == PackageManager.PERMISSION_GRANTED) {
                    if (sharedPreferences.getBoolean(PreferenceKeys.SYNC_IN_SYSTEM, false) && !PreferenceKeys.ENABLE_SYNC_IN_SYSTEM.equals(key)) {
                        ContentValues values = new ContentValues();
                        values.put(CalendarContract.Calendars.ACCOUNT_NAME, ConfigGlobal.ACCOUNT);
                        values.put(CalendarContract.Calendars.ACCOUNT_TYPE, ConfigGlobal.ACCOUNT_TYPE);
                        values.put(CalendarContract.Calendars.NAME, LectureContract.SYSTEM_NAME);
                        values.put(CalendarContract.Calendars.CALENDAR_ACCESS_LEVEL, CalendarContract.Calendars.CAL_ACCESS_READ);
                        values.put(CalendarContract.Calendars.CALENDAR_COLOR, ContextCompat.getColor(getActivity(), R.color.colorPrimary));
                        values.put(CalendarContract.Calendars.CALENDAR_TIME_ZONE, "Europe/Berlin");
                        values.put(CalendarContract.Calendars.SYNC_EVENTS, 1);
                        values.put(CalendarContract.Calendars.OWNER_ACCOUNT, ConfigGlobal.ACCOUNT);
                        values.put(CalendarContract.Calendars.CALENDAR_DISPLAY_NAME, getActivity().getString(R.string.lectures_title));
                        Uri uri = CalendarContract.Calendars.CONTENT_URI.buildUpon()
                                .appendQueryParameter(CalendarContract.CALLER_IS_SYNCADAPTER, "true")
                                .appendQueryParameter(CalendarContract.Calendars.ACCOUNT_NAME, ConfigGlobal.ACCOUNT)
                                .appendQueryParameter(CalendarContract.Calendars.ACCOUNT_TYPE, ConfigGlobal.ACCOUNT_TYPE).build();

                        getActivity().getContentResolver().insert(uri, values);

                        values = new ContentValues();
                        values.put(CalendarContract.Calendars.ACCOUNT_NAME, ConfigGlobal.ACCOUNT);
                        values.put(CalendarContract.Calendars.ACCOUNT_TYPE, ConfigGlobal.ACCOUNT_TYPE);
                        values.put(CalendarContract.Calendars.NAME, MeetingContract.SYSTEM_NAME);
                        values.put(CalendarContract.Calendars.CALENDAR_ACCESS_LEVEL, CalendarContract.Calendars.CAL_ACCESS_READ);
                        values.put(CalendarContract.Calendars.CALENDAR_COLOR, ContextCompat.getColor(getActivity(), R.color.colorPrimaryLight));
                        values.put(CalendarContract.Calendars.CALENDAR_TIME_ZONE, "Europe/Berlin");
                        values.put(CalendarContract.Calendars.SYNC_EVENTS, 1);
                        values.put(CalendarContract.Calendars.OWNER_ACCOUNT, ConfigGlobal.ACCOUNT);
                        values.put(CalendarContract.Calendars.CALENDAR_DISPLAY_NAME, getActivity().getString(R.string.meetings_title));
                        uri = CalendarContract.Calendars.CONTENT_URI.buildUpon()
                                .appendQueryParameter(CalendarContract.CALLER_IS_SYNCADAPTER, "true")
                                .appendQueryParameter(CalendarContract.Calendars.ACCOUNT_NAME, ConfigGlobal.ACCOUNT)
                                .appendQueryParameter(CalendarContract.Calendars.ACCOUNT_TYPE, ConfigGlobal.ACCOUNT_TYPE).build();

                        getActivity().getContentResolver().insert(uri, values);
                        generateSyncSettings(sharedPreferences, true);
                    } else {
                        String[] CALENDAR_PROJECTION = new String[]{
                                CalendarContract.Calendars._ID
                        };

                        ContentResolver cr = getActivity().getContentResolver();
                        Uri uri = CalendarContract.Calendars.CONTENT_URI;
                        String selection = "(" + CalendarContract.Calendars.ACCOUNT_TYPE + " = ?)";
                        String[] selectionArgs = new String[]{ConfigGlobal.ACCOUNT_TYPE};

                        Cursor cursor = cr.query(uri, CALENDAR_PROJECTION, selection, selectionArgs, null);

                        while (cursor != null && cursor.moveToNext()) {
                            Uri uri_tmp = ContentUris.withAppendedId(CalendarContract.Calendars.CONTENT_URI, cursor.getInt(0));
                            uri.buildUpon()
                                    .appendQueryParameter(CalendarContract.CALLER_IS_SYNCADAPTER, "true")
                                    .appendQueryParameter(CalendarContract.Calendars.ACCOUNT_NAME, ConfigGlobal.ACCOUNT)
                                    .appendQueryParameter(CalendarContract.Calendars.ACCOUNT_TYPE, ConfigGlobal.ACCOUNT_TYPE).build();

                            cr.delete(uri_tmp, null, null);
                        }

                        if (cursor != null) {
                            cursor.close();
                        }
                        sharedPreferences.edit().putBoolean(PreferenceKeys.SYNC_IN_SYSTEM, false).apply();
                        ((SwitchPreference) findPreference(PreferenceKeys.SYNC_IN_SYSTEM)).setChecked(false);
                        ((SwitchPreference) findPreference(PreferenceKeys.SYNC_IN_SYSTEM)).setEnabled(false);
                    }

                    if (PreferenceKeys.ENABLE_SYNC_IN_SYSTEM.equals(key) && sharedPreferences.getBoolean(PreferenceKeys.ENABLE_SYNC_IN_SYSTEM, true)) {
                        ((SwitchPreference) findPreference(PreferenceKeys.SYNC_IN_SYSTEM)).setEnabled(true);
                    }
                } else {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_CALENDAR, Manifest.permission.WRITE_CALENDAR}, 23);
                }
        }
    }
}
