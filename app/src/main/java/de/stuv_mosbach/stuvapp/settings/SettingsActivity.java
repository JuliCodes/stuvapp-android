package de.stuv_mosbach.stuvapp.settings;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.CalendarContract;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;

import de.stuv_mosbach.stuvapp.R;
import de.stuv_mosbach.stuvapp.contentProvider.LectureContract;
import de.stuv_mosbach.stuvapp.contentProvider.MeetingContract;
import de.stuv_mosbach.stuvapp.util.ConfigGlobal;
import de.stuv_mosbach.stuvapp.util.ThemeFunctions;

/**
 * @author Peer Beckmann <peer@pbeckmann.de>
 *         <p/>
 *         This is just the frame for the settings fragment, so this has a really simple base functionality.
 */
public class SettingsActivity extends AppCompatActivity {
    private SettingsFragment fragment;

    /**
     * Initialize the SettingsActivity, if savedInstanceState is given create it up from the bundle
     *
     * @param savedInstanceState Saved Instance of the Activity, Android intern parameter
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ThemeFunctions.changeTheme(this, PreferenceManager.getDefaultSharedPreferences(this));

        setContentView(R.layout.simple_fragment_container);

        if (savedInstanceState == null) {
            fragment = new SettingsFragment();
            fragment.setContext(this);
            fragment.setHasOptionsMenu(true);

            getFragmentManager().beginTransaction()
                    .add(R.id.contentPane, fragment, "settings")
                    .commit();
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setDisplayHomeAsUpEnabled(true);
    }


    @SuppressWarnings("ResourceType")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d("SYNC", "Permission granzted: " + String.valueOf(requestCode));
        switch (requestCode) {
            case 23:
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    if (sharedPreferences.getBoolean(PreferenceKeys.SYNC_IN_SYSTEM, false)) {
                        ContentValues values = new ContentValues();
                        values.put(CalendarContract.Calendars.ACCOUNT_NAME, ConfigGlobal.ACCOUNT);
                        values.put(CalendarContract.Calendars.ACCOUNT_TYPE, ConfigGlobal.ACCOUNT_TYPE);
                        values.put(CalendarContract.Calendars.NAME, LectureContract.SYSTEM_NAME);
                        values.put(CalendarContract.Calendars.CALENDAR_ACCESS_LEVEL, CalendarContract.Calendars.CAL_ACCESS_READ);
                        values.put(CalendarContract.Calendars.CALENDAR_COLOR, ContextCompat.getColor(this, R.color.colorPrimary));
                        values.put(CalendarContract.Calendars.CALENDAR_TIME_ZONE, "Europe/Berlin");
                        values.put(CalendarContract.Calendars.SYNC_EVENTS, 1);
                        values.put(CalendarContract.Calendars.OWNER_ACCOUNT, ConfigGlobal.ACCOUNT);
                        values.put(CalendarContract.Calendars.CALENDAR_DISPLAY_NAME, getString(R.string.lectures_title));
                        Uri uri = CalendarContract.Calendars.CONTENT_URI.buildUpon()
                                .appendQueryParameter(CalendarContract.CALLER_IS_SYNCADAPTER, "true")
                                .appendQueryParameter(CalendarContract.Calendars.ACCOUNT_NAME, ConfigGlobal.ACCOUNT)
                                .appendQueryParameter(CalendarContract.Calendars.ACCOUNT_TYPE, ConfigGlobal.ACCOUNT_TYPE).build();

                        getContentResolver().insert(uri, values);

                        values = new ContentValues();
                        values.put(CalendarContract.Calendars.ACCOUNT_NAME, ConfigGlobal.ACCOUNT);
                        values.put(CalendarContract.Calendars.ACCOUNT_TYPE, ConfigGlobal.ACCOUNT_TYPE);
                        values.put(CalendarContract.Calendars.NAME, MeetingContract.SYSTEM_NAME);
                        values.put(CalendarContract.Calendars.CALENDAR_ACCESS_LEVEL, CalendarContract.Calendars.CAL_ACCESS_READ);
                        values.put(CalendarContract.Calendars.CALENDAR_COLOR, ContextCompat.getColor(this, R.color.colorPrimaryLight));
                        values.put(CalendarContract.Calendars.CALENDAR_TIME_ZONE, "Europe/Berlin");
                        values.put(CalendarContract.Calendars.SYNC_EVENTS, 1);
                        values.put(CalendarContract.Calendars.OWNER_ACCOUNT, ConfigGlobal.ACCOUNT);
                        values.put(CalendarContract.Calendars.CALENDAR_DISPLAY_NAME, getString(R.string.meetings_title));
                        uri = CalendarContract.Calendars.CONTENT_URI.buildUpon()
                                .appendQueryParameter(CalendarContract.CALLER_IS_SYNCADAPTER, "true")
                                .appendQueryParameter(CalendarContract.Calendars.ACCOUNT_NAME, ConfigGlobal.ACCOUNT)
                                .appendQueryParameter(CalendarContract.Calendars.ACCOUNT_TYPE, ConfigGlobal.ACCOUNT_TYPE).build();

                        this.getContentResolver().insert(uri, values);
                        fragment.generateSyncSettings(sharedPreferences, true);
                    } else {
                        String[] CALENDAR_PROJECTION = new String[]{
                                CalendarContract.Calendars._ID
                        };

                        ContentResolver cr = this.getContentResolver();
                        Uri uri = CalendarContract.Calendars.CONTENT_URI;
                        String selection = "(" + CalendarContract.Calendars.ACCOUNT_TYPE + " = ?)";
                        String[] selectionArgs = new String[]{ConfigGlobal.ACCOUNT_TYPE};

                        Cursor cursor = cr.query(uri, CALENDAR_PROJECTION, selection, selectionArgs, null);

                        while (cursor != null && cursor.moveToNext()) {
                            Log.d("SYNC", String.valueOf(cursor.getInt(0)));
                            Uri uri_tmp = ContentUris.withAppendedId(CalendarContract.Calendars.CONTENT_URI, cursor.getInt(0));
                            uri.buildUpon()
                                    .appendQueryParameter(CalendarContract.CALLER_IS_SYNCADAPTER, "true")
                                    .appendQueryParameter(CalendarContract.Calendars.ACCOUNT_NAME, ConfigGlobal.ACCOUNT)
                                    .appendQueryParameter(CalendarContract.Calendars.ACCOUNT_TYPE, ConfigGlobal.ACCOUNT_TYPE).build();

                            cr.delete(uri_tmp, null, null);
                        }

                        if (cursor != null) {
                            cursor.close();
                        }
                    }
                } else {
                    if (sharedPreferences.getBoolean(PreferenceKeys.SYNC_IN_SYSTEM, false)) {
                        sharedPreferences.edit().putBoolean(PreferenceKeys.SYNC_IN_SYSTEM, false).apply();
                        fragment.setSyncInSystemValue(false);
                    } else {
                        sharedPreferences.edit().putBoolean(PreferenceKeys.SYNC_IN_SYSTEM, true).apply();
                        fragment.setSyncInSystemValue(true);
                    }
                }
                break;
        }

    }
}
