package de.stuv_mosbach.stuvapp.settings;

/**
 * Diese Klasse enthält alle notwendigen Einstellungskeys.
 *
 * @author Peer Beckmann
 */
public class PreferenceKeys {
    public static final String APP_VERSION = "app_version";
    public static final String DISCLAIMER = "pref_accepted_disclaimer";
    public static final String CLASS = "pref_class";
    public static final String CLASS_VALUE = "pref_class_value";
    public static final String CLASS_LIST_URL = "class_list_url";
    public static final String MESSAGES_URL = "messages_url";
    public static final String MEETINGS_URL = "meetings_url";
    public static final String AUTO_SYNC = "pref_sync_auto";
    public static final String SYNC_INTERVAL = "pref_sync_interval";
    public static final String SYNC_IN_SYSTEM = "dev_sync_in_system";
    public static final String STANDARD_FRAGMENT = "pref_fragment";
    public static final String SHOW_LECTURE_IN_TITLE = "pref_show_lecture";
    public static final String HIDE_STUDY_DAYS = "hide_study_days";
    public static final String AMOUNT_OF_DAYS = "pref.latest.amount_days";
    public static final String THEME = "pref_theme";
    public static final String DARK_THEME_NIGHT_ONLY = "pref_darkThemeNightOnly";
    public static final String WIDGET_THEME = "pref_widget_theme";
    public static final String WIDGET_HIDE_HEADER = "pref_widget_hide_header";
    public static final String INTRO_POSITION = "pref_intro_last_position";
    public static final String INTRO_FINISHED = "pref_intro_finished";
    public static final String LAST_SUCCESS_SYNC = "pref_last_success_sync";
    public static final String NOTIFICATION_ID = "pref_notification_id";
    public static final String FEED_URL = "pref_urls_feed";
    public static final String BSOD = "pref_bsod";
    public static final String BSOD_MESSAGE = "pref_bsod_message";
    public static final String LUNCH_URL = "pref_urls_lunch";
    public static final String PDF_DOCS_URL = "pref_urls_pdf_docs";

    public static final String ENABLE_SYNC_IN_SYSTEM = "enable_sync_in_system";
    public static final String SHOW_INTERNALS = "show_internals";
    public static final String SHOW_NOW = "pref_show_now";
    public static final String SHOW_LECTURES = "pref_show_lectures";
    public static final String SHOW_MEETINGS = "pref_show_meetings";
    public static final String SHOW_NEWS = "pref_show_news";
    public static final String SHOW_LUNCH = "pref_show_lunch";
    public static final String SHOW_STUV = "pref_show_stuv";
    public static final String SHOW_SETTINGS = "pref_show_settings";
    public static final String SHOW_TUTORIAL = "pref_show_tutorial";
    public static final String SHOW_ISSUE = "pref_show_issue";
    public static final String SHOW_ABOUT = "pref_show_about";
}
