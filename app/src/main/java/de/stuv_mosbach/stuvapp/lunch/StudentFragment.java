package de.stuv_mosbach.stuvapp.lunch;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import de.stuv_mosbach.stuvapp.R;
import de.stuv_mosbach.stuvapp.navigation.MySwipeRefreshLayout;
import de.stuv_mosbach.stuvapp.sync.SyncAdapter;
import de.stuv_mosbach.stuvapp.util.PDFRendererActivity;
import de.stuv_mosbach.stuvapp.webview.ShowInWebViewActivity;

/**
 * Diese Fragment ermöglicht es dem Nutzer die verschiedenen Seiten des Studierendenwerks aufzurufen.
 * Wenn der Nutzer den Mensaplan öffnet, so geschieht dies in der App über den Google Drive JS PDF
 * Viewer. Alle anderen Links werden in einem externen Webbrowser geöffnet.
 *
 * @author Peer Beckmann <peer@pbeckmann.de>
 */
public class StudentFragment extends Fragment {

    private static final String URL_STUDENT_SERVICE = "http://www.stw.uni-heidelberg.de/de/";
    private static final String URL_STUDENT_SERVICE_ADVICE = "http://www.stw.uni-heidelberg.de/de/beratung";
    private static final String URL_STUDENT_SERVICE_LIVING = "http://www.stw.uni-heidelberg.de/de/wohnen";
    private static final String URL_STUDENT_SERVICE_EAT_DRINK = "http://www.stw.uni-heidelberg.de/de/essen_trinken";
    private static final String URL_STUDENT_SERVICE_FINANCE = "http://www.stw.uni-heidelberg.de/de/bafoeg";
    private static final String URL_STUDENT_SERVICE_INTERNATIONAL = "http://www.stw.uni-heidelberg.de/de/international";
    private static final String URL_STUDENT_SERVICE_EVENTS = "http://www.stw.uni-heidelberg.de/de/kultur";
    private static final String URL_STUDENT_SERVICE_CHILDREN = "http://www.stw.uni-heidelberg.de/de/studieren_mit_kind";

    private static final String URL_LUNCHPLAN = "https://www.studentenwerk.uni-heidelberg.de/sites/default/files/download/pdf/sp-mos-mensa-aktuell.pdf";
    private static final String URL_GOOGLE_DOCS_LUNCHPLAN = "https://docs.google.com/gview?embedded=true&url=" + URL_LUNCHPLAN;

    private static ProgressDialog progress;

    public static void openLunchplan(Context context) {
        String pdfPath = context.getFilesDir() + File.separator + "mensaplan.pdf";
        new DownloadTask(context, pdfPath).execute("");
    }

    private static void showInWebview(Context context) {
        Intent intent1 = new Intent(context.getApplicationContext(), ShowInWebViewActivity.class);
        intent1.putExtra(ShowInWebViewActivity.KEY_URL, URL_GOOGLE_DOCS_LUNCHPLAN);
        intent1.putExtra(ShowInWebViewActivity.KEY_TITLE, context.getResources().getString(R.string.lunchplan));
        intent1.putExtra(ShowInWebViewActivity.KEY_ENABLE_JS, true);
        intent1.putExtra(ShowInWebViewActivity.KEY_ENABLE_BUILTINZOOM, true);
        context.startActivity(intent1);
    }

    /**
     * Diese Funktion erstellt die Ansicht und liefert sie dem System zurück.
     *
     * @param inflater           Der Layout Inflater der aus der XML, die Ansicht generiert.
     * @param container          Der Container, in den das Fragment eingefügt wird.
     * @param savedInstanceState Wenn vorhanden eine SavedInstance. Da hier keine große Bearbeitung
     *                           von nöten ist, ignorieren wird das hier.
     * @return Die fertige Ansicht.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_lunch, container, false);

        //FadeInAnimation beim Fragmentwechsel
        AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(200);
        view.startAnimation(anim);

        initLinks(view);

        view.findViewById(R.id.download_button).setOnClickListener(v -> {
            if (isOnline()) {
                openLunchplan(getActivity());
            } else {
                Snackbar.make(view, getString(R.string.lunch_no_connection), Snackbar.LENGTH_LONG).show();
            }
        });
        return view;
    }

    /**
     * Diese Funktion wird immer aufgerufen, wenn das Fragment in den Vordergrund kommt. Sie wird auch
     * immer ausgeführt, wenn vorher #onCreateView ausgeführt wurde.
     * Hier wird das SwipeRefreshLayout deaktiviert, weil es hier nichts zum Synchronisieren gibt.
     */
    @Override
    public void onResume() {
        super.onResume();
        MySwipeRefreshLayout swipeRefreshLayout = (MySwipeRefreshLayout) getActivity().findViewById(R.id.swiperefresh);
        swipeRefreshLayout.setEnabled(false);
    }

    /**
     * Hier wird für jeden Button ein OnClickListener hinzugefügt.
     *
     * @param rootView Die View, welche die einzelnen Buttons enthält.
     */
    private void initLinks(View rootView) {
        addOnClickListener(rootView, R.id.student_service_homepage, URL_STUDENT_SERVICE);
        addOnClickListener(rootView, R.id.student_service_advice, URL_STUDENT_SERVICE_ADVICE);
        addOnClickListener(rootView, R.id.student_service_eat_and_drink, URL_STUDENT_SERVICE_EAT_DRINK);
        addOnClickListener(rootView, R.id.student_service_finance, URL_STUDENT_SERVICE_FINANCE);
        addOnClickListener(rootView, R.id.student_service_living, URL_STUDENT_SERVICE_LIVING);
        addOnClickListener(rootView, R.id.student_service_international, URL_STUDENT_SERVICE_INTERNATIONAL);
        addOnClickListener(rootView, R.id.student_service_children, URL_STUDENT_SERVICE_CHILDREN);
        addOnClickListener(rootView, R.id.student_service_events, URL_STUDENT_SERVICE_EVENTS);
    }

    /**
     * Findet in der übergebenen Ansicht den Button und fügt den Listener hinzu. Beim Click wird in
     * einem externen Webbrowser dann die Url aufgerufen.
     *
     * @param view Die View, welche den Button enthält.
     * @param id   Die ID des Buttons.
     * @param Url  Die Url, welche beim Klick aufgerufen wird.
     */
    private void addOnClickListener(View view, int id, final String Url) {
        view.findViewById(id).setOnClickListener(v -> {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(Url));
            startActivity(i);
        });
    }

    /**
     * Zum Testen ob das Gerät verbunden ist. Es zählen alle Varianten(WLAN, Mobiles Internet, Tethering etc).
     * Achtung: Liefert auch true wenn das Gerät zB mit einem Wlan verbunden ist, welches keinen tatsächlichen Zugang zum Internet hat
     */
    private boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    /**
     * Beim Drehen des Geräts wird die Activity gekillt und neugestartet, der AsyncTask überlebt allerdings
     * und versucht wenn er fertig ist den Dialog auszublenden, den es aber gar nicht mehr gibt.
     * Also wird er hier schon ausgeblendet.
     */
    @Override
    public void onPause() {
        super.onPause();
        if ((progress != null) && progress.isShowing())
            progress.dismiss();
        progress = null;
    }

    /**
     * AsyncTask für den Download der Speiseplan Datei. (Für das Anzeigen in der Webview wird diese Funktion nicht aufgerufen.)
     * <p/>
     * param context
     * <p/>
     * param handler Hierüber wird festgelegt mit welcher Methode die PDF Datei nach dem Download geöffnet wird
     * extern: Andere App zB Google Drive
     * renderer: PDF Renderer vom System auf Lollipop oder höher
     */
    private static class DownloadTask extends AsyncTask<String, Void, Boolean> {
        private final Context context;
        private boolean showInWebView;
        private final String pdfPath;

        DownloadTask(Context context, String pdfPath) {
            this.context = context;
            this.pdfPath = pdfPath;
            progress = new ProgressDialog(context);

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                showInWebView = true;
            }
        }

        //Anzeigen eines Ladedialogs damit es klar ist, dass etwas passiert, auch wenn der Download mal etwas länger dauert
        @Override
        protected void onPreExecute() {
            progress.setTitle(R.string.loading);
            progress.setMessage(context.getString(R.string.lunch_is_loading));
            progress.setOnCancelListener(dialog -> cancel(false));
            progress.show();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            if (!showInWebView)
                try {
                    SyncAdapter.downloadFileToDestination(new URL(URL_LUNCHPLAN), pdfPath);
                } catch (IOException e) {
                    return true;
                }
            return false;
        }

        //Ausblenden des LadeDialogs und Entscheidung wie die PDF geöffnet wird
        @Override
        protected void onPostExecute(Boolean hasError) {
            if (progress != null) {
                progress.dismiss();
            }
            if (hasError) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Error");
                builder.setMessage(R.string.emptyview_messages_wilderror);
                builder.setPositiveButton(R.string.error_synchronisation_accept, (dialog, id) -> {});
                builder.show();
            } else {
                if (showInWebView) {
                    showInWebview(context);
                } else {
                    //Der PDF Renderer ist nur auf Lollipop oder höher verfügbar. Die Activity darf nicht auf älteren Android Versionen gestarten werden!
                    Intent intent = new Intent(context.getApplicationContext(), PDFRendererActivity.class);
                    intent.putExtra("Filepath", pdfPath);
                    context.startActivity(intent);
                }
            }
        }

    }
}
