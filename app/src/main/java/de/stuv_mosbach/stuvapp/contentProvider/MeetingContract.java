package de.stuv_mosbach.stuvapp.contentProvider;

import android.net.Uri;
import android.provider.BaseColumns;
import android.provider.CalendarContract;

import de.stuv_mosbach.stuvapp.util.ConfigGlobal;

/**
 * Die Definition für die Veranstaltungen der Studierendenvertretung.
 *
 * @author Peer Beckmann
 */
public class MeetingContract {
    public static final int CONTRACT_VERSION = 4;

    public static final String SQL_DELETE_STUV_TABLE =
            "DROP TABLE IF EXISTS " + MeetingEntry.TABLE_NAME;

    public static final String SYSTEM_NAME = ConfigGlobal.AUTHORITY + ".MEETINGS";
    private static final String TYPE_TEXT = " TEXT";
    private static final String TYPE_INTEGER = " INTEGER";
    private static final String COMMA_SEP = ",";
    public static final String SQL_CREATE_STUV_TABLE =
            "CREATE TABLE " + MeetingEntry.TABLE_NAME + " (" +
                    MeetingEntry._ID + " INTEGER PRIMARY KEY," +
                    MeetingEntry.COLUMN_TITLE + TYPE_TEXT + COMMA_SEP +
                    MeetingEntry.COLUMN_DESCRIPTION + TYPE_TEXT + COMMA_SEP +
                    MeetingEntry.COLUMN_UID + TYPE_TEXT + COMMA_SEP +
                    MeetingEntry.COLUMN_LAST_MODIFIED + TYPE_INTEGER + COMMA_SEP +
                    MeetingEntry.COLUMN_LOCATION + TYPE_TEXT + COMMA_SEP +
                    MeetingEntry.COLUMN_BEGIN + TYPE_INTEGER + COMMA_SEP +
                    MeetingEntry.COLUMN_END + TYPE_INTEGER + COMMA_SEP +
                    MeetingEntry.COLUMN_FULL_DAY + TYPE_INTEGER + COMMA_SEP +
                    MeetingEntry.COLUMN_MORE_DAYS + TYPE_INTEGER + COMMA_SEP +
                    MeetingEntry.COLUMN_NEW + TYPE_INTEGER + COMMA_SEP +
                    MeetingEntry.COLUMN_DELETED + TYPE_INTEGER + COMMA_SEP +
                    MeetingEntry.COLUMN_CHANGED_LOCATION + TYPE_INTEGER + COMMA_SEP +
                    MeetingEntry.COLUMN_CHANGED_DATE + TYPE_INTEGER + " )";

    public static abstract class MeetingEntry implements BaseColumns {
        public static final Uri CONTENT_URI = Uri.parse("content://" + ConfigGlobal.AUTHORITY + "/student_events");

        public static final String TABLE_NAME = "student_events";

        public static final String COLUMN_TITLE = CalendarContract.Events.TITLE;
        public static final String COLUMN_DESCRIPTION = CalendarContract.Events.DESCRIPTION;
        // Use a generic field cause the primary uid field need api 17
        public static final String COLUMN_UID = CalendarContract.Events.CAL_SYNC1;
        public static final String COLUMN_LAST_MODIFIED = "last_modified";
        public static final String COLUMN_LOCATION = CalendarContract.Events.EVENT_LOCATION;
        public static final String COLUMN_BEGIN = CalendarContract.Events.DTSTART;
        public static final String COLUMN_END = CalendarContract.Events.DTEND;
        public static final String COLUMN_FULL_DAY = CalendarContract.Events.ALL_DAY;
        public static final String COLUMN_MORE_DAYS = "more_days";
        public static final String COLUMN_DELETED = "deleted";
        public static final String COLUMN_NEW = "new";
        public static final String COLUMN_CHANGED_LOCATION = "changed_location";
        public static final String COLUMN_CHANGED_DATE = "changed_date";
    }
}
