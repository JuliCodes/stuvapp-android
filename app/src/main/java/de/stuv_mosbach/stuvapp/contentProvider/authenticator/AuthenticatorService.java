package de.stuv_mosbach.stuvapp.contentProvider.authenticator;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

/**
 * Erstelle den Service für den {@link Authenticator}. Nötig für die Synchronisierung. Da keine
 * Logik bitte in die Android Doku schauen.
 */
public class AuthenticatorService extends Service {

    private Authenticator authenticator;

    @Override
    public void onCreate() {
        authenticator = new Authenticator(this);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return authenticator.getIBinder();
    }
}
