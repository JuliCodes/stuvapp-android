package de.stuv_mosbach.stuvapp.contentProvider;

import android.net.Uri;

import de.stuv_mosbach.stuvapp.util.ConfigGlobal;

/**
 * Diese Datei hält die Content URI für die Aktuell, Suche und das Widget bereit.
 *
 * @author Peer Beckmann
 */
public class HomeContract {
    public static final int CONTRACT_VERSION = 3;
    public static final String KEY_LECTURES = "Lecture";
    public static final String KEY_MEETINGS = "Meeting";

    public static final Uri CONTENT_URI = Uri.parse("content://" + ConfigGlobal.AUTHORITY + "/home");

    private static final String HOME_VIEW_NAME = "events";

    public static final String SQL_CREATE_HOME_VIEW = "CREATE VIEW " + HOME_VIEW_NAME + " AS SELECT " +
            LectureContract.LectureEntry._ID + ", " +
            LectureContract.LectureEntry.COLUMN_TITLE + ", " +
            LectureContract.LectureEntry.COLUMN_LOCATION + ", " +
            LectureContract.LectureEntry.COLUMN_BEGIN + ", " +
            LectureContract.LectureEntry.COLUMN_END + ", " +
            LectureContract.LectureEntry.COLUMN_DESCRIPTION + ", " +
            LectureContract.LectureEntry.COLUMN_FULL_DAY + ", " +
            LectureContract.LectureEntry.COLUMN_MORE_DAYS + ", " +
            " '" + KEY_LECTURES + "' AS origin " +
            " FROM " + LectureContract.LectureEntry.TABLE_NAME +
            " WHERE " + LectureContract.LectureEntry.COLUMN_DELETED + " is null" + " UNION SELECT " +
            MeetingContract.MeetingEntry._ID + ", " +
            MeetingContract.MeetingEntry.COLUMN_TITLE + ", " +
            MeetingContract.MeetingEntry.COLUMN_LOCATION + ", " +
            MeetingContract.MeetingEntry.COLUMN_BEGIN + ", " +
            MeetingContract.MeetingEntry.COLUMN_END + ", " +
            MeetingContract.MeetingEntry.COLUMN_DELETED + ", " +
            MeetingContract.MeetingEntry.COLUMN_FULL_DAY + ", " +
            MeetingContract.MeetingEntry.COLUMN_MORE_DAYS + ", " +
            " '" + KEY_MEETINGS + "' AS origin " +
            " FROM " + MeetingContract.MeetingEntry.TABLE_NAME +
            " WHERE " + MeetingContract.MeetingEntry.COLUMN_DELETED + " is null";

    public static final String SQL_DROP_HOME_VIEW = "DROP VIEW IF EXISTS " + HOME_VIEW_NAME;
}
