package de.stuv_mosbach.stuvapp.contentProvider;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.provider.CalendarContract;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.TextUtils;

import java.io.File;
import java.io.FileNotFoundException;

import de.stuv_mosbach.stuvapp.util.ConfigGlobal;

import static de.stuv_mosbach.stuvapp.contentProvider.FeedContract.FeedEntry;
import static de.stuv_mosbach.stuvapp.contentProvider.LectureContract.LectureEntry;
import static de.stuv_mosbach.stuvapp.contentProvider.MeetingContract.MeetingEntry;
import static de.stuv_mosbach.stuvapp.contentProvider.MessageContract.MessageEntry;

/**
 * Der ContentProvider ist dafür vorgesehen, alle Lese- Schreib- und Löschzugriffe auf die Datenbank
 * zu absolvieren. Durch dieses zentrale Stelle ist der Verwaltungsaufwand geringer, als wenn das an
 * den entsprechenden Stellen in der App selber geschehen würde.
 * <p/>
 * Die ausgeblendeten Warnungen gelten den Aufrufen, die auf dem App Context beruhen. Ein SyncAdapter
 * hat immer einen Context, also kann die Warnung guten Gewissens ausgeblendet werden.
 *
 * @author Peer Beckmann
 */
@SuppressWarnings("ConstantConditions")
public class Provider extends ContentProvider {

    private static final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);

    //Standard Aktionen
    private static final int MATCHER_LECTURES_DIR = 10;
    private static final int MATCHER_LECTURES_ITEM = 11;
    private static final int MATCHER_MEETINGS_DIR = 20;
    private static final int MATCHER_MEETINGS_ITEM = 21;
    private static final int MATCHER_MESSAGES_DIR = 30;
    private static final int MATCHER_FEED_DIR = 40;
    private static final int MATCHER_FEED_ITEM = 41;

    //SQL Views
    private static final int MATCHER_EVENTS_DIR = 50;

    //Spezial Aktionen
    private static final int MATCHER_CLEANUP_DIR = 60;
    private static final int MATCHER_FILE = 70;

    static {
        matcher.addURI(ConfigGlobal.AUTHORITY, "lectures", MATCHER_LECTURES_DIR);
        matcher.addURI(ConfigGlobal.AUTHORITY, "lectures/*", MATCHER_LECTURES_ITEM);
        matcher.addURI(ConfigGlobal.AUTHORITY, "student_events", MATCHER_MEETINGS_DIR);
        matcher.addURI(ConfigGlobal.AUTHORITY, "student_events/*", MATCHER_MEETINGS_ITEM);
        matcher.addURI(ConfigGlobal.AUTHORITY, "messages", MATCHER_MESSAGES_DIR);
        matcher.addURI(ConfigGlobal.AUTHORITY, "feed", MATCHER_FEED_DIR);
        matcher.addURI(ConfigGlobal.AUTHORITY, "feed/*", MATCHER_FEED_ITEM);
        matcher.addURI(ConfigGlobal.AUTHORITY, "home", MATCHER_EVENTS_DIR);
        matcher.addURI(ConfigGlobal.AUTHORITY, "cleanup", MATCHER_CLEANUP_DIR);
        matcher.addURI(ConfigGlobal.AUTHORITY, "file/*", MATCHER_FILE);
    }

    private SQLiteDatabase db;
    private DBHelper helper;

    /**
     * Gib den Typ von Daten zurück. Wird zur ZEit nicht bewusst in der App verwendet.
     *
     * @param uri Die URI, von der der Typ zurückgegeben werden soll.
     * @return Der Typ der URI.
     */
    @Override
    public String getType(@NonNull Uri uri) {
        switch (matcher.match(uri)) {
            case MATCHER_LECTURES_DIR:
                return ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd." + ConfigGlobal.AUTHORITY + ".lectures";
            case MATCHER_LECTURES_ITEM:
                return ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd." + ConfigGlobal.AUTHORITY + ".lectures";
            case MATCHER_MEETINGS_DIR:
                return ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd." + ConfigGlobal.AUTHORITY + ".student_events";
            case MATCHER_MEETINGS_ITEM:
                return ContentResolver.CURSOR_ITEM_BASE_TYPE + "/vnd." + ConfigGlobal.AUTHORITY + ".student_event";
            case MATCHER_MESSAGES_DIR:
                return ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd." + ConfigGlobal.AUTHORITY + ".messages";
            case MATCHER_FEED_DIR:
                return ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd." + ConfigGlobal.AUTHORITY + ".feed";
            case MATCHER_FEED_ITEM:
                return ContentResolver.CURSOR_ITEM_BASE_TYPE + "/vnd." + ConfigGlobal.AUTHORITY + ".feed";
            case MATCHER_EVENTS_DIR:
                return ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd." + ConfigGlobal.AUTHORITY + ".home";
            case MATCHER_FILE:
                return ContentResolver.CURSOR_ITEM_BASE_TYPE + "/vnd." + ConfigGlobal.AUTHORITY + ".file";
            default:
                throw new UnsupportedOperationException("Uri not known!" + uri);
        }
    }

    /**
     * Diese Klasse veraltet nicht selber die Datenbank, dafür nutzen wir einen Datenbank-Helfer.
     * Dies führt dazu, dass wir bei aktualisierten Datenbank-Schemata einfacher die Anpassungen
     * vornehmen können. Dieser wird hier initialisiert.
     *
     * @return Ein Boolean, ob alle Bedingungen für die Abfragen erfüllt sind.
     */
    @Override
    public boolean onCreate() {
        helper = new DBHelper(getContext());
        return helper != null;
    }

    @Nullable
    @Override
    public ParcelFileDescriptor openFile(@NonNull Uri uri, @NonNull String mode) throws FileNotFoundException {
        if (MATCHER_FILE == matcher.match(uri)) {
            return ParcelFileDescriptor.open(new File(getContext().getCacheDir() + File.separator + uri.getLastPathSegment()),  ParcelFileDescriptor.MODE_READ_ONLY);
        }

        return super.openFile(uri, mode);
    }

    /**
     * Diese Funktion ermöglich es, Daten aus der Datenbank auszulesen.
     *
     * @param uri           Die URI durch die wir erfahren aus welcher Tabelle, die Daten stammen.
     * @param projection    Die Spalten aus denen wir die Daten benötigen. Die Daten werden in der
     *                      Reihenfolge zurückgeliefert, wie sie in dem Array übergeben werden.
     * @param selection     Hierrüber können die Datensätze eingeschränkt werden. Der String be-
     *                      steht aus dem WHERE-Teil eines SQL-Statements. Dort wo die Werte stehen
     *                      sollten, kommen statt dem Wert Fragezeichen hin.
     * @param selectionArgs Dies ist ein Array mit den Werten für die Selektion. Die Fragezeichen
     *                      werden in der Reihenfolge ersetzt, wie sie stehen.
     * @param sortOrder     Die Reihenfolge der Datensätze. Wenn null übergeben wird, werden die
     *                      Datensätze aufsteigend nach dem Begin sortiert.
     * @return Einen Cursor aus dem wir die Daten auslesen können.
     */
    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        db = helper.getReadableDatabase();
        if (TextUtils.isEmpty(sortOrder)) {
            sortOrder = CalendarContract.Events.DTSTART + " ASC, (" + CalendarContract.Events.DTEND + " - " + CalendarContract.Events.DTSTART + ")";
        }
        Cursor cursor;

        switch (matcher.match(uri)) {
            case MATCHER_LECTURES_DIR:
                cursor = db.query(LectureEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);
                break;
            case MATCHER_MEETINGS_DIR:
                cursor = db.query(MeetingEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);
                break;
            case MATCHER_MESSAGES_DIR:
                cursor = db.query(MessageEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);
                break;
            case MATCHER_FEED_DIR:
                if (sortOrder.equals(CalendarContract.Events.DTSTART + " ASC, (" + CalendarContract.Events.DTEND + " - " + CalendarContract.Events.DTSTART + ")"))
                    sortOrder = " " + FeedEntry.COLUMN_PUBDATE + " ASC";
                cursor = db.query(FeedEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);
                break;
            case MATCHER_EVENTS_DIR:
                cursor = db.query("events", projection, selection, selectionArgs, null, null, sortOrder);
                break;
            case MATCHER_FILE:
                return null;
            default:
                throw new UnsupportedOperationException("Uri not known!" + uri);
        }
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    /**
     * Hierrüber können wir Termine einfügen.
     * Am Ende der Funktion senden wir noch Update Benachrichtigungen an die Adapter, damit diese
     * die Oberfläche aktualisieren.
     *
     * @param uri    Die URI unter der wir die Daten einfügen wollen.
     * @param values Die ContentValues zum Einfügen. Diese sind Key-Value Paare, wobei die Keys mit
     *               den Namen der Spalten übereinstimmen müssen.
     * @return Die URI unter der die eingefügten Datensätze zu finden sind.
     */
    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        db = helper.getWritableDatabase();
        switch (matcher.match(uri)) {
            case MATCHER_LECTURES_DIR:
                db.insert(LectureEntry.TABLE_NAME, null, values);
                break;
            case MATCHER_MEETINGS_DIR:
                db.insert(MeetingEntry.TABLE_NAME, null, values);
                break;
            case MATCHER_MESSAGES_DIR:
                db.insert(MessageEntry.TABLE_NAME, null, values);
                break;
            case MATCHER_FEED_DIR:
                db.insert(FeedEntry.TABLE_NAME, null, values);
                break;
            default:
                throw new UnsupportedOperationException("Uri not known " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        getContext().getContentResolver().notifyChange(HomeContract.CONTENT_URI, null);
        return uri;
    }

    /**
     * Diese Funktion führt die Löschaktionen durch. Aktuell gibt es hier nur generalistische
     * Funktionen und die CleanUp. Bei den generalistischen müssen wir in der App den Query weiter
     * einengen, da sonst die ganze Tabelle gelöscht wird.
     *
     * @param uri           Die URI, durch die wir identifizieren können, welche Datensätze gelöscht
     *                      werden sollen.
     * @param selection     Ein String zur weiteren Eingrenzung der Datensätze. Wie oben werden die
     *                      Werte durch Fragezeichen ersetzt.
     * @param selectionArgs Wie oben ein Array mit den Werten für den Selection String.
     * @return Ein Boolean, ob die Löschung erfolgreich durchgeführt werden konnte, oder nicht.
     */
    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        db = helper.getWritableDatabase();
        switch (matcher.match(uri)) {
            case MATCHER_LECTURES_DIR:
                return db.delete(LectureEntry.TABLE_NAME, selection, selectionArgs);
            case MATCHER_MEETINGS_DIR:
                return db.delete(MeetingEntry.TABLE_NAME, selection, selectionArgs);
            case MATCHER_MESSAGES_DIR:
                return db.delete(MessageEntry.TABLE_NAME, selection, selectionArgs);
            case MATCHER_FEED_DIR:
                return db.delete(FeedEntry.TABLE_NAME, selection, selectionArgs);
            case MATCHER_CLEANUP_DIR:
                int dR = 0;
                dR += db.delete(LectureEntry.TABLE_NAME, LectureEntry.COLUMN_DELETED + "=?", new String[]{"1"});
                dR += db.delete(MeetingEntry.TABLE_NAME, MeetingEntry.COLUMN_DELETED + "=?", new String[]{"1"});

                ContentValues values = new ContentValues();
                values.put(LectureEntry.COLUMN_NEW, 0);
                dR += db.update(LectureEntry.TABLE_NAME, values, LectureEntry.COLUMN_NEW + "=?", new String[]{"1"});

                ContentValues values1 = new ContentValues();
                values1.put(MeetingEntry.COLUMN_NEW, 0);
                dR += db.update(MeetingEntry.TABLE_NAME, values1, MeetingEntry.COLUMN_NEW + "=?", new String[]{"1"});

                ContentValues values2 = new ContentValues();
                values2.put(LectureEntry.COLUMN_CHANGED_LOCATION, 0);
                dR += db.update(LectureEntry.TABLE_NAME, values2, LectureEntry.COLUMN_CHANGED_LOCATION + "=?", new String[]{"1"});

                ContentValues values3 = new ContentValues();
                values3.put(MeetingEntry.COLUMN_CHANGED_LOCATION, 0);
                dR += db.update(MeetingEntry.TABLE_NAME, values2, MeetingEntry.COLUMN_CHANGED_LOCATION + "=?", new String[]{"1"});

                ContentValues values4 = new ContentValues();
                values4.put(LectureEntry.COLUMN_CHANGED_DATE, 0);
                dR += db.update(LectureEntry.TABLE_NAME, values4, LectureEntry.COLUMN_CHANGED_DATE + "=?", new String[]{"1"});

                ContentValues values5 = new ContentValues();
                values5.put(MeetingEntry.COLUMN_CHANGED_DATE, 0);
                dR += db.update(MeetingEntry.TABLE_NAME, values5, MeetingEntry.COLUMN_CHANGED_DATE + "=?", new String[]{"1"});

                getContext().getContentResolver().notifyChange(MeetingEntry.CONTENT_URI, null);
                getContext().getContentResolver().notifyChange(LectureEntry.CONTENT_URI, null);
                return dR;
            default:
                throw new UnsupportedOperationException("Uri not known" + uri);
        }
    }

    /**
     * This function handle update request. In real this function run first a delete request with
     * the data and then an insert request with the data.
     *
     * @param uri           The Uri where we want to change the data. Careful: Not all Uri's are everywhere
     *                      supported. Maybe throw an UnsupportedOperationException.
     * @param values        The new values for the uri.
     * @param selection     The selection string. As in the insert part.
     * @param selectionArgs Just as above, an array with the values for the question mark.
     * @return The number of changed lines.
     */
    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        db = helper.getWritableDatabase();
        switch (matcher.match(uri)) {
            case MATCHER_LECTURES_ITEM:
                return db.update(LectureEntry.TABLE_NAME, values, "_ID=?", new String[]{uri.getLastPathSegment()});
            case MATCHER_MEETINGS_ITEM:
                return db.update(MeetingEntry.TABLE_NAME, values, "_ID=?", new String[]{uri.getLastPathSegment()});
            case MATCHER_MEETINGS_DIR:
                return db.update(MeetingEntry.TABLE_NAME, values, selection, selectionArgs);
            case MATCHER_LECTURES_DIR:
                return db.update(LectureEntry.TABLE_NAME, values, selection, selectionArgs);
            default:
                throw new UnsupportedOperationException("Uri not known." + uri);
        }
    }
}
