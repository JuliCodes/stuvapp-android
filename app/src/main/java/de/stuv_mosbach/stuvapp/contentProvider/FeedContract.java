package de.stuv_mosbach.stuvapp.contentProvider;

import android.net.Uri;
import android.provider.BaseColumns;

import de.stuv_mosbach.stuvapp.util.ConfigGlobal;

/**
 * Define the database structure for the feed elements.
 */
public class FeedContract {
    public static final int CONTRACT_VERSION = 3;
    private static final String TYPE_TEXT = " TEXT";
    private static final String TYPE_INT = " INTEGER";
    private static final String SEP = ",";

    public static final String SQL_CREATE_FEED_TABLE =
            "CREATE TABLE " + FeedEntry.TABLE_NAME + " (" +
                    FeedEntry._ID + TYPE_INT + " PRIMARY KEY" + SEP +
                    FeedEntry.COLUMN_TITLE + TYPE_TEXT + SEP +
                    FeedEntry.COLUMN_DESCRIPTION + TYPE_TEXT + SEP +
                    FeedEntry.COLUMN_IMAGE_NAME + TYPE_TEXT + SEP +
                    FeedEntry.COLUMN_CONTENT + TYPE_TEXT + SEP +
                    FeedEntry.COLUMN_PUBDATE + TYPE_INT + SEP +
                    FeedEntry.COLUMN_LINK + TYPE_TEXT + " )";

    public static final String SQL_DELETE_NEWS_TABLE =
            "DROP TABLE IF EXISTS " + FeedEntry.TABLE_NAME;

    public static abstract class FeedEntry implements BaseColumns {
        public static final Uri CONTENT_URI = Uri.parse("content://" + ConfigGlobal.AUTHORITY + "/feed");

        public static final String TABLE_NAME = "feed";

        public static final String COLUMN_TITLE = "title";
        public static final String COLUMN_CONTENT = "content";
        public static final String COLUMN_DESCRIPTION = "description";
        public static final String COLUMN_IMAGE_NAME = "image_name";
        public static final String COLUMN_PUBDATE = "pubdate";
        public static final String COLUMN_LINK = "link";
    }

}
