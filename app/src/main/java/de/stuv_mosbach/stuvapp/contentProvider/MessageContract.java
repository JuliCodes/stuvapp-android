package de.stuv_mosbach.stuvapp.contentProvider;

import android.net.Uri;
import android.provider.BaseColumns;
import android.provider.CalendarContract;

import de.stuv_mosbach.stuvapp.util.ConfigGlobal;

/**
 * Die Definition für die Nachrichten von der StuV.
 *
 * @author Peer Beckmann
 */
public class MessageContract {
    public static final int CONTRACT_VERSION = 4;

    public static final String SQL_DELETE_MESSAGE_TABLE =
            "DROP TABLE IF EXISTS " + MessageEntry.TABLE_NAME;
    private static final String TYPE_TEXT = " TEXT";
    private static final String TYPE_INTEGER = " INTEGER";
    private static final String COMMA_SEP = ",";
    public static final String SQL_CREATE_MESSAGE_TABLE =
            "CREATE TABLE " + MessageEntry.TABLE_NAME + " (" +
                    MessageEntry._ID + " INTEGER PRIMARY KEY," +
                    MessageEntry.COLUMN_TITLE + TYPE_TEXT + COMMA_SEP +
                    MessageEntry.COLUMN_CONTENT + TYPE_TEXT + COMMA_SEP +
                    MessageEntry.COLUMN_UID + TYPE_TEXT + COMMA_SEP +
                    MessageEntry.COLUMN_BEGIN + TYPE_INTEGER + COMMA_SEP +
                    MessageEntry.COLUMN_PRIORITY + TYPE_INTEGER + COMMA_SEP +
                    MessageEntry.COLUMN_EXECUTED + TYPE_INTEGER + COMMA_SEP +
                    MessageEntry.COLUMN_END + TYPE_INTEGER + " )";

    public static abstract class MessageEntry implements BaseColumns {
        public static final Uri CONTENT_URI = Uri.parse("content://" + ConfigGlobal.AUTHORITY + "/messages");

        public static final String TABLE_NAME = "messages";

        public static final String COLUMN_TITLE = CalendarContract.Events.TITLE;
        public static final String COLUMN_CONTENT = CalendarContract.Events.DESCRIPTION;
        public static final String COLUMN_UID = "uid";
        public static final String COLUMN_BEGIN = CalendarContract.Events.DTSTART;
        public static final String COLUMN_END = CalendarContract.Events.DTEND;
        public static final String COLUMN_PRIORITY = "priority";
        public static final String COLUMN_EXECUTED = "executed";

    }
}
