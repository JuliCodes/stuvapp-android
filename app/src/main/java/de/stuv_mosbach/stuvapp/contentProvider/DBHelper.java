package de.stuv_mosbach.stuvapp.contentProvider;

import android.content.ContentResolver;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;

import de.stuv_mosbach.stuvapp.util.AccountHelper;
import de.stuv_mosbach.stuvapp.util.ConfigGlobal;

/**
 * @author Peer Beckmann <peer@pbeckmann.de>
 *         <p/>
 *         This class helps us with the work of the database.
 */
public class DBHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION =
            FeedContract.CONTRACT_VERSION +
            HomeContract.CONTRACT_VERSION +
            LectureContract.CONTRACT_VERSION +
            MeetingContract.CONTRACT_VERSION +
            MessageContract.CONTRACT_VERSION;
    private static final String DATABASE_NAME = "stuvapp.db";
    private Context context;

    /**
     * The constructor of the class.
     *
     * @param context The context, in which we want to open the database.
     */
    public DBHelper(Context context) {
        super(context, DBHelper.DATABASE_NAME, null, DBHelper.DATABASE_VERSION);
    }

    /**
     * Called when the database is created for the first time. This is where the
     * creation of tables and the initial population of the tables should happen.
     *
     * @param db The database, where we want to create the tables.
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(LectureContract.SQL_CREATE_LECTURE_TABLE);
        db.execSQL(MeetingContract.SQL_CREATE_STUV_TABLE);
        db.execSQL(MessageContract.SQL_CREATE_MESSAGE_TABLE);
        db.execSQL(HomeContract.SQL_CREATE_HOME_VIEW);
        db.execSQL(FeedContract.SQL_CREATE_FEED_TABLE);
    }

    /**
     * Will be run, if there is a difference between the old db version and the new version.
     * So if there are table changes, do it here. At the moment it'll delete all tables, create
     * them new, and initiate a full sync.
     *
     * @param db         The db, where the changes must be done.
     * @param oldVersion The integer value of the old database, so we can decide per version, what
     *                   should be done.
     * @param newVersion The integer value of the new database, so we can decide per version, what
     *                   should be done.
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(FeedContract.SQL_DELETE_NEWS_TABLE);
        db.execSQL(LectureContract.SQL_DELETE_LECTURE_TABLE);
        db.execSQL(MeetingContract.SQL_DELETE_STUV_TABLE);
        db.execSQL(MessageContract.SQL_DELETE_MESSAGE_TABLE);
        db.execSQL(HomeContract.SQL_DROP_HOME_VIEW);

        onCreate(db);

        Bundle settingsBundle = new Bundle();
        settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        ContentResolver.requestSync(AccountHelper.getAccount(context), ConfigGlobal.AUTHORITY, settingsBundle);
    }
}
