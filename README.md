[![pipeline status](https://gitlab.com/JuliCodes/stuvapp-android/badges/master/pipeline.svg)](https://gitlab.com/JuliCodes/stuvapp-android/-/commits/master)[![coverage report](https://gitlab.com/JuliCodes/stuvapp-android/badges/master/coverage.svg)](https://gitlab.com/JuliCodes/stuvapp-android/-/commits/master)

# StuVApp

Diese App ermöglicht den Studenten der DHBW Mosbach Einblick in Ihre Vorlesungspläne, die Termine der StuV und den aktuellen Mensaplan des Studierendenwerks.

# Features

 * Darstellung der Vorlesungen eines Kurses
 * Darstellung der Veranstaltungen seitens der StuV
 * Direkt Nachrichten der StuV auf die Smartphones
 * Darstellung des Mensaplans über den Google Drive PDF-Reader
 * Kombinierte Ansicht mit Vorlesungen und Veranstaltungen

# Lizenz

Der Source-Code selber steht unter GPLv3. Leider liegen alle Rechte für die Datei `stuv_logo.xml` beim Präsidium der Dualen Hochschule Baden-Württemberg. Soll für ein Projekt der reine freie Code verwendet werden, so existiert der Branch `removeOfficialStuvLogo` welchen ich kontinuierlich mit dem `master`-Branch synchronisieren werde.

# Team
## Head of Development

 * [Peer Beckmann](https://chaosdaten.org)

## Code Contributions

 * Karsten Köhler
 * Kevin Lackmann
 * Marco Fuso
 * Florian Braun

## Logo

 * Alexander Nicke
