package org.chaosdaten.commons;

import androidx.annotation.NonNull;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 * A simple class to work with dates.
 */
public class DateHelper {
    public static final int MILLISECOND = 1;
    public static final int SECOND_IN_MILLI = 1000 * MILLISECOND;
    public static final int MINUTE_IN_MILLI = 60 * SECOND_IN_MILLI;
    public static final int HOUR_IN_MILLI = 60 * MINUTE_IN_MILLI;
    public static final int DAY_IN_MILLI = 24 * HOUR_IN_MILLI;

    public static final int SECOND = 1;
    public static final int MINUTE_IN_SEC = 60 * SECOND;
    public static final int HOUR_IN_SEC = 60 * MINUTE_IN_SEC;
    public static final int DAY_IN_SEC = 24 * HOUR_IN_SEC;

    /**
     * Set the time of an date to 0'clock.
     *
     * @param date   The original date.
     * @param locale The locale of the date.
     * @return The changed date, with the same locale as the input date.
     */
    public static
    @NonNull
    Date getDateZeroClock(@NonNull Date date, @NonNull Locale locale) {
        GregorianCalendar calendar = new GregorianCalendar(locale);
        calendar.setTime(date);
        calendar.set(GregorianCalendar.MILLISECOND, 0);
        calendar.set(GregorianCalendar.SECOND, 0);
        calendar.set(GregorianCalendar.MINUTE, 0);
        calendar.set(GregorianCalendar.HOUR_OF_DAY, 0);
        return calendar.getTime();
    }

    public static long getTimestampZeroClock(@NonNull Date date, @NonNull Locale locale) {
        return getDateZeroClock(date, locale).getTime();
    }

}
