package org.chaosdaten.commons;

import android.content.ContentResolver;
import android.content.Context;
import android.content.CursorLoader;
import android.database.Cursor;
import android.net.Uri;
import androidx.annotation.NonNull;

import java.util.LinkedList;

/**
 * A simple builder for CursorLoader.
 *
 * @author Peer Beckmann
 */
//TODO: Extend the functionality.
public class CursorLoaderBuilder {
    private final LinkedList<String> queryArg = new LinkedList<>();
    private final Context context;
    private final StringBuilder query = new StringBuilder();
    private final StringBuilder sortOrder = new StringBuilder();
    private String[] projection;
    private boolean bracketOpen;
    private Uri uri;

    public CursorLoaderBuilder(@NonNull Context context) {
        this.context = context;
    }

    public CursorLoaderBuilder setProjection(@NonNull final String[] projection) {
        this.projection = projection;
        return this;
    }

    public CursorLoaderBuilder addSortOrder(@NonNull final String sort) {
        if (sortOrder.length() > 0)
            sortOrder.append(",");
        sortOrder.append(sort);
        return this;
    }

    public CursorLoaderBuilder andQuery(@NonNull final String newQuery) {
        if (query.length() > 0 && query.charAt(query.length() - 1) != '(')
            query.append(" AND ");
        query.append(newQuery);
        return this;
    }

    public CursorLoaderBuilder orQuery(@NonNull final String newQuery) {
        if (query.length() > 0 && query.charAt(query.length() - 1) != '(')
            query.append(" OR ");
        query.append(newQuery);
        return this;
    }

    public CursorLoaderBuilder addQueryArgument(@NonNull final String argument) {
        queryArg.add(argument);
        return this;
    }

    public CursorLoaderBuilder setUri(@NonNull final Uri uri) {
        this.uri = uri;
        return this;
    }

    public CursorLoaderBuilder addBracket() {
        if (!bracketOpen) {
            if (query.length() > 0)
                query.append(" AND ");
            query.append(" (");
            bracketOpen = true;
        } else {
            query.append(" )");
            bracketOpen = false;
        }
        return this;
    }

    public Cursor build(@NonNull ContentResolver resolver) {
        return resolver.query(
                uri,
                projection,
                query.toString(),
                queryArg.toArray(new String[queryArg.size()]),
                sortOrder.toString()
        );
    }

    public CursorLoader build() {
        return new CursorLoader(
                context,
                uri,
                projection,
                query.toString(),
                queryArg.toArray(new String[queryArg.size()]),
                sortOrder.toString());
    }
}
