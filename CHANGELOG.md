# Changelog
All notable changes to this project will be documented in this file.
The date format is yyyy-mm-dd.

## Unveröffentlicht

## [v1.5.3] - 2015-02-12
- BSOD Dialog
- Umstieg auf https
- Tastatur wird beim Öffnen der Navigation geschlossen

## [v1.5.2.2] - 2015-12-14
- Context Suche bei langem Drücken einer Vorlesung
- Fix: Absturz wenn Fehler nicht angezeigt werden sollen und ein Fehler auftritt

## [v1.5.2] - 2015-11-20
- Ausführlichere Fehlerberichte
- Fix: Anzeige laufender Synchronisation
- Fix: Benachrichtigungen werden korrekt dargestellt

## [v1.5.1] - 2015-11-15
- Öffne die entsprechende Liste von Widget-Einträgen aus
- Fix: Duplizierte Events werden korrekt gelöscht
- Fix: Termien werden wirklich als gelöscht markiert

## [v1.5.0] - 2016-11-02
- Überarbeitung der Benachrichtigungen
- Stabilisierung der Event Verarbeitung
- Vorbereitungen für die Erkennung von Änderungen
- Parallelisierung der Synchronisation (Deutlich schneller)
- App Shortcut für den Mensaplan

## [v1.4.0.5] - 2016-10-13
- Fehlerbehebung in der Synchronisation

## [v1.4.0.4] - 2016-10-07
- Anzeige des Mensaplans korrigiert

## [v1.4.0.3] - 2016-10-02
- Fix: NPEs in der Synchronisation

## [v1.4.0.2] - 2016-09-30
- Fix: NPE in der Synchronisation

## [v1.4.0.1] - 2016-09-27
- Führe das Intro an der Stelle des Abbruchs fort
- Zeige die EmptyView ggf. im News Fragment an
- Aktiviere AutoLink im News Fragment
- Korrektur der Anzeige der News Elemente

## [v1.4.0] - 2016-09-19
- Self-built Parser ersetzt mit biweekly
- APK-Größe optimiert
- Neues Intro
- Das Datum wird beim schnellen Scrollen jetzt oben in der Liste angezeigt
- Die Einträge von der StuV Homepage werden jetzt in der App angezeigt
- Fix: Beim Zurückgehen von den Einstellungen, WebView etc. ist nun der korrekte Menüeintrag ausgewählt.
- Fix: Beim Deaktivieren der Synchronisation stürzt die App auf Marshmallow nicht mehr ab.
- Fix: Layout der Vorlesungen/Veranstaltungen auf Android 4.x verbessert.
- Fix: Die Geschwindigkeit der App beim Wechsel der verschiedenen Elemente ist deutlich verbessert.

##  [v1.3.7] - 2016-05-26
- Fix Dialog-Hintergründe für Android 4.x
- Dynamisches öffnen des Mensaplans mit PDF App, PDFRenderer, Website
- Fix Crash auf der Heute-Ansicht bei wiederaufrufen/60s offen lassen

## [v1.3.6.2] - 2016-05-03
- HotFix: Themes sind nun funktionabel für Android 4.x Geräte
- HotFix: Widget Konfiguration stürzt ab für Android 4.x Geräte
- HotFix: Nullpointer bei fehlerhafter Synchronisierung

## [v1.3.6.1] - 2016-04-25
- HotFix: Fehlerhafte Benachrichtigungen für Android 4.x Geräte

## [v1.3.6] - 2015-04-19
- Fehlerbehebung für SVG Grafiken
- Oberflächenanpassungen
- Fehlerbehebung für Nachrichten
- Stille Tod Option für Benachrichtigungen

## [v1.3.6] - 2015-04-17
- Layout Optimierungen
- Ermögliche das Verwerfen von Benachrichtigungen
- Fix: Crash bei Deaktivierung Synchronisation
- Fix: WebView Titel wurde nicht angezeigt

## [v1.3.5.1] - 2016-04-02
- Fixed Codierung

## [v1.3.5] - 2016-03-29
- Option zum Teilen von Veranstaltungen.
- Fehlerbehebungen
- Performance-Optimierung im Design
- Nutzung von SVG-Grafiken statt Bitmaps
- Nutzung von DataBinding
- Fehlerbehebung für die Zeit rund um 0 Uhr
- Ein erstes Tabletlayout
- Theming
- Hinzufügen einer Konfiguration für das Widget
- In der Mittagszeit gibt es in der Heute Ansicht einen direkten Link zum Mittagsessen
- Endgültiges Löschen der Feed-Funktionalität

## [v1.3.4.2] - 2016-01-19
- Mensaplan ist jetzt durch die App zu erreichen
- Neues Design
- Anpassung für Marshmellow
- Viele Bugfixes

## [v1.3.4.1] - 2016-01-11
- News Fragment ist nun deaktiviert.
- Latest Ansicht ist wieder scroll und klickbar
- Funktion zur Anzeige der veralteten Synchronisation deaktiviert.
- Bugfix beim Sync in den Systemkalender.

## [v1.3.4] - 2015-12-13
- Der SyncAdapter ist neu geschrieben.
- App-Berechtigungen sind an Marshmellow angepasst.

## [v1.3.3] - 2015-11-07
- Bugfixes
- Heute Ansicht schmiert nicht mehr ab
- Die vergangenen Termine werden nach dem Drehen der App noch angezeigt
- WebView zeigt jetzt den korrekten Titel an

## [v1.3.2] - 2015-10-27
- Die Termine werden nun erst wieder nach einer halben Stunde ausgeblendet.
- Die WebView stürzt beim Drehen der App nicht mehr ab

## [v1.3.1] - 2015-10-24
- Die Suche ist in die bestehenden Oberflächen integriert.
- Performance-Verbesserungen beim Start der App
- Der Ort wird wieder überall angezeigt.

## [v1.3.0] - 2015-10-22
- Einfügen einer News Sektion
- Vorbereitungen für die Veröffentlichung des Mensaplans

## [v1.2.9] - 2015-10-10
- Aufräumen in dem GUI
- Code Optimierungen

## [v1.2.8] - 2015-10-09
- Reimplementierung der Synchronisation in den System-Kalender.
- Einfügen eines Reiters zum Fehler melden.

## [v1.2.7] - 2015-10-07
- Sync in den Systemkalender

## [v1.2.6] - 2015-09-27
- Zeitgesteuerte Benachrichtigungen
- Es gibt nun eine Kurzanleitung
- Getrennte Benachrichtigungen für Veranstaltungen und Vorlesungen
- Einfügen eine ProgressBar in der Aktuell-Ansicht
- SwipeToRefresh für die Aktualisierung

## [v1.2.5] - 2015-09-26
- Viele Bugfixes
- Neue Entwickleroptionen

## [v1.2.4] - 2015-09-23
- Zeitgesteuerte Benachrichtigungen
- Es gibt nun eine Kurzanleitung

## [v1.2.3] - 2015-09-23
- Fehlerbehebung
- Getrennte Benachrichtigungen für Veranstaltungen und Vorlesungen

## [v1.2.2] - 2015-09-20
- Fehlerbehebung. Ich bitte um Entschuldigung für die entstandenen Unannehmlichkeiten.
- Fehlerbehebung im Pull-To-Refresh auf Galaxy A5
- Einfügen eine ProgressBar in der Aktuell-Ansicht

## [v1.2.1] - 2015-09-15
- Fehlerbehebung in der Synchronisation
- Fehlerbehebung im Pull-To-Refresh

## [v1.2] - 2015-09-15
- Löschen der Funktionen für die Synchronisierung in den Systemkalender
- Viele optische Veränderungen
- Die manuelle Synchronisierung wurde überarbeitet.

## [v1.1.7] - 2015-08-31
- Fehlerbehebung für Android 4.x
- Einie Hintergrundverbesserungen

## [v1.1.6] - 2015-08-30
- Einige Verbesserungen
- Fehlerbehebung auf 4.x Geräten mit dem Sync-Button

## [v1.1.5] - 2015-08-27
- Einige Bugfixes

## [v1.1.4.1] - 2015-08-25
- Hotfix wegen fehlgeschlagenen Update

## [v1.1.4] - 2015-08-25
- Einige Bugfixes
- Vorbereitungen für den Wechsel der Domains in der 1. Septemberwoche

## [v1.1.3] - 2015-08-20
- Suchfunktion eingebaut
- Nachrichtensystem eingebaut
- Funktionen für die Synchronisation in den Systemkalender neu erstellt
- Einige optische Verbesserungen

## [v1.1.2] - 2015-08-18
- Einige optische Verbesserungen

## [v1.1.1] - 2015-08-16
- Einige Fehler behoben.
- Die Funktionen für den System Sync neu geschrieben um Fehler zu vermeiden.
- Anpassen des Message Systems an die neue URL.

## [v1.1.0] - 2015-08-15
- Suchfunktion hinzugefügt

## [v1.0] - 2015-08-12
- Erstes öffentliches Release
